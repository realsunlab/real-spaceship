logLevel := Level.Warn

resolvers ++= Seq(
  "Apache Staging" at "https://repository.apache.org/content/repositories/staging/",
  "Typesafe" at "http://repo.typesafe.com/typesafe/releases",
  "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository"
)

resolvers += Resolver.url(
  "artifactory",
  url("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases"))(
  Resolver.ivyStylePatterns)

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.3")

// Use the Scalariform plugin to reformat the code
addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "0.3.1")
