import sbt._

object deps {
  val spark = Seq(
    "org.apache.spark" %% "spark-core" % "2.0.0" % "provided",
    "org.apache.spark" %% "spark-mllib" % "2.0.0" % "provided"
  )

  val scalaTest = "org.scalatest" %% "scalatest" % "2.2.1" % "test"
}
