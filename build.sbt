import sbt.Defaults
import sbt.Keys._

lazy val commonSettings = Seq(
  organization := "edu.gatech.sunlab",
  version := "0.1.0",
  scalaVersion := "2.11.8",
  scalacOptions ++= Seq("-feature", "-deprecation"),
  //force version
  ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) },
  test in assembly := {},
  fork in run := true,
  run in Compile <<= Defaults.runTask(fullClasspath in Compile,
                                      mainClass in (Compile, run),
                                      runner in (Compile, run)),
  runMain in Compile <<= Defaults.runMainTask(fullClasspath in Compile,
                                              runner in (Compile, run))
)

lazy val core = (project in file("core"))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= deps.spark)
  .settings(libraryDependencies += deps.scalaTest)

// applications
lazy val ucb = (project in file("application/ucb"))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= deps.spark)
  .settings(libraryDependencies += deps.scalaTest)
  .dependsOn(core)

lazy val sutter_sample = (project in file("application/sutter-sample"))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= deps.spark)
  .settings(libraryDependencies += deps.scalaTest)
  .dependsOn(core)

lazy val choa = (project in file("application/choa"))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= deps.spark)
  .settings(libraryDependencies += deps.scalaTest)
  .dependsOn(core)

// root project
lazy val root = (project in file("."))
  .aggregate(choa, ucb, sutter_sample, core)
  .settings(commonSettings: _*)
