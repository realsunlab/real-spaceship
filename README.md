Welcome to Spaceship Project
============================

[![Build Status](http://ec2-52-1-176-51.compute-1.amazonaws.com/api/badges/realsunlab/real-spaceship/status.svg)](http://ec2-52-1-176-51.compute-1.amazonaws.com/realsunlab/real-spaceship)

### What is this repository for? ###

* Healthcare predictive modeling platform

* Version 1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review: See JIRA (TODO add link).
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Important Links

- [Confluence Project Wiki](https://sunlab.atlassian.net/wiki/display/SPAC/)
- [JIRA Project Dashboard](https://sunlab.atlassian.net/projects/SPAC)



