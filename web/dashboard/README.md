# Web Frontend of Spaceship

## Modules

``` yaml
- devtools::install_github('klmr/modules')
- devtools::install_github('rmongodb')
- shiny
- DT
- shinydashboard
- yaml
- ggplot2
- futile.logger
- scales
```

