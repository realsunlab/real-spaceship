.common                       <- import("./common")

.rmongodb                     <- import_package("rmongodb")
.flog                         <- import_package("futile.logger")
.col                          <- "spaceship.feature_analysis_result"

ById                          <- function (job.id) {
  .common$FindById(.col, job.id)
}

ByJob                         <- function (job.id, fields = 'default', as.table = TRUE) {
  job.id                      <- .common$AsObjectId(job.id)
  fields                      <- .GetFields(fields)
  fields                      <- .common$VectorToList(fields)
  sort.fields                 <- list(create_time = -1L)
  query                       <- list('job' = job.id)
  
  featurejob.result                    <- .common$Find(.col, 
                                              query = query,
                                              fields = fields,
                                              sort = sort.fields
                                            )
  
  if (as.table) {
    .common$ListToDataFrame(featurejob.result)
  } else {
    featurejob.result
  }
} 

All                           <- function (fields = 'default', as.table = TRUE) {
  fields                      <- .GetFields(fields)
  fields                      <- .common$VectorToList(fields)
  sort.fields                 <- list(submit_time = -1L)
  
  featurejob.results                    <- .common$Find(.col, 
                                              fields = fields,
                                              sort = sort.fields)
  
  if (as.table) {
    .common$ListToDataFrame(featurejob.results)
  } else {
    featurejob.results
  }
}

Total                         <- function (job.id = NULL) {
  if (is.null(job.id)) {
    .common$Total(.col)
  } else {
    job.id                    <- .common$AsObjectId(job.id)
    query                     <- list('job' = job.id)
    .common$Count(.col, query = query)
  }
}

.GetFields                    <- function (fields = 'default') {
  if (fields == 'default') {
    c("_id",
      "job",
      "create_time",
      "method")
  } else if (fields == 'all') {
    c("_id",
      "job",
      "create_time",
      "method")
  } else if (is.character(fields)) {
    fields
  }
}