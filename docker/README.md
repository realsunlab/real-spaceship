# Dashboard
## Build
In home folder of project, run

```
docker build -t spaceship/dashboard -f docker/dashboard/Dockerfile .
```

To build the model
```
docker build -t spaceship/model:0.01 -f docker/model/Dockerfile .
```

## Run

Dashboard
```
docker run -d --link hang-mongo:mongo -p 9999:9999 spaceship/dashboard
```

Below command will open a new
``` 
docker run -it --rm --link hang-mongo:mongo -v $PWD:/root/mywork spaceship/model:0.01
```

# Run docker compose file
```
docker-compose -f docker/docker-compose.yml up
```


It's important to give the link to the docker with mongo