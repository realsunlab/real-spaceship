package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.cohort.dsl
import edu.gatech.sunlab.spaceship.common.events.Concept
import dsl._
import edu.gatech.sunlab.spaceship.common.model.Event
import edu.gatech.sunlab.spaceship.utils.TestPatient
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/13/16
  */
class PropertyFilterTest extends FunSuite with TestPatient with Matchers {

  test("testApply") {

    val filter = ($"attr2" < 0.41) and ($"concept" === "c3")
    val todo = events.filter(e => e.properties("concept") == "c3")
    filter(todo) should have length 17
  }

  test("get concept") {
    val filter = Concept("DOB")
    patients
      .map(filter.apply)
      .flatMap(_.headOption)
      .toList should have length 20
  }

  test("test boolean") {
    val events = Stream(new Event(1, "test", null, null, Seq("attr" -> true)),
                        new Event(2, "test", null, null, Seq("attr" -> false)))

    val filter = $"attr" === false

    filter.apply(events) should have length 1
  }

  test("test regex") {
    val events =
      Stream(new Event(1, "435.2", null, null, Seq("type" -> "diagnosis")),
             new Event(2, "423.2", null, null, Seq("attr" -> false)))

    val filter = (($"concept" =~= "41[0|1|3|4].*") || ($"concept" =~= "43[0-2|4-7].*")) && ($"type" === "diagnosis")

    filter(events) should have length 1
  }

  test("duration") {
    val filter = $"duration" >= (12 months)
    filter.apply(events) should have length 18
  }
}
