package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.events.aggregate.MaxContinuousDurationAggregator
import edu.gatech.sunlab.spaceship.common.model.Event
import org.joda.time.{DateTime, Duration}
import org.scalatest.{FunSuite, Matchers}
import edu.gatech.sunlab.spaceship.cohort.dsl._

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/14/16
  */
class GroupTransformerTest extends FunSuite with Matchers {
  test("NeighborOverlapEventGroupTransformer") {
    val events = Stream(
      new Event(1,
                "test",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 4, 12, 0, 0, 0),
                Seq("medclass" -> "A")),
      new Event(1,
                "test1",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0),
                Seq("medclass" -> "B")),
      new Event(1,
                "test",
                new DateTime(2010, 3, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0),
                Seq("medclass" -> "aba")),
      new Event(1,
                "test1",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0),
                Seq("medclass" -> "aba"))
    )

    val transformer = Overlap("concept", 2 days)

    val overlaps = transformer.apply(events)
    overlaps.foreach(println)

    overlaps should have length 2
    overlaps.head.begin should equal(new DateTime(2009, 4, 13, 0, 0, 0))
  }

  test("IntervalConcatEventGroupTransformer") {
    val events = Stream(
      new Event(1,
                "test1",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 4, 12, 0, 0, 0),
                Seq("medclass" -> "A")),
      new Event(1,
                "test1",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0),
                Seq("medclass" -> "A")),
      new Event(1,
                "test",
                new DateTime(2010, 3, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0),
                Seq("medclass" -> "A")),
      new Event(1,
                "test",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0),
                Seq("medclass" -> "A"))
    )

    val transformer = Concat("concept", 2 days)

    val overlaps = transformer.apply(events)
    overlaps.foreach(println)

    overlaps should have length 2
    overlaps.head.begin should equal(new DateTime(2009, 3, 13, 0, 0, 0))
  }

  test("Yearly temporal grouper") {
    val events = Stream(
      new Event(1,
                "test",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 3, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0))
    )

    val grouper = new CalenderYearTemporalGrouper[Event]
    val grouped = grouper.group(events)

    grouped should have length 2
    grouped.head should have length 2
    grouped.head.head.begin should equal(new DateTime(2009, 3, 13, 0, 0, 0))
  }

  test("Quarter temporal grouper") {
    val events = Stream(
      new Event(1,
                "test",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 5, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 6, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 4, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0))
    )

    val grouper = new CalenderQuarterTemporalGrouper[Event]
    val grouped = grouper.group(events)

    grouped should have length 3
    grouped.head should have length 1
    grouped(1) should have length 3
    grouped(2) should have length 2
    grouped.head.head.begin should equal(new DateTime(2009, 3, 13, 0, 0, 0))
  }

  test("Yearly group") {
    val events = Stream(
      new Event(1,
                "test",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 4, 13, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 5, 13, 0, 0, 0),
                new DateTime(2009, 12, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 6, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 11, 30, 0, 0, 0),
                new DateTime(2009, 12, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 4, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0))
    )

    val aggregator = new MaxContinuousDurationAggregator(Duration.ZERO)

    val transformer =
      new AggregateGroupedEventsGroupTransformer(
        new CalenderYearTemporalGrouper[Event],
        List("MaxDu" -> aggregator))

    val newEvents = transformer.apply(events)

    newEvents should have length 2
    newEvents.head.properties("MaxDu") should equal(
      new Duration(new DateTime(2009, 3, 13, 0, 0, 0),
                   new DateTime(2009, 12, 29, 0, 0, 0)))
  }
}
