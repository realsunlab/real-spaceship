package edu.gatech.sunlab.spaceship.common.util

import org.joda.time.{Period, DateTime}
import org.scalatest.FunSuite

/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
class TimeWindowTest extends FunSuite {

  test("negative infinite to point not include") {
    val current = DateTime.now()
    val timeWindow = TimeWindow.before(current)
    assert(!timeWindow.contains(current))
    assert(timeWindow.contains(current.minus(1l)))
    assert(!timeWindow.contains(current.plus(1l)))
  }

  test("negative infinite to point include") {
    val current = DateTime.now()
    val timeWindow = TimeWindow.beforeIncl(current)
    assert(timeWindow.contains(current))
    assert(timeWindow.contains(current.minus(1l)))
    assert(!timeWindow.contains(current.plus(1l)))
  }

  test("period before time point not include") {
    val current = DateTime.now()
    val duration = Period.years(1).toDurationTo(current)
    val closeEndTimeWindow = TimeWindow.before(current, duration)

    assert(!closeEndTimeWindow.contains(current))
    assert(closeEndTimeWindow.contains(current.minus(duration)))
    assert(!closeEndTimeWindow.contains(current.minus(duration).minus(1l)))
    assert(closeEndTimeWindow.contains(current.minus(duration).plus(1l)))
    assert(!closeEndTimeWindow.contains(current.plus(1l)))
    assert(closeEndTimeWindow.contains(current.minus(1l)))

    val openEndTimeWindow =
      TimeWindow.before(current, duration, closeStart = false)

    assert(!openEndTimeWindow.contains(current))
    assert(!openEndTimeWindow.contains(current.minus(duration)))
    assert(!openEndTimeWindow.contains(current.minus(duration).minus(1l)))
    assert(openEndTimeWindow.contains(current.minus(duration).plus(1l)))
    assert(!openEndTimeWindow.contains(current.plus(1l)))
    assert(openEndTimeWindow.contains(current.minus(1l)))
  }

  test("period before time point include") {
    val current = DateTime.now()
    val duration = Period.years(1).toDurationTo(current)
    val closeEndTimeWindow = TimeWindow.beforeIncl(current, duration)

    assert(closeEndTimeWindow.contains(current))
    assert(closeEndTimeWindow.contains(current.minus(duration)))
    assert(!closeEndTimeWindow.contains(current.minus(duration).minus(1l)))
    assert(closeEndTimeWindow.contains(current.minus(duration).plus(1l)))
    assert(!closeEndTimeWindow.contains(current.plus(1l)))
    assert(closeEndTimeWindow.contains(current.minus(1l)))

    val openEndTimeWindow =
      TimeWindow.beforeIncl(current, duration, closeStart = false)

    assert(openEndTimeWindow.contains(current))
    assert(!openEndTimeWindow.contains(current.minus(duration)))
    assert(!openEndTimeWindow.contains(current.minus(duration).minus(1l)))
    assert(openEndTimeWindow.contains(current.minus(duration).plus(1l)))
    assert(!openEndTimeWindow.contains(current.plus(1l)))
    assert(openEndTimeWindow.contains(current.minus(1l)))
  }

  test("point to infinite not include") {
    val current = DateTime.now()
    val timeWindow = TimeWindow.after(current)
    assert(!timeWindow.contains(current))
    assert(!timeWindow.contains(current.minus(1l)))
    assert(timeWindow.contains(current.plus(1l)))
  }

  test("point to infinite include") {
    val current = DateTime.now()
    val timeWindow = TimeWindow.afterIncl(current)
    assert(timeWindow.contains(current))
    assert(!timeWindow.contains(current.minus(1l)))
    assert(timeWindow.contains(current.plus(1l)))
  }

  test("period after time point not include") {
    val current = DateTime.now()
    val duration = Period.years(1).toDurationFrom(current)
    val closeEndTimeWindow = TimeWindow.after(current, duration)

    assert(!closeEndTimeWindow.contains(current))
    assert(closeEndTimeWindow.contains(current.plus(duration)))
    assert(!closeEndTimeWindow.contains(current.plus(duration).plus(1l)))
    assert(closeEndTimeWindow.contains(current.plus(duration).minus(1l)))
    assert(closeEndTimeWindow.contains(current.plus(1l)))
    assert(!closeEndTimeWindow.contains(current.minus(1l)))

    val openEndTimeWindow =
      TimeWindow.after(current, duration, closeEnd = false)

    assert(!openEndTimeWindow.contains(current))
    assert(!openEndTimeWindow.contains(current.plus(duration)))
    assert(!openEndTimeWindow.contains(current.plus(duration).plus(1l)))
    assert(openEndTimeWindow.contains(current.plus(duration).minus(1l)))
    assert(openEndTimeWindow.contains(current.plus(1l)))
    assert(!openEndTimeWindow.contains(current.minus(1l)))
  }

  test("period after time point include") {
    val current = DateTime.now()
    val duration = Period.years(1).toDurationFrom(current)
    val closeEndTimeWindow = TimeWindow.afterIncl(current, duration)

    assert(closeEndTimeWindow.contains(current))
    assert(closeEndTimeWindow.contains(current.plus(duration)))
    assert(!closeEndTimeWindow.contains(current.plus(duration).plus(1l)))
    assert(closeEndTimeWindow.contains(current.plus(duration).minus(1l)))
    assert(closeEndTimeWindow.contains(current.plus(1l)))
    assert(!closeEndTimeWindow.contains(current.minus(1l)))

    val openEndTimeWindow =
      TimeWindow.afterIncl(current, duration, closeEnd = false)

    assert(openEndTimeWindow.contains(current))
    assert(!openEndTimeWindow.contains(current.plus(duration)))
    assert(!openEndTimeWindow.contains(current.plus(duration).plus(1l)))
    assert(openEndTimeWindow.contains(current.plus(duration).minus(1l)))
    assert(openEndTimeWindow.contains(current.plus(1l)))
    assert(!openEndTimeWindow.contains(current.minus(1l)))
  }

}
