package edu.gatech.sunlab.spaceship.cohort.eligibility

import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.common.events.Concept
import edu.gatech.sunlab.spaceship.common.events.transform.{Debug, Offset}
import edu.gatech.sunlab.spaceship.common.util.DateTimeGranularity._
import edu.gatech.sunlab.spaceship.utils.TestPatient
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/21/16
  */
class dslTest extends FunSuite with Matchers with TestPatient {
  test("PropertyFilterTransformer to criteria") {
    val age = Concept("DOB") | Offset(granularity = Year) | Debug

    val _ = ($"duration" < 23).on(age)
  }

  test("higher order temporal") {
    val A = $"attr2" < 0.41 and $"concept" === "c3"
    val B = $"attr2" < 0.41 and $"concept" === "c3"
    val e1 = A before B atMost (2 days)
    e1 and e1
  }

  test("windowed sublist") {
    val window = "2007-07-01" to "2012-06-30" /*Date(yyyy-MM-dd).*/
    val f = ($"attr2" < 0.41 and $"concept" === "c2") | Debug | window | Debug
    patients.map(f.apply).filter(_.nonEmpty).toList should have length 12
  }

  test("InExpr dsl") {
    val concepts = List("c0", "c1")
    val filter = $"concept" =?= concepts

    filter.apply(events).toList should have length 71
  }
}
