package edu.gatech.sunlab.spaceship.feature

import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.common.events.transform._
import edu.gatech.sunlab.spaceship.common.model.{
  FeatureConstructionResult,
  FeatureNameValue,
  SamplePatient
}
import edu.gatech.sunlab.spaceship.feature.FeatureBuilder.FeatureTree._
import edu.gatech.sunlab.spaceship.feature.FeatureBuilder._
import edu.gatech.sunlab.spaceship.utils.TestPatient
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps

/**
  *
  * @author Yu Jing <yu@argcv.com> on 8/30/16
  */
class FeatureBuilderTest extends FunSuite with TestPatient with Matchers {

  val samplePatients: List[SamplePatient] = patients.map { p =>
    new SamplePatient(p,
                      p.latestDate.minusMonths(20),
                      p.latestDate.minusMonths(2),
                      0.0)
  }.toList

  val testCaseSampeList: List[FeatureExecutor] =
    samplePatients.map(patientEventConv)

  def fn2string(fnv: FeatureNameValue): String = {
    s"${fnv.name}\t:\t${fnv.value}"
  }

  object SparkContextHelper {

    import org.apache.spark.{SparkConf, SparkContext}

    lazy val createContext: SparkContext =
      createContext("SpaceShip", "local[*]")

    def createContext(appName: String): SparkContext =
      createContext(appName, "local")

    def createContext(appName: String, masterUrl: String): SparkContext = {
      val conf = new SparkConf()
        .setAppName(appName)
        .setMaster(masterUrl)
        .set("spark.executor.memory", "3G")
        .set("spark.driver.memory", "1G")
      //        .set("spark.executor.memory", "30G")
      //        .set("spark.driver.memory", "10G")
      new SparkContext(conf)
    }

  }

  test("extract all features") {
    val resp: List[Stream[FeatureNameValue]] = testCaseSampeList.take(1).map {
      (f: FeatureExecutor) =>
        val fn: Stream[FeatureNameValue] =
          !(f | LENGTH %% SUM("attr1") %% SUM("attr2"))
        println(
          s"***NON_FILTERED*******\n${fn.map(fn2string).mkString("\n")}\n----------")
        fn
    }
    resp.nonEmpty
  }

  test("filter attrs") {
    val filter: EventFilterTransformer = $"attr2" < 0.41 and $"concept" === "c3"
    val resp: List[Stream[FeatureNameValue]] = testCaseSampeList.take(1).map {
      (f: FeatureExecutor) =>
        val fn: Stream[FeatureNameValue] =
          !(f | filter | LENGTH %% SUM("attr1") %% SUM("attr2"))
        println(
          s"***I S_FILTERED*******\n${fn.map(fn2string).mkString("\n")}\n----------")
        fn
    }
    resp.nonEmpty
  }

  test("group attrs") {
    val filter: EventFilterTransformer = $"attr2" < 1.0
    val bgrouper: BinaryLabeledGrouper = BinaryLabeledGrouper($"attr2" < 0.41)
    val agroupder: AttrLabeledGrouper = AttrLabeledGrouper("attr3")
    val resp: List[Stream[FeatureNameValue]] = testCaseSampeList.take(1).map {
      (f: FeatureExecutor) =>
        val fn: Stream[FeatureNameValue] =
          !(f | filter | LENGTH %% SUM("attr1") %% SUM("attr2")
            | bgrouper | agroupder
            | SUM("attr1") %% SUM("attr2"))
        println(
          s"***I S_GROUPED *******\n${fn.map(fn2string).mkString("\n")}\n----------")
        fn
    }
    resp.nonEmpty
  }

  test("feature merge") {
    val filter: EventFilterTransformer = $"attr2" < 1.0
    val bgrouper: BinaryLabeledGrouper = BinaryLabeledGrouper($"attr2" < 0.41)
    val agroupder: AttrLabeledGrouper = AttrLabeledGrouper("attr3")
    val respl: List[Stream[FeatureNameValue]] = testCaseSampeList.take(1).map {
      (f: FeatureExecutor) =>
        val fn: Stream[FeatureNameValue] =
          !(f | filter | LENGTH %% SUM("attr1") %% SUM("attr2")
            | bgrouper | agroupder
            | SUM("attr1") %% SUM("attr2"))
        println(
          s"***FEATURE_MERGE(L)*******\n${fn.map(fn2string).mkString("\n")}\n----------")
        fn
    }
    val respr: List[Stream[FeatureNameValue]] = testCaseSampeList.take(1).map {
      (f: FeatureExecutor) =>
        val fn: Stream[FeatureNameValue] =
          !(f | filter | LENGTH %% SUM("attr1") %% SUM("attr2")
            | bgrouper | agroupder
            | SUM("attr1") %% SUM("attr2"))
        println(
          s"***FEATURE_MERGE(R)*******\n${fn.map(fn2string).mkString("\n")}\n----------")
        fn
    }
    val resp: Stream[FeatureNameValue] = respl.head ::: respr.head
    println(
      s"***FEATURE_MERGE(R)*******\n${resp.map(fn2string).mkString("\n")}\n----------")
    resp.nonEmpty
  }

  test("test feature tree builder simple") {
    val resp: (Stream[FeatureNameValue], List[(String, String)]) =
      T.s(
          $"attr2" < 1.0,
          $"attr2" > 0.1
        )(
          T.a(SUM("attr1"), SUM("attr2"))
        )
        .a(LENGTH)
        .extract(samplePatients.head)
    println(
      s"***FEATURE_BUILD_SIMPLE(TREE_1)*******\n${resp._1.map(fn2string).mkString("\n")}\n----------")
    println(s"***FEATURE_BUILD_SIMPLE(STATS_1)*******\n${resp._2
      .map(e => s"${e._1}\t=>\t${e._2}")
      .mkString("\n")}\n----------")
    resp._1.nonEmpty
  }

  test("test feature tree builder complex") {
    val filter: EventFilterTransformer = $"attr2" < 1.0
    val bgrouper: LabeledGrouper = BinaryLabeledGrouper($"attr2" < 0.41)
    val agroupder: LabeledGrouper = AttrLabeledGrouper("attr3")
    val resp: (Stream[FeatureNameValue], List[(String, String)]) =
      T.f(filter)(
          T.g(
            bgrouper,
            agroupder
          )(T.a(SUM("attr1"), SUM("attr2")))
        )
        .a(LENGTH, SUM("attr1"), SUM("attr2"))
        .extract(samplePatients.head)
    println(
      s"***FEATURE_BUILD_SIMPLE(TREE_2)*******\n${resp._1.map(fn2string).mkString("\n")}\n----------")
    println(s"***FEATURE_BUILD_SIMPLE(STATS_2)*******\n${resp._2
      .map(e => s"${e._1}\t=>\t${e._2}")
      .mkString("\n")}\n----------")
    resp._1.nonEmpty
  }

  //  test("pip pattern in rdd") {
  //    val sc: SparkContext = SparkContextHelper.createContext
  //    val filter: EventFilterTransformer = $"attr2" < 1.0
  //    val bgrouper: BinaryLabeledGrouper = BinaryLabeledGrouper($"attr2" < 0.41)
  //    val agroupder: AttrLabeledGrouper = AttrLabeledGrouper("attr3")
  //
  //    val rdd: RDD[SamplePatient] = sc.parallelize(samplePatients)
  //    val fcr: FeatureConstructionResult = RDDFeatureConstruct.exec(rdd, 2) {
  //      f =>
  //        f | filter | LENGTH | SUM("attr1") | SUM("attr2") |
  //          bgrouper | agroupder | SUM("attr1") | SUM("attr2")
  //    }
  //    println(
  //      s"num features: ${fcr.features.count()} , size mapping: ${fcr.mapping.size}")
  //  }

  test("feature tree in rdd") {
    val sc: SparkContext = SparkContextHelper.createContext
    //val filter: EventFilterTransformer = $"attr2" < 1.0
    val bgrouper: BinaryLabeledGrouper = BinaryLabeledGrouper($"attr2" < 0.41)
    val agroupder: AttrLabeledGrouper = AttrLabeledGrouper("attr3")

    val rdd: RDD[SamplePatient] = sc.parallelize(samplePatients)

    val ft: FeatureTree =
      T.s($"attr2" < 1.0,
           $"attr2" > 0.1,
           BinaryLabeledGrouper($"attr2" < 0.41))(
          T.g(
              bgrouper,
              agroupder
            )(T.a(SUM("attr1"), SUM("attr2")))
            .a(LENGTH)
        )
        .a(LENGTH, SUM("attr1"), SUM("attr2"))

    val fcr: (FeatureConstructionResult, RDD[(String, List[String])]) =
      RDDFeatureConstruct(rdd, 2) { sp =>
        ft.extract(sp)
      }
    println(
      s"num features: ${fcr._1.features.count()} , size mapping: ${fcr._1.mapping.size}")
  }

  test("dsl for feature") {
    import edu.gatech.sunlab.spaceship.feature.dsl._
    //    val filter: EventFilterTransformer = $"attr2" < 1.0
    //    val bgrouper: LabeledGrouper = BinaryLabeledGrouper($"attr2" < 0.41)
    //    val agroupder: LabeledGrouper = AttrLabeledGrouper("attr3")
    //    val resp: FeatureTree =
    //      T.f(filter)(
    //        T.g(
    //          bgrouper,
    //          agroupder
    //        )(T.a(SUM("attr1"), SUM("attr2")))
    //      )
    //        .a(LENGTH, SUM("attr1"), SUM("attr2"))

    val t = (
        ($"attr2" < 1.0) ->
          (BG($"attr2" < 0.41) ->
            (AG("attr3") -> SUM("attr1")) ~
              SUM("attr2")) ~
            (BG($"attr2" > 0.1) -> LENGTH ~ DURATION)
      ) ~
        LENGTH ~
        SUM("attr1") ~
        SUM("attr2")
    //
    //    val sc: SparkContext = SparkContextHelper.createContext
    //
    //    val rdd: RDD[SamplePatient] = sc.parallelize(samplePatients)
    //
    //    val fcr: (FeatureConstructionResult, RDD[(String, List[String])]) =
    //      RDDFeatureConstruct(rdd, 2) { sp =>
    //        t.extract(sp)
    //      }
    //    println(s"[dsl for feature] num features: ${fcr._1.features
    //      .count()} , size mapping: ${fcr._1.mapping.size}")

    samplePatients.foreach { samplePatient =>
      val resp: (Stream[FeatureNameValue], List[(String, String)]) =
        t.extract(samplePatient)
      println(
        s"***FEATURE_BUILD_TREE_FOR_DSL*******\n${resp._1.map(fn2string).mkString("\n")}\n----------")
      println(s"***FEATURE_BUILD_TREE_FOR_DSL*******\n${resp._2
        .map(e => s"${e._1}\t=>\t${e._2}")
        .mkString("\n")}\n----------")
    }

  }

}
