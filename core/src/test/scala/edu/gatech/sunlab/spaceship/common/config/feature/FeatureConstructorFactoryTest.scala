//package edu.gatech.sunlab.spaceship.common.config.feature
//
//import edu.gatech.sunlab.spaceship.common.events.aggregate.{AverageDurationAggregator, AverageValueAggregator, CountValueAggregator, EventAggregator}
//import edu.gatech.sunlab.spaceship.feature.{CompositeFeatureConstructor, FeatureConstructor, AttributeDefaultConstructor, EventAggregationConstructor}
//import edu.gatech.sunlab.spaceship.utils.TestConfig
//import org.joda.time.Duration
//import org.scalatest.{Matchers, FunSuite}
//
///**
// * @author Hang Su <hangsu@gatech.edu>
// *         Created on 9/28/15
// */
//
//class FeatureConstructorFactoryTest extends FunSuite with TestConfig with Matchers {
//  //  """
//  //    |{
//  //    |    "event": [{
//  //    |        "group": "medication",
//  //    |        "operation": "count",
//  //    |        "window": 365
//  //    |    }, {
//  //    |        "group": "diagnostic",
//  //    |        "operation": "count",
//  //    |        "window": 365
//  //    |    }, {
//  //    |        "group": "medication",
//  //    |        "operation": "avrgdu",
//  //    |        "window": 365
//  //    |    }, {
//  //    |        "group": "encounter",
//  //    |        "operation": "avrgdu",
//  //    |        "window": 365
//  //    |    }],
//  //    |    "attribute": [{
//  //    |        "attribute": "DOB",
//  //    |        "operation": "default"
//  //    |    },{
//  //    |        "attribute": "gender"
//  //    |    }]
//  //    |}
//  //    |
//  //  """
//
//  test("testToFeatureConstructor") {
//    val specs = FeatureSpec.fromJson(featureSpecJson)
//    val constructors = specs.flatMap(FeatureConstructorFactory.toFeatureConstructor)
//
//    constructors should have length 6
//    constructors.head should be (new EventAggregationConstructor(Duration.standardDays(365), "medication", CountValueAggregator))
//
//    constructors(3) should be (new EventAggregationConstructor(Duration.standardDays(365), "encounter", AverageDurationAggregator))
//
//    constructors(4) should be (new AttributeDefaultConstructor("DOB"))
//    constructors(5) should be (new AttributeDefaultConstructor("gender"))
//  }
//
//  test("testApply") {
//    val specs = FeatureSpec.fromJson(featureSpecJson)
//    val constructor = FeatureConstructorFactory(specs)
//
//    constructor shouldBe defined
//    constructor shouldBe a [Option[_]]
//    constructor.get shouldBe a [CompositeFeatureConstructor]
//  }
//}
