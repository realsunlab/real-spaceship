package edu.gatech.sunlab.spaceship.common.events.aggregate

import edu.gatech.sunlab.spaceship.common.model.Event
import org.joda.time.{DateTime, Duration}
import org.scalatest.{Matchers, FunSuite}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 7/6/16
  */
class AggregatorTest extends FunSuite with Matchers {
  test("MaxContinuousDurationAggregator") {
    val events = List(
      new Event(1,
                "test",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 4, 13, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 5, 13, 0, 0, 0),
                new DateTime(2009, 12, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2009, 6, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 4, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0)),
      new Event(1,
                "test",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0))
    )

    val aggregator = new MaxContinuousDurationAggregator(Duration.ZERO)
    val du = aggregator.apply(events)

    du should equal(
      new Duration(new DateTime(2009, 3, 13, 0, 0, 0),
                   new DateTime(2009, 12, 29, 0, 0, 0)))
  }
}
