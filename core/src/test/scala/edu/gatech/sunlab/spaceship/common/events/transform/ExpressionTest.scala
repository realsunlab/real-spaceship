package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.common.model.Event
import org.joda.time.DateTime
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps

/**
  * Created by hang on 7/18/16.
  */
class ExpressionTest extends FunSuite with Matchers {
  test("plus expression") {
    val events = List(
      new Event(1,
                "test1",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test2",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test3",
                new DateTime(2010, 3, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0)),
      new Event(1,
                "test4",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0))
    )

    val t = Project("bt" -> ($"bt" + (10 days)))
    events.map(t.project).foreach(println)
  }
}
