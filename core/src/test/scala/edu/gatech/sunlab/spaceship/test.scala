package edu.gatech.sunlab.spaceship

import edu.gatech.sunlab.spaceship.common.io.FeatureAdapter._
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 11/24/15
  */
object test {
  def main(): Unit = {

    val sc = createContext
    import org.apache.spark.mllib.linalg.Vectors
    import org.apache.spark.mllib.regression.LabeledPoint
    case class Event(patientId: String,
                     eventId: String,
                     timestamp: Int,
                     value: Double)

    val rawData = sc.textFile("input/").map { line =>
      val splits = line.split(",")
      new Event(splits(0), splits(1), splits(2).toInt, splits(3).toDouble)
    }

    // group raw data with patient id and ignore patient id
    // then we will run parallel on patient lelvel
    val grpPatients = rawData.groupBy(_.patientId).map(_._2)
    val patientTargetAndFeatures = grpPatients.map { events =>
      // find event that encode our prediction target, heart failure
      val targetEvent = events.find(_.eventId == "heartfailure").get
      val target = targetEvent.value

      // filter out other events to construct features
      val featureEvents = events.filter(_.eventId != "heartfailure")

      // define index date as one year before index
      // and use events happened one year before heart failure
      val indexDate = targetEvent.timestamp - 365
      val filteredFeatureEvents =
        featureEvents.filter(_.timestamp <= indexDate)

      // aggregate events into features
      val features = filteredFeatureEvents.groupBy(_.eventId).map {
        case (eventId, grpEvents) =>
          // user event id as feature name
          val featureName = eventId
          // event value sum as feature value
          val featureValue = grpEvents.map(_.value).sum

          (featureName, featureValue)
      }

      (target, features)
    }

    // assign unique integer id to feature name
    val featureMap =
      patientTargetAndFeatures. // RDD[(target, Map[feature-name, feature-value])]
      flatMap(_._2.keys). // get all feature values
      distinct. // remove duplication
      collect. // collect to driver program
      zipWithIndex. // assign an integer id
      toMap

    // broadcast feature map from driver to all workers
    val scFeatureMap = sc.broadcast(featureMap)
    val finalSamples = patientTargetAndFeatures.map {
      case (target, features) =>
        val numFeature = scFeatureMap.value.size
        val indexedFeatures = features.toList.
        // map feature name to id to get List[(feature-id, feature-value)]
        map {
          case (featureName, featureValue) =>
            (scFeatureMap.value(featureName), featureValue)
        }

        val featureVector = Vectors.sparse(numFeature, indexedFeatures)
        val labeledPoint = LabeledPoint(target, featureVector)
        labeledPoint
    }

    import org.apache.spark.mllib.util.MLUtils
    MLUtils.saveAsLibSVMFile(finalSamples, "samples")
    val data = MLUtils.loadLibSVMFile(sc, "samples")

    val splits = finalSamples.randomSplit(Array(0.6, 0.4), seed = 0L)
    val train = splits(0).cache()
    val test = splits(1).cache()

    val model = "x"
    import java.io.{
      FileOutputStream,
      ObjectOutputStream,
      ObjectInputStream,
      FileInputStream
    }
    val oos = new ObjectOutputStream(new FileOutputStream("/tmp/nflx"))
    oos.writeObject(model)
    oos.close()

    // (3) read the object back in
    val ois = new ObjectInputStream(new FileInputStream("/tmp/nflx"))
    val loadedModel = ois
      .readObject()
      .asInstanceOf[org.apache.spark.mllib.classification.SVMModel]
    ois.close()

    val mapping =
      featureMap.toList
        .sortBy(_._2)
        .map(pair => s"${pair._1}$$${pair._2}")
        .mkString("\n")

    scala.tools.nsc.io.File("mapping.txt").writeAll(mapping)

  }

  def createContext(appName: String, masterUrl: String): SparkContext = {
    val conf = new SparkConf()
      .setAppName(appName)
      .setMaster(masterUrl)
      .set("spark.executor.memory", "30G")
      .set("spark.driver.memory", "10G")
    new SparkContext(conf)
  }

  def createContext(appName: String): SparkContext =
    createContext(appName, "local")

  def createContext: SparkContext = createContext("SpaceShip", "local[*]")

}
