package edu.gatech.sunlab.spaceship.cohort.eligibility.temporal

import java.util.UUID

import edu.gatech.sunlab.spaceship.utils.TestEvents
import org.joda.time.DateTime
import org.scalatest.{Matchers, FunSuite}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/23/16
  */
class TemporalPredicateTest extends FunSuite with Matchers with TestEvents {
  val one = UUID.randomUUID()
  val two = UUID.randomUUID()
  val three = UUID.randomUUID()

  test("test eval") {
    val eventsContext: EventsContext = new EventsContext(
      Map(
        one -> timedEvents.take(10).toStream,
        two -> timedEvents.slice(10, 20).toStream,
        three -> timedEvents.slice(20, 30).toStream
      ))

    val predicate = new AtomicTemporalPredicate(one, two, false, false)
    with EventAllenBefore

    val (_, events) = predicate.eval(eventsContext)

    val ends = events(one).map(_.end)
    val begins = events(two).map(_.begin)

    val merged = (ends.map((_, "e")) ++ begins.map((_, "b")))
      .sortBy(_._1)(new Ordering[DateTime] {
        override def compare(x: DateTime, y: DateTime): Int = x.compareTo(y)
      })

    merged.head._2 shouldBe "e"

    ends.foreach {
      case e =>
        begins.count(_.compareTo(e) > 0) should be > 0
    }
  }

  test("test and") {
    val eventsContext: EventsContext = new EventsContext(
      Map(
        one -> timedEvents.take(10).toStream,
        two -> timedEvents.slice(10, 20).toStream,
        three -> timedEvents.slice(20, 30).toStream
      ))

    val predicate1 = new AtomicTemporalPredicate(one, two, false, false)
    with EventAllenBefore

    val predicate2 = new AtomicTemporalPredicate(two, three, false, false)
    with EventAllenBefore

    val predicate = predicate1 and predicate2

    val (_, events1) = predicate1.eval(eventsContext)

    val (_, events2) = predicate2.eval(eventsContext)

    val (_, events) = predicate.eval(eventsContext)

    events1(two).intersect(events2(two)) should have length events(two).length

    //    events.values.map(_.length).foreach(println)
    //
    //    events.toList.flatMap{case (g, es) => es.map((g, _))}.foreach(println)

    events(two).foreach {
      case e2 =>
        /*valid e1 before*/
        events(one).count(_.end.compareTo(e2.begin) < 0) should be > 0
        /*valid e3 after*/
        events(three).count(_.begin.compareTo(e2.end) > 0) should be > 0
    }
  }

  test("test or") {
    val eventsContext: EventsContext = new EventsContext(
      Map(
        one -> timedEvents.take(10).toStream,
        two -> timedEvents.slice(10, 20).toStream,
        three -> timedEvents.slice(20, 30).toStream
      ))

    val predicate1 = new AtomicTemporalPredicate(one, two, false, false)
    with EventAllenBefore

    val predicate2 = new AtomicTemporalPredicate(two, three, false, false)
    with EventAllenBefore

    val predicate = predicate1 or predicate2

    val (_, events1) = predicate1.eval(eventsContext)

    val (_, events2) = predicate2.eval(eventsContext)

    val (_, events) = predicate.eval(eventsContext)

    events1(two).union(events2(two)) should have length events(two).length

    //    events.toList.flatMap{case (g, es) => es.map((g, _))}.foreach(println)

    events(two).foreach {
      case e2 =>
        /*valid e1 before*/
        val cBefore = events(one).count(_.end.compareTo(e2.begin) < 0)
        /*valid e3 after*/
        val cAfter = events(two).count(_.begin.compareTo(e2.end) > 0)

        (cBefore + cAfter) should be > 0
    }
  }
}
