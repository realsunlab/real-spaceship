package edu.gatech.sunlab.spaceship.utils

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/28/15
  */
trait TestConfig {
  val featureSpecJson = """
      |{
      |    "event": [{
      |        "group": "medication",
      |        "operation": "count",
      |        "window": 365
      |    }, {
      |        "group": "diagnostic",
      |        "operation": "count",
      |        "window": 365
      |    }, {
      |        "group": "medication",
      |        "operation": "avrgdu",
      |        "window": 365
      |    }, {
      |        "group": "encounter",
      |        "operation": "avrgdu",
      |        "window": 365
      |    }],
      |    "attribute": [{
      |        "attribute": "DOB",
      |        "operation": "default"
      |    },{
      |        "attribute": "gender"
      |    }]
      |}
      |
    """.stripMargin

  val cohortSpecJson =
    """{
      |    "indexFirst": {
      |        "assigner": {
      |            "occurrence": {
      |                "events": [
      |                    {
      |                        "group": "medication",
      |                        "name": "GABAPENTIN"
      |                    }
      |                ],
      |                "merge": "all",
      |                "multiple": false
      |            }
      |        },
      |        "filter": {
      |            "and": [
      |                {
      |                    "recordLength": {
      |                        "length": {
      |                            "value": -365,
      |                            "unit": "day"
      |                        },
      |                        "include": true
      |                    }
      |                },
      |                {
      |                    "recordLength": {
      |                        "length": {
      |                            "value": 365,
      |                            "unit": "day"
      |                        },
      |                        "include": true
      |                    }
      |                },
      |                {
      |                    "or": [
      |                        {
      |                            "aggregation": {
      |                                "events": [
      |                                    {
      |                                        "group": "diagnostic",
      |                                        "name": "icd-9-cm:78039"
      |                                    }
      |                                ],
      |                                "window": {
      |                                    "length": {
      |                                        "value": 365,
      |                                        "unit": "day"
      |                                    },
      |                                    "include": [true, false]
      |                                },
      |                                "aggregator": "count",
      |                                "operator": ">=",
      |                                "value": 2
      |                            }
      |                        },
      |                        {
      |                            "exist": {
      |                                "events": [
      |                                    {
      |                                        "group": "diagnostic",
      |                                        "name": "icd-9-cm:78039"
      |                                    }
      |                                ],
      |                                "window": {
      |                                    "length": {
      |                                        "value": 365,
      |                                        "unit": "day"
      |                                    },
      |                                    "include": [true, false]
      |                                }
      |                            }
      |                        }
      |                    ]
      |                }
      |            ]
      |        },
      |        "target": {
      |            "positive": {
      |                "condition": {
      |                    "not":
      |                    {
      |                        "exist": {
      |                            "events": [
      |                                {"group": "medication", "name": "CARBAMAZEPINE"},
      |                                {"group": "medication", "name": "CLONAZEPAM"},
      |                                {"group": "medication", "name": "DIAZEPAM"},
      |                                {"group": "medication", "name": "LACOSAMIDE"},
      |                                {"group": "medication", "name": "LAMOTRIGINE"},
      |                                {"group": "medication", "name": "LEVETIRACETAM"},
      |                                {"group": "medication", "name": "LORAZEPAM"},
      |                                {"group": "medication", "name": "OXCARBAZEPINE"},
      |                                {"group": "medication", "name": "PHENOBARBITAL"},
      |                                {"group": "medication", "name": "PHENYTOIN"},
      |                                {"group": "medication", "name": "PREGABALIN"},
      |                                {"group": "medication", "name": "PRIMIDONE"},
      |                                {"group": "medication", "name": "TOPIRAMATE"},
      |                                {"group": "medication", "name": "ZONISAMIDE"}
      |                            ]
      |                        }
      |                    }
      |                }
      |            }
      |        }
      |    }
      |}
      |
    """.stripMargin
}
