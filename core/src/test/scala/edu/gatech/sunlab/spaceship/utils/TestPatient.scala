package edu.gatech.sunlab.spaceship.utils

import edu.gatech.sunlab.spaceship.common.model.Patient

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/2/15
  */
trait TestPatient extends TestEvents {
  lazy val patients = events.groupBy(_.patientId).map {
    case (pid, evs) => new Patient(pid, evs.toList)
  }
}
