package edu.gatech.sunlab.spaceship.common.config.feature

import edu.gatech.sunlab.spaceship.utils.TestConfig
import org.scalatest.{Matchers, FunSuite}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/28/15
  */
class FeatureSpec$Test extends FunSuite with Matchers with TestConfig {

  test("test parse feature spec from json") {

    val specs = FeatureSpec.fromJson(featureSpecJson)
    specs should have length 6
    specs.head should equal(EventFeatureSpec("medication", None, "count", 365))
    specs(4) should equal(AttributeFeatureSpec("DOB", Some("default")))
    specs.last should equal(AttributeFeatureSpec("gender", None))
  }
}
