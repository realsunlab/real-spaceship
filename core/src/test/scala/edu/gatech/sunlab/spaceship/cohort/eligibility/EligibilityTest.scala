package edu.gatech.sunlab.spaceship.cohort.eligibility

import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.common.events.Concept
import edu.gatech.sunlab.spaceship.common.events.transform.{Debug, Offset}
import edu.gatech.sunlab.spaceship.common.model.SamplePatient
import edu.gatech.sunlab.spaceship.common.util.DateTimeGranularity._
import edu.gatech.sunlab.spaceship.utils.TestPatient
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
class EligibilityTest extends FunSuite with TestPatient with Matchers {
  test("age at index") {
    val samplePatients = patients.map {
      case p =>
        new SamplePatient(p,
                          p.latestDate.minusMonths(20),
                          p.latestDate.minusMonths(2),
                          0.0)
    }

    val age = Concept("DOB") | Offset(granularity = Year) | Debug

    val eligibility = ($"value" > 25).on(age)

    val results = samplePatients.map(sp => (sp.p, eligibility.check(sp)))
    results.filter(_._2).toList should have length 4
  }

  test("temporal order before") {
    val samplePatients = patients.map {
      case p =>
        new SamplePatient(p,
                          p.latestDate.minusMonths(20),
                          p.latestDate.minusMonths(2),
                          0.0)
    }

    val aEvents = $"concept" === "c3"
    val bEvents = $"concept" === "c2"

    val eligibility = aEvents before bEvents atMost (1 years)

    val results = samplePatients.map(sp => (sp.p, eligibility.check(sp)))

    results.filter(_._2).toList should have length 11
  }

  test("temporal order non exists before") {
    val samplePatients = patients.map {
      case p =>
        new SamplePatient(p,
                          p.latestDate.minusMonths(20),
                          p.latestDate.minusMonths(2),
                          0.0)
    }

    val aEvents = $"concept" === "c3"
    val bEvents = $"concept" === "c2"

    val eligibility = !aEvents before bEvents atMost (1 years)

    val results = samplePatients.map(sp => (sp.p, eligibility.check(sp)))

    results.filter(_._2).foreach(println)
  }

  test("toStrign") {
    val aEvents = $"concept" === "c3"
    val bEvents = $"concept" === "c2"

    val cEvents = $"concept" === "c5"
    val dEvents = $"concept" === "c6"

    val eligibility = (!aEvents before bEvents atMost (1 years)) and (cEvents after dEvents)

    println(eligibility)
  }
}
