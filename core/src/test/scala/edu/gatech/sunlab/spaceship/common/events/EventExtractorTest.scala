package edu.gatech.sunlab.spaceship.common.events

import edu.gatech.sunlab.spaceship.cohort.dsl
import dsl._
import edu.gatech.sunlab.spaceship.common.events.transform.{
  BeginDateTime,
  Debug,
  NeighborOverlapEventGroupTransformer
}
import edu.gatech.sunlab.spaceship.common.model.Event
import edu.gatech.sunlab.spaceship.common.util.Events
import edu.gatech.sunlab.spaceship.utils.TestPatient
import org.joda.time.DateTime
import org.scalatest.{FunSuite, Matchers}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/14/16
  */
class EventExtractorTest extends FunSuite with TestPatient with Matchers {
  test("chain transformers") {
    val extractor = AllEventExtractor | (($"attr2" < 0.41) and ($"concept" === "c3"))
    val patientEvents = patients.map(extractor.apply)

    patientEvents.last should have length 1
    patientEvents.filter(_.length > 1).toList should have length 2
  }

  test("multiple occurrence transformers") {
    val caseTargetEventExtractor = ($"attr2" < 0.41 and $"concept" === "c3") |
        Debug |
        Overlap |
        Merge |
        $"size" > 1 |
        BeginDateTime

    val targetEvents = patients.map(caseTargetEventExtractor.apply)
    targetEvents.foreach(println)
  }

  test("toString") {
    val caseTargetEventExtractor = ($"attr2" < 0.41 and $"concept" === "c3") |
        Debug |
        Overlap |
        Merge |
        $"size" > 1 |
        BeginDateTime

    println(caseTargetEventExtractor.toString())
  }

  test("test events merging") {
    val events = Stream(
      new Event(1,
                "test1",
                new DateTime(2009, 3, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test2",
                new DateTime(2009, 4, 13, 0, 0, 0),
                new DateTime(2009, 11, 29, 0, 0, 0)),
      new Event(1,
                "test3",
                new DateTime(2010, 3, 13, 0, 0, 0),
                new DateTime(2010, 11, 29, 0, 0, 0)),
      new Event(1,
                "test4",
                new DateTime(2010, 5, 10, 0, 0, 0),
                new DateTime(2010, 12, 29, 0, 0, 0))
    )

    val merged = Events.merge(events.slice(0, 3), events.slice(1, 4))
    merged should have length 4
    merged.last.begin shouldBe new DateTime(2010, 5, 10, 0, 0, 0)
  }
}
