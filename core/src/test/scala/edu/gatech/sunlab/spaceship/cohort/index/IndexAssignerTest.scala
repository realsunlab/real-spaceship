package edu.gatech.sunlab.spaceship.cohort.index

import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.common.events.transform.{Debug, Within}
import edu.gatech.sunlab.spaceship.common.model.SamplePatient
import edu.gatech.sunlab.spaceship.utils.TestPatient
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
class IndexAssignerTest extends FunSuite with TestPatient with Matchers {
  test("event index assigner") {
    val samplePatientsWithTarget = patients.map {
      case p =>
        new SamplePatient(p, null, p.latestDate.minusMonths(2), 0.0)
    }

    val eventExtractor =
      Within(-6 months, -2 months, beginIncl = true, endIncl = true) | Debug |
        ($"attr2" < 0.41 and $"concept" === "c3") |
        Merge |
        Begin

    val indexAssigner = new EventIndexAssigner(eventExtractor)

    val samplePatients = samplePatientsWithTarget.flatMap(indexAssigner.apply)

    samplePatients.foreach {
      case sp =>
        println(s"${sp.p} index=${sp.indexDate} target=${sp.targetDate}")
    }

    samplePatients.toList should have length 4
  }

}
