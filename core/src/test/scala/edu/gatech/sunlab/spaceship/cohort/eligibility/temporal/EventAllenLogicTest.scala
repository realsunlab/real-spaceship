package edu.gatech.sunlab.spaceship.cohort.eligibility.temporal

import edu.gatech.sunlab.spaceship.common.model.{Temporal, Event}
import org.joda.time.{Duration, DateTime}
import org.scalatest.{Matchers, FunSuite}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/22/16
  */
class EventAllenLogicTest extends FunSuite with Matchers {
  test("Event Before") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base
    }
    val a2 = new Temporal {
      val begin = base.plus(20)
      val end = base.plus(30)
    }
    val a3 = new Temporal {
      val begin = base.plus(2)
      val end = base.plus(5)
    }
    val a4 = new Temporal {
      val begin = base.plus(4)
      val end = base.plus(20)
    }

    val before = new EventAllenBefore {
      override val maxDuration = Some(Duration.millis(19))
      override val minDuration = Duration.millis(3)
    }

    before.check(a1, a2) shouldBe false
    before.check(a1, a3) shouldBe false
    before.check(a1, a4) shouldBe true
  }

  test("Event After") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base
    }
    val a2 = new Temporal {
      val begin = base.plus(20)
      val end = base.plus(30)
    }
    val a3 = new Temporal {
      val begin = base.plus(2)
      val end = base.plus(5)
    }
    val a4 = new Temporal {
      val begin = base.plus(3)
      val end = base.plus(20)
    }

    val after = new EventAllenAfter {
      override val maxDuration = Some(Duration.millis(20))
      override val minDuration = Duration.millis(0)
    }

    after.check(a2, a1) shouldBe true
    after.check(a3, a1) shouldBe true
    after.check(a4, a1) shouldBe true
    after.check(a2, a4) shouldBe false
  }

  test("meet") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base
    }
    val a2 = new Temporal {
      val begin = base.plus(20)
      val end = base.plus(30)
    }
    val a3 = new Temporal {
      val begin = base.plus(1)
      val end = base.plus(5)
    }

    val meet1 = new EventAllenMeet {
      override val tolerance = Duration.millis(1)
    }

    meet1.check(a1, a2) shouldBe false
    meet1.check(a1, a3) shouldBe true
  }

  test("overlap") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base.plus(20)
    }
    val a2 = new Temporal {
      val begin = base.plus(20)
      val end = base.plus(30)
    }
    val a3 = new Temporal {
      val begin = base.plus(10)
      val end = base.plus(20)
    }

    val overlap = new EventAllenOverlap {
      override val minDuration = Duration.millis(2)
    }
    overlap.check(a1, a2) shouldBe false
    overlap.check(a1, a3) shouldBe true
  }

  test("start") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base.plus(20)
    }
    val a2 = new Temporal {
      val begin = base.plus(1)
      val end = base.plus(30)
    }
    val a3 = new Temporal {
      val begin = base.minus(1)
      val end = base.plus(21)
    }
    val a4 = new Temporal {
      val begin = base.minus(1)
      val end = base.plus(10)
    }

    val begin = new EventAllenStart {
      override val tolerance = Duration.millis(1)
    }

    begin.check(a1, a2) shouldBe true
    begin.check(a1, a3) shouldBe true
    begin.check(a1, a4) shouldBe false
  }

  test("finish") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base.plus(20)
    }
    val a2 = new Temporal {
      val begin = base.minus(1)
      val end = base.plus(21)
    }
    val a3 = new Temporal {
      val begin = base
      val end = base.plus(19)
    }

    val finish = new EventAllenFinish {
      override val tolerance = Duration.millis(1)
    }

    finish.check(a2, a1) shouldBe true
    finish.check(a3, a1) shouldBe false
  }

  test("equals") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base.plus(20)
    }
    val a2 = new Temporal {
      val begin = base.minus(1)
      val end = base.plus(21)
    }

    val equals = new EventAllenEquals {
      override val tolerance = Duration.millis(1)
    }

    equals.check(a1, a2) shouldBe true
  }

  test("during") {
    val base = DateTime.now()
    val a1 = new Temporal {
      val begin = base
      val end = base.plus(20)
    }

    val a2 = new Temporal {
      val begin = base.plus(4)
      val end = base.plus(19)
    }

    val a3 = new Temporal {
      val begin = base.plus(4)
      val end = base.plus(18)
    }

    val a4 = new Temporal {
      val begin = base.plus(3)
      val end = base.plus(19)
    }

    val during = new EventAllenDuring {
      override val beginMargin = Duration.millis(3)
      override val endMargin = Duration.millis(1)
    }

    during.check(a2, a1) shouldBe false
    during.check(a3, a1) shouldBe true
    during.check(a4, a1) shouldBe false
  }
}
