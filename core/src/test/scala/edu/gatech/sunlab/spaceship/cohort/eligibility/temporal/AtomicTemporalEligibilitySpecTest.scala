package edu.gatech.sunlab.spaceship.cohort.eligibility.temporal

import edu.gatech.sunlab.spaceship.cohort.dsl
import org.scalatest.{FunSuite, Matchers}
import dsl._

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/23/16
  */
class AtomicTemporalEligibilitySpecTest extends FunSuite with Matchers {
  test("before") {
    val spec = new BeforeTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec atLeast (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenBefore]
    predicate.asInstanceOf[EventAllenBefore].minDuration shouldBe (20 days)
  }

  test("after") {
    val spec = new AfterTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec atLeast (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenAfter]
    predicate.asInstanceOf[EventAllenAfter].minDuration shouldBe (20 days)
  }

  test("meet") {
    val spec = new MeetTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec tolerate (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenMeet]
    predicate.asInstanceOf[EventAllenMeet].tolerance shouldBe (20 days)
  }

  test("overlap") {
    val spec = new OverlapTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec atLeast (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenOverlap]
    predicate.asInstanceOf[EventAllenOverlap].minDuration shouldBe (20 days)
  }

  test("start") {
    val spec = new StartTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec tolerate (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenStart]
    predicate.asInstanceOf[EventAllenStart].tolerance shouldBe (20 days)
  }

  test("finish") {
    val spec = new FinishTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec tolerate (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenFinish]
    predicate.asInstanceOf[EventAllenFinish].tolerance shouldBe (20 days)
  }

  test("during") {
    val spec = new DuringTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec after (10 days) before (20 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenDuring]
    predicate.asInstanceOf[EventAllenDuring].beginMargin shouldBe (10 days)
  }

  test("equals") {
    val spec = new EqualTemporalEligibilitySpec(null, null, false, false)

    val predicate = spec tolerate (10 days) instantiatePredicate (null, null)

    predicate shouldBe a[EventAllenEquals]
    predicate.asInstanceOf[EventAllenEquals].tolerance shouldBe (10 days)
  }
}
