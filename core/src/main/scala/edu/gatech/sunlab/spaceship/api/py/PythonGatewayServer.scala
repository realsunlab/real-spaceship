package edu.gatech.sunlab.spaceship.api.py

import edu.gatech.sunlab.spaceship.common.util.Logging
import py4j.GatewayServer

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/28/15
  */
object PythonGatewayServer extends Logging {
  def main(args: Array[String]): Unit = {
    val gatewayServer: GatewayServer = new GatewayServer(null)
    gatewayServer.start()

    // Exit on EOF or broken pipe to ensure that this process dies when the Python driver dies:
    while (System.in.read() != -1) {
      // Do nothing
    }

    logDebug("Exiting due to broken pipe from Python driver")
  }
}
