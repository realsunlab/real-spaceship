package edu.gatech.sunlab.spaceship.common

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/1/16
  */
package object model {
  type Events = Stream[Event]
  type Temporals = List[Temporal]
  type PatientId = Long
}
