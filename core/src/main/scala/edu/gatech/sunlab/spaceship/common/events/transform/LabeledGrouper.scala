package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.events.FeatureOperator
import edu.gatech.sunlab.spaceship.common.events.expression.BooleanExpr
import edu.gatech.sunlab.spaceship.common.model.{Event, Events}

/**
  *
  * @author Yu Jing <yu@argcv.com> on 9/8/16
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait LabeledGrouper extends Serializable with FeatureOperator {
  def group(events: Events): Stream[(String, String, Events)]

  /**
    * @param events event stream
    * @param labels root label stream
    * @return extended group stream, events, and group value
    */
  def group(events: Events,
            labels: List[String]): Stream[(List[String], Events, String)] =
    group(events).map(e => (labels :+ s"GROUP_BY(${e._2})", e._3, e._1))
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class BinaryLabeledGrouper(bexpr: BooleanExpr) extends LabeledGrouper {
  lazy val LABEL_OPTION = s"B:${bexpr.toString}"
  lazy val LABEL_OPTION_TRUE = s"$LABEL_OPTION:Y"
  lazy val LABEL_OPTION_FALSE = s"$LABEL_OPTION:N"

  override def group(events: Events): Stream[(String, String, Events)] = {
    events
      .groupBy(e => bexpr.eval(e)._1)
      .map { e =>
        if (e._1) ("Y", LABEL_OPTION_TRUE, e._2)
        else ("N", LABEL_OPTION_FALSE, e._2)
      }
      .toStream
  }

  override def toString: String = LABEL_OPTION

  /**
    * another implementation
    */
  //    override def group(events: Events): Stream[(String, Events)] = {
  //      val iProperty = transformer.toString
  //      new CustomLabeledGrouper(
  //        mapper = new EventGroupMapper {
  //          override def property: String = iProperty
  //          override def map(in: Event): String = transformer.check(in) match {
  //            case true => "Y"
  //            case false => "N"
  //          }
  //        }
  //      ).group(events)
  //    }
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class AttrLabeledGrouper(property: String) extends LabeledGrouper {
  lazy val LABEL_OPTION = s"A:$property"

  override def group(events: Events): Stream[(String, String, Stream[Event])] = {
    events.groupBy { e =>
      e.properties.get(property) match {
        case Some(v) =>
          v.toString
        case None =>
          ""
      }
    }.map(e => (e._1, s"$LABEL_OPTION:${e._1}", e._2)).toStream
  }

  override def toString: String = LABEL_OPTION

  /**
    * another implementation
    */
  //  override def group(events: Events): Stream[(String, Events)] = {
  //    val iProperty = property
  //    new CustomLabeledGrouper(
  //      mapper = new EventGroupMapper {
  //        override def property: String = iProperty
  //        override def map(in: Event): String = in.properties.get(property).getOrElse("").toString
  //      }
  //    ).group(events)
  //  }
}

/**
  * convert on envent to a string label (if it is double or sth else, please )
  *
  * @tparam T one event
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait GroupMapper[T <: Event] {
  def map(in: T): String

  def property: String
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait EventGroupMapper extends GroupMapper[Event]

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class CustomLabeledGrouper(mapper: EventGroupMapper)
    extends LabeledGrouper {
  lazy val LABEL_OPTION = s"C:${mapper.property}"

  override def group(events: Events): Stream[(String, String, Events)] = {
    events.groupBy { e =>
      mapper.map(e)
    }.map(e => (e._1, s"$LABEL_OPTION:${e._1}", e._2)).toStream
  }

  override def toString: String = LABEL_OPTION
}

/**
  * convert on envent to a string label (if it is double or sth else, please )
  *
  * @tparam T one event
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait MultiValueGroupMapper[T <: Event] {
  def map(in: T): List[String]

  def property: String
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait MultiValueEventGroupMapper extends MultiValueGroupMapper[Event]

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class MultiValueCustomLabeledGrouper(mapper: MultiValueEventGroupMapper)
    extends LabeledGrouper {
  lazy val LABEL_OPTION = s"M:${mapper.property}"

  // origin key, key with label, value
  override def group(events: Events): Stream[(String, String, Events)] = {
    events
      .flatMap(e => mapper.map(e).map(f => (f, e)))
      .groupBy { e =>
        e._1
      }
      .map(kv => (kv._1, s"$LABEL_OPTION:${kv._1}", kv._2.map(_._2)))
      .toStream
  }

  override def toString: String = LABEL_OPTION
}
