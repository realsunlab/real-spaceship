package edu.gatech.sunlab.spaceship.cohort.index

import edu.gatech.sunlab.spaceship.common.events.HigherOrderEventExtractor
import edu.gatech.sunlab.spaceship.common.model.SamplePatient
import org.joda.time.{Duration, DateTime}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait IndexAssigner extends Serializable {
  def apply(sp: SamplePatient): Option[SamplePatient]
}

class FixedDateIndexAssigner(id: DateTime) extends IndexAssigner {
  def apply(sp: SamplePatient): Option[SamplePatient] =
    Some(sp.withIndexDate(id))
}

class FixedOffsetIndexAssigner(du: Duration) extends IndexAssigner {
  def apply(sp: SamplePatient): Option[SamplePatient] =
    Some(sp.withIndexDate(sp.targetDate.minus(du)))
}

/**
  * User first occurrence of event as index
  * @param higherOrderEventExtractor parameterized by target date
  */
class EventIndexAssigner(higherOrderEventExtractor: HigherOrderEventExtractor)
    extends IndexAssigner {
  def apply(sp: SamplePatient): Option[SamplePatient] = {
    val eventExtractor = higherOrderEventExtractor.apply(List(sp.targetDate))
    val events = eventExtractor(sp.p)
    events.headOption.map(e => sp.withIndexDate(e.begin))
  }
}
