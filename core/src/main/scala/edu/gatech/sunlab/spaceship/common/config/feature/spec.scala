package edu.gatech.sunlab.spaceship.common.config.feature
import org.json4s._
import org.json4s.jackson.JsonMethods._

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/28/15
  */
abstract class FeatureSpec

case class EventFeatureSpec(group: String,
                            name: Option[String],
                            operation: String,
                            window: Int)
    extends FeatureSpec

case class AttributeFeatureSpec(attribute: String, operation: Option[String])
    extends FeatureSpec

object FeatureSpec {
  implicit val formats: Formats = org.json4s.DefaultFormats

  def fromJson(spec: String): List[FeatureSpec] = {
    val json = parse(spec)
    val eventFeatureSpecsJson = json \ "event"
    val attributeFeatureSpecsJson = json \ "attribute"

    val eventFeatures = if (eventFeatureSpecsJson != JNothing) {
      val JArray(eventFeatureSpecs) = eventFeatureSpecsJson
      eventFeatureSpecs.map(_.extract[EventFeatureSpec])
    } else List.empty[FeatureSpec]

    val attributeFeatures = if (attributeFeatureSpecsJson != JNothing) {
      val JArray(attributeFeatureSpecs) = attributeFeatureSpecsJson
      attributeFeatureSpecs.map(_.extract[AttributeFeatureSpec])
    } else List.empty[FeatureSpec]

    eventFeatures ::: attributeFeatures
  }
}
