package edu.gatech.sunlab.spaceship.common.util

import org.joda.time.{Duration, Period, DateTime, Interval}

/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
class TimeWindow(start: DateTime,
                 end: DateTime,
                 closeStart: Boolean = true,
                 closeEnd: Boolean = true)
    extends Serializable {

  def toInterval: Interval = {
    require(start != null && end != null)
    new Interval(start, end)
  }

  def contains(dateTime: DateTime): Boolean = {
    (
      start == null || dateTime.isAfter(start) ||
      (closeStart && dateTime.isEqual(start))
    ) && (
      end == null || dateTime.isBefore(end) ||
      (closeEnd && dateTime.isEqual(end))
    )
  }

  def intersect(interval: Interval): Option[Interval] = {
    if (end != null && interval.getStart.isAfter(end)) return None
    if (end != null && !closeEnd && interval.getStart == end) return None

    if (start != null && interval.getEnd.isBefore(start)) return None
    if (start != null && !closeStart && interval.getEnd == start) return None

    val begin =
      if (start == null || start.isBefore(interval.getStart)) interval.getStart
      else start
    val stop =
      if (end == null || end.isAfter(interval.getEnd)) interval.getEnd else end

    Some(new Interval(begin, stop))
  }

  override def toString = {
    val startBracket = if (closeStart) "[" else "("
    val endBracket = if (closeEnd) "]" else ")"
    s"$startBracket$start, $end$endBracket"
  }
}

object UnboundTimeWindow extends TimeWindow(null, null) {
  override def contains(dateTime: DateTime) = true
}

object TimeWindow {
  def before(dateTime: DateTime,
             duration: Duration = null,
             closeStart: Boolean = true) =
    new TimeWindow(if (duration == null) null else dateTime.minus(duration),
                   dateTime,
                   closeStart,
                   false)

  def beforeIncl(dateTime: DateTime,
                 duration: Duration = null,
                 closeStart: Boolean = true) =
    new TimeWindow(if (duration == null) null else dateTime.minus(duration),
                   dateTime,
                   closeStart)

  def after(dateTime: DateTime,
            duration: Duration = null,
            closeEnd: Boolean = true) =
    new TimeWindow(dateTime,
                   if (duration == null) null else dateTime.plus(duration),
                   false,
                   closeEnd)

  def afterIncl(dateTime: DateTime,
                duration: Duration = null,
                closeEnd: Boolean = true) =
    new TimeWindow(dateTime,
                   if (duration == null) null else dateTime.plus(duration),
                   true,
                   closeEnd)
}
