package edu.gatech.sunlab.spaceship.common.util

import edu.gatech.sunlab.spaceship.common.model._
import edu.gatech.sunlab.spaceship.common.util.collection.{
  GroupedStream,
  MergedEvents
}

/**
  * Created by hang on 7/14/16.
  */
object Events {
  def empty: Stream[Event] = Stream.empty[Event]

  def merge(ls: Events, rs: Events): Events = new MergedEvents(ls, rs)

  def group[T](self: Stream[T], size: Int): GroupedStream[T] =
    new GroupedStream[T](self, size, size)

  def sliding[T](self: Stream[T], size: Int): GroupedStream[T] =
    new GroupedStream[T](self, size, 1)
}
