//package edu.gatech.sunlab.spaceship.common.util
//
//import org.joda.time.Duration
//
//import scala.reflect.ClassTag
//import scala.util.matching.Regex
//
///**
//  * @author Hang Su <hangsu@gatech.edu>
//  *         Created on 6/14/16
//  */
//// Predicates
//@deprecated("use expression instead")
//@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
//abstract class Predicate extends (Any => Boolean) with Serializable with Logging
//
//class InPredicate(choices: Any*) extends Predicate {
//  override def apply(self: Any): Boolean = choices.contains(self)
//
//  override def toString: String = s"=?= $choices"
//}
//
//class RegexPredicate(pattern: String) extends Predicate {
//  private val regex = new Regex(pattern)
//  override def apply(v1: Any): Boolean = regex.pattern.matcher(v1.toString).matches()
//
//  override def toString: String = s"=~= $pattern"
//}
//
//abstract class OrderingPredicate(val other: Any) extends Predicate {
//  logDebug(s"Predicate compare with $other of class ${ClassTag(other.getClass)}")
//
//  protected val (ordering: Ordering[Any], classTag, num: Boolean) = other match {
//    case _: Double =>
//      (Ordering[Double].asInstanceOf[Ordering[Any]], ClassTag(other.getClass), true)
//    case _: Int =>
//      (Ordering[Int].asInstanceOf[Ordering[Any]], ClassTag(other.getClass), true)
//    case _: String =>
//      (Ordering[String].asInstanceOf[Ordering[Any]], ClassTag(other.getClass), false)
//    // Ordering[Int].asInstanceOf[Ordering[Any]] is work around to avoid null
//    case _: Boolean =>
//      (Ordering[Int].asInstanceOf[Ordering[Any]], ClassTag(other.getClass), true)
//    case _: Duration =>
//      (new Ordering[Duration]{
//        override def compare(x: Duration, y: Duration): Int = x.compareTo(y)
//      }.asInstanceOf[Ordering[Any]], ClassTag(other.getClass), false)
//    case unknown =>
//      sys.error(s"Type ${unknown.getClass} doesn't support ordering")
//  }
//
//  protected def safeConvert(self: Any): Any = other match {
//    case _: Double  => self.asInstanceOf[Number].doubleValue()
//    case _: Int     => self.asInstanceOf[Number].intValue()
//    case _: String  => self.toString
//    case _: Boolean => self.asInstanceOf[Boolean]
//    case _: Duration => self.asInstanceOf[Duration]
//    case _          => null
//  }
//}
