package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.events.{
  AllEventExtractor,
  EventExtractor,
  FeatureOperator
}
import edu.gatech.sunlab.spaceship.common.model.Events
import edu.gatech.sunlab.spaceship.common.util.Logging

import scala.language.implicitConversions

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/1/16
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait Transformer extends Serializable with Logging with FeatureOperator {
  def apply(in: Events): Events

  def toExtractor: EventExtractor = { AllEventExtractor | this }

  override def toString: String = getClass.getSimpleName
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait HigherOrderTransformer extends Serializable {
  def apply(param: List[Any]): Transformer

  /**
    * whether need call {{apply}} with valid param to get transformer
    * if false, one can pass `null` to {{apply}} since it's not used
    *
    * @return
    */
  def dependent: Boolean = true
}

object Debug extends Transformer {
  def apply(in: Events): Events = {
    in.map {
      case e =>
        logDebug(s"checked ${e.toString}")
        e
    }
  }

  override def toString: String = "Debug"
}

object HigherOrderTransformer {
  implicit def transformerToHigherOrder(
      t: Transformer): HigherOrderTransformer =
    new HigherOrderTransformer {
      override def apply(param: List[Any]): Transformer = t
    }
}
