package edu.gatech.sunlab.spaceship.common.util.collection

import scala.collection.Seq
import scala.collection.immutable.Stream

/**
  * Created by hang on 7/20/16.
  */
class GroupedStream[B](self: Stream[B], size: Int, step: Int)
    extends Stream[Seq[B]] {
  require(size >= 1 && step >= 1,
          "size=%d and step=%d, but both must be positive".format(size, step))

  override def isEmpty = self.isEmpty

  override def head: Seq[B] = self.take(size)

  @volatile private[this] var tlVal: Stream[Seq[B]] = _

  override protected def tailDefined: Boolean = tlVal ne null

  override def tail: Stream[Seq[B]] = {
    if (!tailDefined)
      synchronized {
        if (!tailDefined)
          tlVal = new GroupedStream[B](self.drop(step), size, step)
      }

    tlVal
  }
}
