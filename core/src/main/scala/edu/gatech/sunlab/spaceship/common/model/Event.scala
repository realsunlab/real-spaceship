/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
package edu.gatech.sunlab.spaceship.common.model

import java.io.Serializable

import scala.collection.JavaConversions._
import it.unimi.dsi.fastutil.objects.{
  Object2IntArrayMap,
  Reference2IntArrayMap
}
import org.joda.time.{Duration, DateTime, Interval}

/**
  * Start definition of event
  */
trait Temporal {
  def begin: DateTime

  def end: DateTime
}

class Properties(kv: Seq[(String, Any)]) extends Serializable {

  val name2index =
    new Object2IntArrayMap[String](kv.map(_._1).toArray, kv.indices.toArray)
  name2index.defaultReturnValue(-1)
  val values = kv.map(_._2)

  def apply(name: String): Any = {
    this.get(name).get
  }

  def get(name: String): Option[Any] = {
    val index = name2index.getInt(name)
    if (index != -1) Some(values(index)) else None
  }

  def kvs: Seq[(String, Any)] = {
    name2index
      .entrySet()
      .map(entry => (entry.getKey, values(entry.getValue)))
      .toSeq
  }

  def +(pairs: (String, Any)*): Properties = {
    new Properties((kvs ++ pairs).groupBy(_._1).map(_._2.last).toSeq)
  }

  override def toString: String = kvs.toString()
}

class Event(val patientId: Int,
            val concept: String,
            _begin: DateTime,
            _end: DateTime,
            _properties: Seq[(String, Any)])
    extends Serializable
    with Temporal {
  /* extend properties to facilitate filtering */
//  val properties = _properties +()
  val properties = new Properties(
    _properties ++ Seq("concept" -> concept,
                       "bt" -> _begin,
                       "et" -> _end,
                       "duration" -> (if (_begin == null || _end == null)
                                        Duration.ZERO
                                      else new Duration(_begin, _end))))

  def this(patientId: Int, concept: String, begin: DateTime, end: DateTime) =
    this(patientId, concept, begin, end, Seq.empty)

  def this(patientId: Int, concept: String, begin: DateTime) =
    this(patientId, concept, begin, begin)

  def copy(patientId: Int = patientId,
           concept: String = concept,
           begin: DateTime = _begin,
           end: DateTime = end,
           props: Properties = properties) =
    new Event(patientId, concept, begin, end, removeSpecialFields(props.kvs))

  def isInstant: Boolean =
    _begin != null && (_end == null || _begin.equals(_end))

  def begin = _begin

  def end = if (_end == null) _begin else _end

  def interval: Option[Interval] =
    if (isInstant) None else Some(new Interval(begin, end))

  override def toString: String = {
    new StringBuilder()
      .append("Event(")
      .append(patientId)
      .append(' ')
      .append(properties)
      .append(")")
      .toString()
  }

  private def removeSpecialFields(props: Seq[(String, Any)]) = props.filter {
    case (n, _) => n != "concept" && n != "bt" && n != "et"
  }
}
