package edu.gatech.sunlab.spaceship.cohort.eligibility.temporal

import java.util.UUID

import edu.gatech.sunlab.spaceship.common.model.{Event, Events, Temporal}
import edu.gatech.sunlab.spaceship.common.util.DurationHelper
import org.joda.time.Duration

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/16/16
  */
trait TemporalPredicate {
  def eval(eventsContext: EventsContext): (Boolean, Map[UUID, Events])

  def and(other: TemporalPredicate): TemporalPredicate =
    new AndTemporalPredicate(this, other)

  def or(other: TemporalPredicate): TemporalPredicate =
    new OrTemporalPredicate(this, other)
}

class AndTemporalPredicate(left: TemporalPredicate, right: TemporalPredicate)
    extends TemporalPredicate {
  override def eval(
      eventsContext: EventsContext): (Boolean, Map[UUID, Events]) = {
    val (leftResult, leftEvents) = left.eval(eventsContext)
    /* early stop */
    if (!leftResult)
      return (leftResult, Map.empty)

    val (rightResult, rightEvents) = right.eval(eventsContext)

    val result = leftResult && rightResult
    val events =
      (leftEvents.toSeq ++ rightEvents.toSeq).groupBy(_._1).mapValues {
        case Seq(first, second) =>
          first._2.intersect(second._2) // both contains
        case Seq(first) => first._2 // one side contains
      }
    (result, events)
  }

  override def toString: String = s"[$left T-AND $right]"
}

class OrTemporalPredicate(left: TemporalPredicate, right: TemporalPredicate)
    extends TemporalPredicate {
  override def eval(
      eventsContext: EventsContext): (Boolean, Map[UUID, Events]) = {
    val (leftResult, leftEvents) = left.eval(eventsContext)
    /* early stop */
    if (!leftResult)
      return (leftResult, Map.empty)

    val (rightResult, rightEvents) = right.eval(eventsContext)

    val result = leftResult && rightResult
    val events =
      (leftEvents.toSeq ++ rightEvents.toSeq).groupBy(_._1).mapValues {
        case Seq(first, second) =>
          first._2.union(second._2) // any side contains
        case Seq(first) => first._2 // one side contains
      }
    (result, events)
  }

  override def toString: String = s"($left T-OR $right)"
}

trait EventAllenLogic {
  def check(a: Temporal, b: Temporal): Boolean
  def logicString: String
}

abstract class AtomicTemporalPredicate(leftEventsId: UUID,
                                       rightEventsId: UUID,
                                       noLeft: Boolean = false,
                                       noRight: Boolean = false)
    extends TemporalPredicate { self: EventAllenLogic =>

  override def eval(
      eventsContext: EventsContext): (Boolean, Map[UUID, Events]) = {
    val left = eventsContext.get(leftEventsId)
    val right = eventsContext.get(rightEventsId)

    val (processedLeftEvents, processedRightEvents) = events(left, right)
    val result = processedLeftEvents.nonEmpty && processedRightEvents.nonEmpty
    val id2events = Map(leftEventsId -> processedLeftEvents,
                        rightEventsId -> processedRightEvents)

    (result, id2events)
  }

  def events(left: Events, right: Events): (Events, Events) =
    (noLeft, noRight) match {
      case (false, false) =>
        val pairs = left.flatMap(a => right.map(b => (a, b))).filter {
          case (a, b) => check(a, b)
        }
        val leftEvents = pairs.map(_._1).distinct
        val rightEvents = pairs.map(_._2).distinct
        (leftEvents, rightEvents)

      case (true, false) =>
        val rightEvents = right.filter(b => left.forall(check(_, b)))
        (left, rightEvents)

      case (false, true) =>
        val leftEvents = left.filter(a => right.forall(check(a, _)))
        (leftEvents, right)

      case (true, true) => sys.error("invalid: both side non-exist")
    }

  override def toString: String = {
    val left = if (noLeft) s"!$leftEventsId" else leftEventsId.toString
    val right = if (noRight) s"!$rightEventsId" else rightEventsId.toString

    s"($left #$logicString# $right)"
  }
}

// Allen's interval algebra with parameters
trait EventAllenBefore extends EventAllenLogic {
  val maxDuration: Option[Duration] = None
  val minDuration: Duration = Duration.ZERO

  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.end != null && b.begin != null, "timestamp N/A")

    val difference = new Duration(a.end, b.begin)

    val satisfyMin = difference.compareTo(minDuration) > 0
    val satisfyMax =
      if (maxDuration.isDefined) maxDuration.get.compareTo(difference) >= 0
      else true

    satisfyMin && satisfyMax
  }

  override def logicString: String =
    s"Before <= ${DurationHelper.prettyPrint(maxDuration)} > ${DurationHelper.prettyPrint(minDuration)}"
}

trait EventAllenAfter extends EventAllenBefore {
  override def check(a: Temporal, b: Temporal) = super.check(b, a)

  override def logicString: String =
    s"After <= ${DurationHelper.prettyPrint(maxDuration)} > ${DurationHelper.prettyPrint(minDuration)}"
}

trait EventAllenMeet extends EventAllenLogic {
  val tolerance: Duration = Duration.ZERO
  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.begin != null && b.begin != null, "timestamp N/A")

    val du = new Duration(a.end, b.begin)

    Math.abs(du.getMillis) <= tolerance.getMillis && a.end.compareTo(b.end) < 0
  }

  override def logicString: String =
    s"Meet <= ${DurationHelper.prettyPrint(tolerance)}"
}

trait EventAllenOverlap extends EventAllenLogic {
  val minDuration: Duration = Duration.ZERO

  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.begin != null && b.begin != null, "timestamp N/A")

    val difference = new Duration(b.begin, a.end)

    difference.compareTo(minDuration) >= 0 && a.begin.compareTo(b.begin) <= 0 && a.end
      .compareTo(b.end) <= 0
  }

  override def logicString: String =
    s"Overlap >= ${DurationHelper.prettyPrint(minDuration)}"
}

trait EventAllenStart extends EventAllenLogic {
  val tolerance: Duration = Duration.ZERO

  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.begin != null && b.begin != null, "timestamp N/A")

    val startDu = new Duration(a.begin, b.begin)

    Math.abs(startDu.getMillis) <= tolerance.getMillis && a.end.compareTo(
      b.end) < 0
  }

  override def logicString: String =
    s"Start <= ${DurationHelper.prettyPrint(tolerance)}"
}

trait EventAllenFinish extends EventAllenLogic {
  val tolerance: Duration = Duration.ZERO

  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.begin != null && b.begin != null, "timestamp N/A")

    val finishDu = new Duration(a.end, b.end)

    Math.abs(finishDu.getMillis) <= tolerance.getMillis && a.begin.compareTo(
      b.begin) < 0
  }

  override def logicString: String =
    s"Finish <= ${DurationHelper.prettyPrint(tolerance)}"
}

trait EventAllenDuring extends EventAllenLogic {
  val beginMargin: Duration = Duration.ZERO
  val endMargin: Duration = Duration.ZERO

  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.begin != null && b.begin != null, "timestamp N/A")
    b.begin.plus(beginMargin).compareTo(a.begin) < 0 &&
    b.end.minus(endMargin).compareTo(a.end) > 0
  }

  override def logicString: String =
    s"During margin > ${DurationHelper.prettyPrint(beginMargin)}, ${DurationHelper.prettyPrint(endMargin)}"
}

trait EventAllenEquals extends EventAllenLogic {
  val tolerance: Duration = Duration.ZERO

  override def check(a: Temporal, b: Temporal): Boolean = {
    require(a.begin != null && b.begin != null, "timestamp N/A")

    val beginDu = new Duration(a.begin, b.begin)
    val endDu = new Duration(a.end, b.end)

    Math.abs(beginDu.getMillis) <= tolerance.getMillis && Math.abs(
      endDu.getMillis) <= tolerance.getMillis
  }

  override def logicString: String =
    s"Equals <= ${DurationHelper.prettyPrint(tolerance)}"
}
