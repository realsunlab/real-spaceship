package edu.gatech.sunlab.spaceship.common.io

import java.text.SimpleDateFormat

import edu.gatech.sunlab.spaceship.common.model.Event
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime
import org.json4s.Formats
import org.json4s._
import org.json4s.jackson.JsonMethods._

abstract class EventLoader {
  def load(): RDD[Event]
}

object EventJsonParser {
  implicit val formats: Formats = new org.json4s.DefaultFormats {
    override def dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  } ++ org.json4s.ext.JodaTimeSerializers.all

  def apply(line: String): Event = {
    val json = parse(line)

    //    required
    val patientId = (json \ "pid").extract[Int]
    val concept = (json \ "concept").extract[String]

    //    optional
    val begin = (json \ "bt").extractOrElse[DateTime](null)
    val end = (json \ "et").extractOrElse[DateTime](null)

    // tags
    val fields = json.values.asInstanceOf[Map[String, Any]]
    val dateTimePattern = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}".r
    val properties = fields
      .filterKeys(key => !Set("pid", "concept", "bt", "et").contains(key))
      .mapValues {
        case s: String =>
          if (dateTimePattern.findFirstIn(s).isDefined) DateTime.parse(s)
          else s
        case other => other
      }
      .toSeq

    new Event(patientId = patientId,
              concept = concept,
              _begin = begin,
              _end = end,
              _properties = properties)
  }
}

class JsonFileEventLoader(path: String, sc: SparkContext) extends EventLoader {
  def load(): RDD[Event] = {
    sc.textFile(path).map(EventJsonParser.apply)
  }
}
