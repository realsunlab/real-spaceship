package edu.gatech.sunlab.spaceship.common.util.collection

import edu.gatech.sunlab.spaceship.common.model.{Event, Events}

/**
  * Created by hang on 7/20/16.
  */
class MergedEvents(left: Events, right: Events) extends Events {
  override def isEmpty = left.isEmpty && right.isEmpty

  override def head: Event = {
    if (left.isEmpty) right.head
    else if (right.isEmpty) left.head
    else if (left.head.begin.compareTo(right.head.begin) <= 0) left.head
    else right.head
  }

  @volatile private[this] var tlVal: Events = _

  override protected def tailDefined: Boolean = tlVal ne null

  override def tail: Events = {
    if (!tailDefined)
      synchronized {
        if (!tailDefined) tlVal = calcTail
      }

    tlVal
  }

  private def calcTail: Events = {
    if (left.isEmpty) right.tail
    else if (right.isEmpty) left.tail
    else if (right.head eq left.head) new MergedEvents(left.tail, right.tail)
    else if (left.head.begin.compareTo(right.head.begin) <= 0)
      new MergedEvents(left.tail, right)
    else new MergedEvents(left, right.tail)
  }
}
