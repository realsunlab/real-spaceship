package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.model.Events
import edu.gatech.sunlab.spaceship.common.util.{Events, TimeWindow}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/6/16
  */
class NthSubListTransformer(idx: Int) extends Transformer {
  override def apply(in: Events): Events = in.slice(idx - 1, idx)
}

class FirstNSubListTransformer(n: Int) extends Transformer {
  override def apply(in: Events): Events = in.slice(0, n)
}

class LastNSubListTransformer(n: Int) extends Transformer {
  override def apply(in: Events): Events = in.slice(in.length - n, in.length)
}

class TimeWindowSubListTransformation(timeWindow: TimeWindow)
    extends Transformer {
  override def apply(in: Events): Events = {
    in.filter(e => timeWindow.contains(e.begin))
  }

  override def toString: String = timeWindow.toString
}
