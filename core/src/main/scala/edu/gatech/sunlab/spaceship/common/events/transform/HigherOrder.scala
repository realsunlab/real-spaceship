package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.model.Event
import edu.gatech.sunlab.spaceship.common.util.{
  DurationHelper,
  DateTimeGranularity,
  TimeWindow
}
import org.joda.time.{DateTime, Duration}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
object Within {
  def apply(beginOffset: Duration,
            endOffset: Duration,
            beginIncl: Boolean = false,
            endIncl: Boolean = false) = new HigherOrderTransformer {
    override def apply(param: List[Any]): Transformer = {
      val current = param.head.asInstanceOf[DateTime]
      val beginDateTime =
        if (beginOffset != null) current.plus(beginOffset) else null
      val endDateTime =
        if (endOffset != null) current.plus(endOffset) else null

      val timeWindow =
        new TimeWindow(beginDateTime, endDateTime, beginIncl, endIncl)
      new TimeWindowSubListTransformation(timeWindow)
    }
  }
}

object Offset {
  def apply(prop: String = "value",
            out: String = "value",
            granularity: DateTimeGranularity.Value): HigherOrderTransformer = {

    new HigherOrderTransformer {
      override def apply(param: List[Any]): Transformer = {
        new ProjectTransformer {
          override def project(in: Event): Event = {
            val end = param.head.asInstanceOf[DateTime]
            val begin = in.properties.get(prop).map(_.asInstanceOf[DateTime])

            if (begin.isEmpty) {
              sys.error(s"failed to find $prop:DateTime in $in")
            }

            val duration =
              DurationHelper.toInt(new Duration(begin.get, end), granularity)

            in.copy(props = in.properties + (out -> duration))
          }
        }
      }
    }
  }
}
