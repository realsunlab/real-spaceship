package edu.gatech.sunlab.spaceship.common.events.expression

import edu.gatech.sunlab.spaceship.common.model.Event
import org.joda.time.{Duration, DateTime}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/12/16
  */
abstract class ArithmeticExpr(left: Expression, right: Expression)
    extends BinaryExpr(left, right) {
  override def eval(e: Event): (Any, DataType) = {
    val (leftVal, leftTyp) = left.eval(e)
    val (rightVal, rightTyp) = right.eval(e)

    (leftTyp, rightTyp) match {
      case (a, b) if a == NullType || b == NullType => (null, NullType)
      case (_: NumericType, _: NumericType) =>
        val (newLeftVal, newRightVal, outputType) =
          toSameType(leftTyp, leftVal, rightTyp, rightVal)
        val outputVal =
          evalNum(outputType.asInstanceOf, newLeftVal, newRightVal)
        (outputVal, outputType)

      case (_: DateTimeType, _: DurationType) =>
        val outputType = DataTypes.DateTime
        val outputVal = evalDate(leftVal, rightVal)
        (outputVal, outputType)

      case _ => sys.error("unsupported")
    }
  }

  def evalNum(typ: NumericType, leftVal: Any, rightVal: Any): Any

  def evalDate(leftVal: Any, rightVal: Any): Any = {
    sys.error("not implemented")
  }
}

class AddExpr(left: Expression, right: Expression)
    extends ArithmeticExpr(left, right) {
  override def evalNum(typ: NumericType, leftVal: Any, rightVal: Any): Any = {
    typ.numeric.asInstanceOf[Numeric[Any]].plus(leftVal, rightVal)
  }

  override def evalDate(leftVal: Any, rightVal: Any): Any = {
    leftVal.asInstanceOf[DateTime].plus(rightVal.asInstanceOf[Duration])
  }

  override def toString = s"$left + $right"
}

class MinusExpr(left: Expression, right: Expression)
    extends ArithmeticExpr(left, right) {
  override def evalNum(typ: NumericType, leftVal: Any, rightVal: Any): Any = {
    typ.numeric.asInstanceOf[Numeric[Any]].minus(leftVal, rightVal)
  }

  override def evalDate(leftVal: Any, rightVal: Any): Any = {
    leftVal.asInstanceOf[DateTime].minus(rightVal.asInstanceOf[Duration])
  }

  override def toString = s"$left - $right"
}
