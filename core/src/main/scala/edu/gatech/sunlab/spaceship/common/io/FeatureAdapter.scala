package edu.gatech.sunlab.spaceship.common.io

import java.io.{BufferedWriter, File, FileWriter}

import edu.gatech.sunlab.spaceship.common.model.{
  FeatureConstructionResult,
  LabeledPointWithId
}
import edu.gatech.sunlab.spaceship.common.util.Logging

import scala.collection.mutable

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 11/1/15
  */
object FeatureAdapter extends Logging {

  /**
    *
    * @param featureResult feature result
    * @param labelPath label path, it will saved as a single file
    * @param featurePath feature path, it will saved as a directory
    */
  def save(featureResult: FeatureConstructionResult,
           labelPath: String = "output/label",
           featurePath: String = "output/feature"): Unit = {
    // features mush be saved as

    val nFeatures = featureResult.mapping.size

    logInfo(s"Total features is $nFeatures")

    val labelWriter = new BufferedWriter(new FileWriter(new File(labelPath)))
    featureResult.mapping.toList.sortWith(_._2 < _._2).foreach { iv =>
      labelWriter
        .append(iv._1)
        .append(",")
        .append(iv._2.toString)
        .append("\t")
        .append("\n")
    }
    labelWriter.flush()
    labelWriter.close()

    featureResult.features.cache()

    val dataStrs = featureResult.features.map { point =>
      val buffer = new StringBuffer
      buffer.append(point.label.toString).append(" ")
      val len = point.features.size
      (0 until len).foreach { i =>
        val v: Double = point.features(i)
        if (v != 0.0) {
          buffer.append(s"$i:$v ")
        }
      }
      //buffer.append(" #").append(point.id).toString
      buffer.toString.trim
    }
    dataStrs.saveAsTextFile(featurePath)
  }
}
