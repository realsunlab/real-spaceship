package edu.gatech.sunlab.spaceship.cohort.target

import java.io.Serializable

import edu.gatech.sunlab.spaceship.common.events.EventExtractor
import edu.gatech.sunlab.spaceship.common.model.{Patient, SamplePatient}
import edu.gatech.sunlab.spaceship.common.util.Logging

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 11/3/15
  */
abstract class TargetAssigner extends Serializable with Logging {
  def apply(p: Patient): List[SamplePatient]
}

class OccurrenceTargetAssigner(caseTargetEventExtractor: EventExtractor,
                               ctrlTargetEventExtractor: EventExtractor)
    extends TargetAssigner {

  def apply(p: Patient): List[SamplePatient] = {
    val asCase = tryAsCase(p)
    if (asCase.isEmpty) {
      tryAsControl(p)
    } else {
      asCase
    }
  }

  def tryAsCase(p: Patient): List[SamplePatient] = {
    val targetEvents = caseTargetEventExtractor(p)
    targetEvents.map(e => new SamplePatient(p, null, e.begin, 1)).toList
  }

  def tryAsControl(p: Patient): List[SamplePatient] = {
    val targetEvents = ctrlTargetEventExtractor(p)
    targetEvents.map(e => new SamplePatient(p, null, e.begin, 0)).toList
  }
}
