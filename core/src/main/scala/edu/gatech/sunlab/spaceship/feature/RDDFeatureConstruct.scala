package edu.gatech.sunlab.spaceship.feature

import edu.gatech.sunlab.spaceship.common.model._
import edu.gatech.sunlab.spaceship.common.util.Logging
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.rdd.RDD

import scala.language.postfixOps

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/4/15
  */
object RDDFeatureConstruct extends Logging {
  //  def exec(patientRDD: RDD[SamplePatient], minSupport: Int = 2)(
  //      body: (FeatureExecutor) => FeatureExecutor = e => e)
  //    : FeatureConstructionResult = {
  //    apply(patientRDD, minSupport)(p => !body(patientEventConv(p)))
  //  }

  def apply(patientRDD: RDD[SamplePatient], minSupport: Int = 2)(
      body: (SamplePatient) => (Stream[FeatureNameValue],
                                List[(String, String)]))
    : (FeatureConstructionResult, RDD[(String, List[String])]) = {

    logInfo("[RDDFeatureConstruct] starting...")

    logInfo("[RDDFeatureConstruct] target-with-name-features extracting ...")
    //RDD: PatientID, Score, FeatureVector
    val extractResults: RDD[
      (PatientId, Double, Stream[FeatureNameValue], List[(String, String)])] =
      patientRDD.map { p =>
        val fs = body(p)
        (p.id, p.targetValue, fs._1, fs._2)
      }.filter(_._3.nonEmpty)

    val targetWithNamedFeatures =
      extractResults.map(e => (e._1, e._2, e._3)).cache()

    logInfo("[RDDFeatureConstruct] target-with-name-features extracted ...")

    logInfo("[RDDFeatureConstruct] feature stats starting")
    val featureStats: RDD[(String, List[String])] = extractResults
      .flatMap(_._4)
      .groupBy(_._1)
      .map(e => (e._1, e._2.map(_._2).toList.distinct))
    logInfo("[RDDFeatureConstruct] feature stats finished..")

    logInfo("[RDDFeatureConstruct] done...")

    logInfo("[RDDFeatureConstruct] sc-min-support broadcasting ...")
    val scMinSupport = patientRDD.sparkContext.broadcast(minSupport)
    logInfo("[RDDFeatureConstruct] sc-min-support broadcasted ...")

    logInfo("[RDDFeatureConstruct] feature-name-id-mapping generating ...")
    val featureNameIdMapping = targetWithNamedFeatures
      .flatMap((e: (PatientId, Double, Stream[FeatureNameValue])) => e._3)
      .map(f => (f.name, 1))
      .reduceByKey(_ + _)
      .filter(_._2 >= scMinSupport.value)
      .map(_._1)
      .zipWithIndex() // slow
      .map(pair => (pair._1, pair._2.toInt))
      .collectAsMap()
      .toMap
    logInfo("[RDDFeatureConstruct] feature-name-id-mapping generated ...")

    logInfo(
      "[RDDFeatureConstruct] sc-feature-name-id-mapping broadcasting ...")
    val scFeatureNameIdMapping: Broadcast[Map[String, Int]] =
      patientRDD.sparkContext.broadcast(featureNameIdMapping)
    logInfo("[RDDFeatureConstruct] sc-feature-name-id-mapping broadcasted ...")

    logInfo("[RDDFeatureConstruct] target-and-features generating ...")
    val targetAndFeatures: RDD[LabeledPointWithId] =
      targetWithNamedFeatures.mapPartitions { iter =>
        val name2id: Map[String, Int] = scFeatureNameIdMapping.value
        val total: Int = name2id.size
        iter.map { (sample: (PatientId, Double, Stream[FeatureNameValue])) =>
          val features: Stream[(Int, Double)] = sample._3
            .filter(pair => name2id.contains(pair.name))
            .map(pair => (name2id(pair.name), pair.value))
            .sortBy(_._1)
          //LabeledPointWithId(sample._1, sample._2, Vectors.sparse(total, features))
          // use *String* as point id,
          LabeledPointWithId(sample._1.toString,
                             sample._2,
                             Vectors.sparse(total, features))
        }
      }
    logInfo("[RDDFeatureConstruct] target-and-features generated ...")

    (new FeatureConstructionResult(targetAndFeatures, featureNameIdMapping),
     featureStats)
  }
}
