package edu.gatech.sunlab.spaceship.common.events

import edu.gatech.sunlab.spaceship.common.events.transform.{
  EventFilterTransformer,
  HigherOrderTransformer,
  Transformer
}
import edu.gatech.sunlab.spaceship.common.model.{Events, Patient}
import edu.gatech.sunlab.spaceship.common.util.Events

import scala.language.implicitConversions

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/7/16
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait EventExtractor extends Serializable { self =>

  def apply(p: Patient): Events

  def andThen(transformer: Transformer) =
    new TransformedExtractor(this, transformer)

  def |(transformer: Transformer) = new TransformedExtractor(this, transformer)
}

object AllEventExtractor extends EventExtractor {
  def apply(p: Patient): Events = p.events.toStream

  override def toString: String = "All"
}

object Concept {
  def apply(concept: String) = new EventExtractor {
    override def apply(p: Patient): Events =
      p.eventsWithConcept(concept).toStream

    override def toString: String = s"Concept == $concept"
  }
}

class TransformedExtractor(previous: EventExtractor, transformer: Transformer)
    extends EventExtractor {
  override def apply(p: Patient) = (previous, transformer) match {
    case (AllEventExtractor, f: EventFilterTransformer) =>
      /* TODO optimize*/
      transformer(previous(p))
    case _ => transformer(previous(p))
  }

  override def toString: String =
    previous.toString + " | " + transformer.toString
}

class UnionEventExtractor(left: EventExtractor, right: EventExtractor)
    extends EventExtractor {
  override def apply(p: Patient): Events =
    Events.merge(left(p), right(p))

  override def toString: String = s"(${left.toString} U ${right.toString})"
}

class IntersectEventExtractor(left: EventExtractor, right: EventExtractor)
    extends EventExtractor {
  override def apply(p: Patient): Events = {
    val leftEvents = left(p)
    val rightEvents = right(p)

    if (leftEvents.isEmpty || rightEvents.isEmpty) Stream.empty
    else Events.merge(leftEvents, rightEvents)
  }

  override def toString: String = s"(${left.toString} inter ${right.toString})"
}

/// Higher order with parameters, i.e. target date

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
abstract class HigherOrderEventExtractor extends Serializable {
  def apply(param: List[Any]): EventExtractor

  def |(transformer: HigherOrderTransformer) =
    new HigherOrderTransformedExtractor(this, transformer)

  def union(other: HigherOrderEventExtractor) =
    new UnionHigherOrderEventExtractor(this, other)

  def intersect(other: HigherOrderEventExtractor) =
    new IntersectHigherOrderEventExtractor(this, other)

  val uuid = java.util.UUID.randomUUID
}

/**
  * Wrapper of EventExtractor as HigherOrder
  * param to apply will be simply dropped
  *
  * @param et {{EventExtractor}} to wrap
  */
class StaticHigherOrderEventExtractor(et: EventExtractor)
    extends HigherOrderEventExtractor {
  override def apply(param: List[Any]): EventExtractor = et

  override def toString: String = et.toString
}

class HigherOrderTransformedExtractor(previous: HigherOrderEventExtractor,
                                      transformer: HigherOrderTransformer)
    extends HigherOrderEventExtractor {
  override def apply(param: List[Any]): EventExtractor =
    new TransformedExtractor(previous.apply(param), transformer.apply(param))

  override def toString: String =
    previous.toString + " | " + transformer.toString
}

class UnionHigherOrderEventExtractor(left: HigherOrderEventExtractor,
                                     right: HigherOrderEventExtractor)
    extends HigherOrderEventExtractor {
  def apply(param: List[Any]): EventExtractor =
    new UnionEventExtractor(left(param), right(param))
}

class IntersectHigherOrderEventExtractor(left: HigherOrderEventExtractor,
                                         right: HigherOrderEventExtractor)
    extends HigherOrderEventExtractor {
  def apply(param: List[Any]): EventExtractor =
    new IntersectEventExtractor(left(param), right(param))
}
