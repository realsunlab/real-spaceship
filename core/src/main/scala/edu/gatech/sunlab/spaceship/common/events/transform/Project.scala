package edu.gatech.sunlab.spaceship.common.events.transform

//import edu.gatech.sunlab.spaceship.common.events.expression.Expression
import edu.gatech.sunlab.spaceship.common.events.expression.Expression
import edu.gatech.sunlab.spaceship.common.model._
import org.joda.time.{DateTime, Duration}

import scala.reflect.runtime.universe._

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/6/16
  */
/**
  * One-one mapping of event, i.e. substitute icd-9 with ccs
  */
abstract class ProjectTransformer extends Transformer {
  override def apply(in: Events): Events = in.map(project)
  def project(in: Event): Event
}

/***
  * select part of the properties
  */
class PartialTagsEventProjection extends ProjectTransformer {
  def project(in: Event) = ???
}

/**
  * Using map to substitute value of tags or concept
  *
  * @param map
  */
class SubstituteEventProjection(map: Map[String, List[_]])
    extends ProjectTransformer {
  def project(in: Event) = ???
}

/**
  * Keep Begin time only
  */
object BeginDateTime extends ProjectTransformer {
  def project(in: Event) = {
    in.copy(end = in.begin)
  }
}

/**
  * Keep end time only
  */
object EndDateTime extends ProjectTransformer {
  def project(in: Event) = {
    in.copy(begin = in.end)
  }
}

/* Expression based projection */

class ExpressionProjection(exprs: Seq[(String, Expression)])
    extends ProjectTransformer {
  def project(in: Event) = {
    val properties = exprs.map {
      case (name, expr) => (name, expr.eval(in)._1)
    }
    val bt = properties
      .find(_._1 == "bt")
      .map(_._2)
      .getOrElse(in.begin)
      .asInstanceOf[DateTime]
    val et = properties
      .find(_._2 == "et")
      .map(_._2)
      .getOrElse(in.end)
      .asInstanceOf[DateTime]
    in.copy(in.patientId, in.concept, bt, et, in.properties + (properties: _*))
  }
}
