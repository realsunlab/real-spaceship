package edu.gatech.sunlab.spaceship.common.events.expression

import org.joda.time.{Duration, DateTime}

import scala.reflect.runtime.universe.{TypeTag, typeTag, typeOf}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/6/16
  */
class DataType {}

abstract class AtomicType extends DataType {
  type InternalType
  val tag: TypeTag[InternalType]
  val ordering: Ordering[InternalType]
}

object AtomicType {
  def dataType[T: TypeTag](a: T): DataType = typeOf[T] match {
    case t if t =:= typeOf[Int] => DataTypes.Integer
    case t if t =:= typeOf[Long] => DataTypes.Long
    case t if t =:= typeOf[Double] => DataTypes.Double
    case t if t =:= typeOf[String] => DataTypes.String
    case t if t =:= typeOf[DateTime] => DataTypes.DateTime
    case t if t =:= typeOf[Duration] => DataTypes.Duration
    case t if t =:= typeOf[Boolean] => DataTypes.Boolean
    case _ => sys.error(s"unrecognized atomic type for ${typeOf[T]}")
  }
}

class BooleanType extends AtomicType {
  type InternalType = Boolean
  val tag = typeTag[InternalType]
  val ordering = implicitly[Ordering[InternalType]]
}

case object BooleanType extends BooleanType

abstract class NumericType extends AtomicType {
  val numeric: Numeric[InternalType]
}

class DoubleType extends NumericType {
  type InternalType = Double
  val tag = typeTag[InternalType]
  val numeric = implicitly[Numeric[InternalType]]
  val ordering = implicitly[Ordering[InternalType]]
}

case object DoubleType extends DoubleType

abstract class IntegralType extends NumericType {
  val integral: Integral[InternalType]
}

class IntegerType extends IntegralType {
  type InternalType = Int
  val tag = typeTag[InternalType]
  val numeric = implicitly[Numeric[Int]]
  val integral = implicitly[Integral[Int]]
  val ordering = implicitly[Ordering[Int]]
}

case object IntegerType extends IntegerType

class LongType extends IntegralType {
  type InternalType = Long
  val tag = typeTag[InternalType]
  val integral = implicitly[Integral[InternalType]]
  val numeric = implicitly[Numeric[InternalType]]
  val ordering = implicitly[Ordering[InternalType]]
}

case object LongType extends LongType

class StringType extends AtomicType {
  type InternalType = String
  val tag = typeTag[String]
  val ordering = implicitly[Ordering[String]]
}

case object StringType extends StringType

class DateTimeType extends AtomicType {
  type InternalType = DateTime
  val tag = typeTag[InternalType]
  val ordering = new Ordering[InternalType] {
    override def compare(x: DateTime, y: DateTime): Int = x.compareTo(y)
  }
}

case object DateTimeType extends DateTimeType

class DurationType extends AtomicType {
  type InternalType = Duration
  val tag = typeTag[InternalType]
  val ordering = new Ordering[InternalType] {
    override def compare(x: Duration, y: Duration): Int = x.compareTo(y)
  }
}

case object DurationType extends DurationType

case class ArrayType(elemType: DataType) extends DataType

case object NullType extends DataType

object DataTypes {
  final val Integer: DataType = IntegerType
  final val Long: DataType = LongType

  final val Double: DataType = DoubleType

  final val String: DataType = StringType

  final val DateTime: DataType = DateTimeType
  final val Duration: DataType = DurationType

  final val Boolean: DataType = BooleanType

  final val Null: DataType = NullType

  /**
    * Lack of pre-defined schema, RTTI is employed here
    * TODO check performance
    */
  def get(value: Any): DataType = value match {
    case null => DataTypes.Null
    case _: String => DataTypes.String
    case _: Int => DataTypes.Integer
    case _: Double => DataTypes.Double
    case _: Long => DataTypes.Long
    case _: Boolean => DataTypes.Boolean
    case _: DateTime => DataTypes.DateTime
    case _: Duration => DataTypes.Duration

    case _ => sys.error(s"failed to recognize type of `$value")
  }
}
