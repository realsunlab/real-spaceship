package edu.gatech.sunlab.spaceship.cohort

import edu.gatech.sunlab.spaceship.cohort.eligibility._
import edu.gatech.sunlab.spaceship.cohort.eligibility.temporal.{
  EqualTemporalEligibilitySpec,
  FinishTemporalEligibilitySpec,
  OverlapTemporalEligibilitySpec,
  StartTemporalEligibilitySpec,
  _
}
import edu.gatech.sunlab.spaceship.common.events.aggregate._
import edu.gatech.sunlab.spaceship.common.events.expression._
import edu.gatech.sunlab.spaceship.common.events.transform.{BeginDateTime, _}
import edu.gatech.sunlab.spaceship.common.events.{
  AllEventExtractor,
  EventExtractor,
  HigherOrderEventExtractor,
  StaticHigherOrderEventExtractor
}
import edu.gatech.sunlab.spaceship.common.model.Event
import edu.gatech.sunlab.spaceship.common.util.{TimeWindow, DateTimeUtils}
import org.joda.time.Duration

import scala.reflect.runtime.universe.TypeTag
import scala.language.implicitConversions

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/16/16
  */
object dsl {
  /*Duration related*/
  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  implicit class stringWithOp(value: String) extends Serializable {
    def to(other: String): Transformer = {
      val bt = DateTimeUtils.tryParse(value).get
      val et = DateTimeUtils.tryParse(other).get

      val tw = new TimeWindow(bt, et)
      new TimeWindowSubListTransformation(tw)
    }
  }

  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  implicit class IntDuration(value: Int) extends Serializable {
    def days: Duration = {
      Duration.standardDays(value)
    }

    def months: Duration = {
      Duration.standardDays(value * 30)
    }

    def years: Duration = {
      Duration.standardDays(value * 365)
    }

    def millis: Duration = Duration.millis(value)
  }

  /* extractor define */
  implicit def transformerToExtractor(t: Transformer): EventExtractor =
    t.toExtractor
  implicit def higherOrderTransformerToHigherOrderExtractor(
      t: HigherOrderTransformer): HigherOrderEventExtractor = {
    AllEventExtractor | t
  }
  implicit def transformerToHigherOrderExtractor(
      t: Transformer): HigherOrderEventExtractor = {
    new StaticHigherOrderEventExtractor(AllEventExtractor | t)
  }
  implicit def eventExtractor2HigherOrder(
      et: EventExtractor): HigherOrderEventExtractor =
    new StaticHigherOrderEventExtractor(et)
// lead to ambiguous conversions, booleanExpr => extractor, boolean => filter both OK
  implicit def booleanExprToHigherOrderExtractor(
      bp: BooleanExpr): HigherOrderEventExtractor =
    new StaticHigherOrderEventExtractor(
      AllEventExtractor | booleanExprToFilter(bp))

  implicit def booleanExprToHigherOrderTransformer(
      bp: BooleanExpr): HigherOrderTransformer =
    HigherOrderTransformer.transformerToHigherOrder(booleanExprToFilter(bp))

  /* expression */
  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  implicit class StringToFieldExprConversion(val sc: StringContext)
      extends Serializable {
    def $(args: Any*): FieldExpr = {
      val name = sc.s(args: _*)
      new FieldExpr(name)
    }
  }

  implicit def literalToExpression[T: TypeTag](v: T): Expression =
    new LiteralExpr[T](v)
//  implicit def literalSeqToExpression[T: TypeTag](v: Seq[T]): Expression = new LiteralExpr[T](v: _*)

  /* filter transformations */
  implicit def booleanExprToFilter(
      bp: BooleanExpr): PropertyFilterTransformer =
    new PropertyFilterTransformer(bp)

  /* aggregation on event criterion */
  def Exists(e: HigherOrderEventExtractor): PatientEligibility =
    new EventExistsEligibility(e)

  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  class AggregatorAndPredicate(eventAggregator: EventAggregator,
                               predicate: PartiallyAppliedCompare)
      extends Serializable {
    def on(higherOrderEventExtractor: HigherOrderEventExtractor) =
      new AggregationEventsEligibility(higherOrderEventExtractor,
                                       eventAggregator,
                                       predicate)
  }

  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  implicit class BooleanExprWithOp(expr: BooleanExpr) extends Serializable {
    def on(higherOrderEventExtractor: HigherOrderEventExtractor) =
      new SingleEventBasedEligibility(higherOrderEventExtractor, expr)
  }

  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  implicit class AggregatorWithOp(a: EventAggregator) extends Serializable {
    def <(other: Any) =
      new AggregatorAndPredicate(a,
                                 new PartiallyAppliedCompare(other)
                                 with TypedCmpLt)

    def ===(other: Any) =
      new AggregatorAndPredicate(a,
                                 new PartiallyAppliedCompare(other)
                                 with TypedCmpEq)

    def >(other: Any) =
      new AggregatorAndPredicate(a,
                                 new PartiallyAppliedCompare(other)
                                 with TypedCmpGt)
  }

  def PROP(field: String = "value"): PropertyValueEventAggregator =
    new PropertyValueEventAggregator(field)

  def SUM(field: String = "value"): SumEventAggregator =
    new SumEventAggregator(field)

  def LENGTH = LengthEventAggregator

  def DURATION = DurationEventAggregator

  /* Group Transformation */
  def Merge: EventGroupTransformer = Merge("size" -> LengthEventAggregator)
  def Merge(fields: (String, EventAggregator)*): EventGroupTransformer =
    new AggregateGroupedEventsGroupTransformer(new WholeGrouper[Event], fields)

  def Overlap: NeighborOverlapEventGroupTransformer =
    Overlap("concept", Duration.ZERO)
  def Overlap(by: String, gap: Duration = Duration.ZERO) =
    new NeighborOverlapEventGroupTransformer(by, gap)

  def Concat: IntervalConcatEventGroupTransformer =
    Concat("concept", Duration.ZERO)
  def Concat(by: String, gap: Duration) =
    new IntervalConcatEventGroupTransformer(by, gap)

  def Grouped(window: Int,
              fields: (String, EventAggregator)*): EventGroupTransformer =
    new AggregateGroupedEventsGroupTransformer(
      new SimpleGrouper[Event](window),
      fields)
  def Sliding(window: Int,
              fields: (String, EventAggregator)*): EventGroupTransformer =
    new AggregateGroupedEventsGroupTransformer(
      new SlidingGrouper[Event](window),
      fields)

  /* Projection transformation */
  def Begin = BeginDateTime
  def Project(exprs: (String, Expression)*): ProjectTransformer =
    new ExpressionProjection(exprs)

  /* subset transformation */
  def First = new FirstNSubListTransformer(1)
  def First(n: Int = 1) = new FirstNSubListTransformer(n)
  def Last = new LastNSubListTransformer(1)
  def Last(n: Int = 1) = new LastNSubListTransformer(n)

  /* temporal */
  /***
    * For parsing !A < B style constrain, which means not exists a \in A s.t. a before b for all b in B
    */
  class NegativeHigherOrderEventExtractor(val real: HigherOrderEventExtractor)
      extends HigherOrderEventExtractor {
    override def apply(param: List[Any]): EventExtractor = ???
  }

  implicit def atomicTemporalEligibilitySpecToEligibility(
      spec: AtomicTemporalEligibilitySpec): TemporalEligibility = spec.build

  @SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
  implicit class HigherOrderEventExtractorWithOp(
      extractor: HigherOrderEventExtractor)
      extends Serializable {
    val (left, noLeft) = extractorAndSign(extractor)

    def before(
        other: HigherOrderEventExtractor): BeforeTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new BeforeTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def after(other: HigherOrderEventExtractor): AfterTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new AfterTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def meets(other: HigherOrderEventExtractor): MeetTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new MeetTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def overlaps(
        other: HigherOrderEventExtractor): OverlapTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new OverlapTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def starts(
        other: HigherOrderEventExtractor): StartTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new StartTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def finishes(
        other: HigherOrderEventExtractor): FinishTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new FinishTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def equalTo(
        other: HigherOrderEventExtractor): EqualTemporalEligibilitySpec = {
      val (right, noRight) = extractorAndSign(other)

      new EqualTemporalEligibilitySpec(left, right, noLeft, noRight)
    }

    def unary_! = new NegativeHigherOrderEventExtractor(extractor)

    private def extractorAndSign(extractor: HigherOrderEventExtractor)
      : (HigherOrderEventExtractor, Boolean) =
      extractor match {
        case a: NegativeHigherOrderEventExtractor => (a.real, true)
        case _ => (extractor, false)
      }
  }

  implicit class EventExtractorWithOp(extractor: EventExtractor)
      extends HigherOrderEventExtractorWithOp(
        eventExtractor2HigherOrder(extractor))

  implicit class TransformerWithTemporalOp(transformer: Transformer)
      extends EventExtractorWithOp(transformer.toExtractor)

  implicit class BooleanExprWithTemporalOp(expr: BooleanExpr)
      extends TransformerWithTemporalOp(booleanExprToFilter(expr))
}
