/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
package edu.gatech.sunlab.spaceship.common.io

import org.json4s.jackson.Serialization
import org.json4s.{Extraction, NoTypeHints, _}

object JsonUtils {
  implicit val formats = Serialization.formats(NoTypeHints)
  def encodeJson(src: AnyRef): JValue = {
    Extraction.decompose(src)
  }
}
