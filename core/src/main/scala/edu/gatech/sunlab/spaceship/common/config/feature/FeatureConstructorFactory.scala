//package edu.gatech.sunlab.spaceship.common.config.feature
//
//import com.typesafe.scalalogging.slf4j.LazyLogging
//import edu.gatech.sunlab.spaceship.common.events.aggregate.{EventAggregator, AverageDurationAggregator}
//import edu.gatech.sunlab.spaceship.feature.{AttributeDefaultConstructor, CompositeFeatureConstructor, EventAggregationConstructor, FeatureConstructor}
//import org.joda.time.Duration
//
///**
// * @author Hang Su <hangsu@gatech.edu>
// *         Created on 9/28/15
// */
//
//object FeatureConstructorFactory extends LazyLogging {
//  def apply(specs: List[FeatureSpec]): Option[FeatureConstructor] = {
//    val validConstructors = specs.flatMap(toFeatureConstructor)
//
//    if (validConstructors.nonEmpty) Some(new CompositeFeatureConstructor(validConstructors)) else None
//  }
//
//  def toFeatureConstructor(spec: FeatureSpec): Option[FeatureConstructor] = spec match {
//    case EventFeatureSpec(group, _, operation, window) if EventAggregator.apply.isDefinedAt(operation) =>
//      val aggregator: EventAggregator = EventAggregator.apply(operation)
//      Some(new EventAggregationConstructor(Duration.standardDays(window), group, aggregator))
//
//    case AttributeFeatureSpec(attribute, None) =>
//      Some(new AttributeDefaultConstructor(attribute))
//
//    case AttributeFeatureSpec(attribute, Some(operation)) =>
//      //TODO
//      Some(new AttributeDefaultConstructor(attribute))
//
//    case _ =>
//      logger.warn(s"unrecognized feature spec $spec")
//      None
//  }
//}
