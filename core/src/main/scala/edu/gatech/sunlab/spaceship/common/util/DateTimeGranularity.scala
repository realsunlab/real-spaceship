package edu.gatech.sunlab.spaceship.common.util

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
object DateTimeGranularity extends Enumeration {
  val Day, Month, Year = Value
}
