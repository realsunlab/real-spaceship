package edu.gatech.sunlab.spaceship.feature

import edu.gatech.sunlab.spaceship.common.events.FeatureOperator
import edu.gatech.sunlab.spaceship.common.events.aggregate.EventAggregator
import edu.gatech.sunlab.spaceship.common.events.transform.{
  EventFilterTransformer,
  LabeledGrouper,
  Transformer
}
import edu.gatech.sunlab.spaceship.common.model._
import edu.gatech.sunlab.spaceship.common.util.Logging
import org.apache.spark.rdd.RDD

import scala.collection.parallel.immutable.ParSeq
import scala.language.implicitConversions
import scala.util.control.Exception

object FeatureBuilder {

  implicit def patientEventsConv(
      samplePatients: RDD[SamplePatient]): RDD[BaseFeatureSwitcher] = {
    samplePatients.map { pe =>
      patientEventConv(pe)
    }
  }

  implicit def patientEventConv(
      samplePatient: SamplePatient): BaseFeatureSwitcher = {
    BaseFeatureSwitcher(
      in = Stream[(List[String], Stream[Event])](
        (List[String](), samplePatient.p.events.toStream))
    )
  }

  implicit def featureOperatorOp2Seq(
      op: FeatureOperator): Seq[FeatureOperator] =
    Seq[FeatureOperator](op)

  sealed trait FeatureSwitcher extends FeatureExecutor

  sealed trait FeatureExtractor extends FeatureExecutor

  trait FeatureExecutor extends Logging {
    //def hasFeature: Boolean
    def data(): Stream[(List[String], Events)]

    def features(): Stream[FeatureNameValue]

    def unary_! : Stream[FeatureNameValue] = this.features()

    /**
      * filter
      *
      * @return
      * @param ops right operators
      */
    def |(ops: Seq[FeatureOperator]): FeatureExecutor = {
      ops.foldLeft(this) { (l, c) =>
        c match {
          case o: Transformer =>
            logDebug(s"op: ${this} |(f) $o")
            l | o
          case o: LabeledGrouper =>
            logDebug(s"op: ${this} |(g) $o")
            l | o
          case o: EventAggregator =>
            logDebug(s"op: ${this} |(a) $o")
            l | o
          case o =>
            logWarning(s"unknown operation: ${o.toString}")
            l
        }
      }
    }

    /**
      * filter
      *
      * @param transformer constructed filter classe
      * @return
      */
    def |(transformer: Transformer): FeatureSwitcher = {
      transformer match {
        case e: EventFilterTransformer =>
          BaseFeatureSwitcher(
            in = this.data().map(d => new FeatureFilter(d._2, e, d._1).filter),
            _features = this.features()
          )
        case t =>
          BaseFeatureSwitcher(
            in = this
              .data()
              .map(d =>
                new FeatureDefaultTransformer(d._2, t, d._1).transform),
            _features = this.features()
          )
      }

    }

    /**
      * grouper
      *
      * @param labeledGrouper constructed grouper
      * @return
      */
    def |(labeledGrouper: LabeledGrouper): FeatureSwitcher = {
      // 1 => N
      BaseFeatureSwitcher(
        in = this
          .data()
          .flatMap(d => new FeatureGrouper(d._2, labeledGrouper, d._1).group),
        _features = this.features()
      )
    }

    /**
      * extract features for current events
      *
      * @param eventAggregator eventAggregator
      * @return
      */
    def |(eventAggregator: EventAggregator): FeatureSwitcher = {
      BaseFeatureSwitcher(
        in = this.data(),
        _features = this.features() ++ this
            .data()
            .flatMap(d =>
              new BaseFeatureExtractor(d._2, eventAggregator, d._1).features())
      )
    }
  }

  case class BaseFeatureSwitcher(in: Stream[(List[String], Stream[Event])],
                                 _features: Stream[FeatureNameValue] =
                                   Stream[FeatureNameValue]())
      extends FeatureSwitcher {
    override def data(): Stream[(List[String], Stream[Event])] = in

    override def features(): Stream[FeatureNameValue] = _features
  }

  class BlankFeatureSwitcher(
      _features: Stream[FeatureNameValue]
  ) extends FeatureSwitcher {
    //def hasFeature: Boolean
    override def data(): Stream[(List[String], Events)] =
      Stream[(List[String], Events)]()

    override def features(): Stream[FeatureNameValue] = _features
  }

  class FeatureFilter(events: Events,
                      transformer: EventFilterTransformer,
                      labels: List[String] = List[String]())
      extends FeatureSwitcher {
    lazy val _data: (List[String], Stream[Event]) =
      (labels :+ s"FILTER(${transformer.toString})",
       events.filter(transformer.check))

    def data(): Stream[(List[String], Stream[Event])] = Stream(filter)

    def filter: (List[String], Stream[Event]) = _data

    override def features(): Stream[FeatureNameValue] =
      Stream[FeatureNameValue]()
  }

  class FeatureDefaultTransformer(events: Events,
                                  transformer: Transformer,
                                  labels: List[String] = List[String]())
      extends FeatureSwitcher {
    lazy val _data: (List[String], Stream[Event]) =
      (labels :+ s"TRANSFORM(${transformer.toString})",
       transformer.apply(events))

    def data(): Stream[(List[String], Stream[Event])] = Stream(transform)

    def transform: (List[String], Stream[Event]) = _data

    override def features(): Stream[FeatureNameValue] =
      Stream[FeatureNameValue]()
  }

  class FeatureGrouper(events: Events,
                       labeledGrouper: LabeledGrouper,
                       labels: List[String] = List[String]())
      extends FeatureSwitcher {
    lazy val _data: Stream[(List[String], Events)] =
      labeledGrouper.group(events, labels).map(e => (e._1, e._2))

    override def data(): Stream[(List[String], Events)] = group

    def group: Stream[(List[String], Events)] = _data

    override def features(): Stream[FeatureNameValue] =
      Stream[FeatureNameValue]()
  }

  class BaseFeatureExtractor(events: Events,
                             eventAggregator: EventAggregator,
                             labels: List[String],
                             _features: Stream[FeatureNameValue] =
                               Stream[FeatureNameValue]())
      extends FeatureExtractor
      with Logging {

    lazy val label = (labels :+ eventAggregator.toString).mkString("|")

    //override def hasFeature: Boolean = true
    override def features(): Stream[FeatureNameValue] = {
      Exception.catching(classOf[Throwable]) opt
        eventAggregator
          .apply(events)
          .asInstanceOf[Number]
          .doubleValue() match {
        case Some(value) =>
          _features :+ FeatureNameValue(name = label, value = value)
        case None =>
          logWarning(s"parse failed in: $label")
          _features
      }
    }

    //def hasFeature: Boolean
    override def data(): Stream[(List[String], Events)] =
      Stream[(List[String], Events)]((labels, events))
  }

  implicit class FeatureOperatorOp(op: Seq[FeatureOperator]) {

    /**
      * filter
      *
      * @return
      * @param right right operator
      */
    def |(right: FeatureOperator): Seq[FeatureOperator] = {
      op :+ right
    }

    /**
      * filter
      *
      * @return
      * @param ops right operators
      */
    def |(ops: Seq[FeatureOperator]): Seq[FeatureOperator] = {
      ops.foldLeft(op) { (l, c) =>
        l :+ c
      }
    }

  }

  implicit class EventAggregatorsOp(op: Seq[EventAggregator]) {

    /**
      * filter
      *
      * @return
      * @param right right operator
      */
    def %%(right: EventAggregator): Seq[EventAggregator] = {
      op :+ right
    }

    /**
      * filter
      *
      * @return
      * @param ops right operators
      */
    def %%(ops: Seq[EventAggregator]): Seq[EventAggregator] = {
      ops.foldLeft(op) { (l, c) =>
        l :+ c
      }
    }
  }

  implicit class EventAggregatorOp(op: EventAggregator) {

    /**
      * filter
      *
      * @return
      * @param right right operator
      */
    def %%(right: EventAggregator): Seq[EventAggregator] = {
      Seq[EventAggregator](op) :+ right
    }

    /**
      * filter
      *
      * @return
      * @param ops right operators
      */
    def %%(ops: Seq[EventAggregator]): Seq[EventAggregator] = {
      op +: ops
    }
  }

  implicit class FeatureNameValueStreamMerge(stream: Stream[FeatureNameValue]) {

    /**
      * filter
      *
      * @return
      * @param right right operator
      */
    def :::(right: Stream[FeatureNameValue]): Stream[FeatureNameValue] = {
      right
        .foldLeft(stream)(_ :+ _)
        .groupBy(e => e.name)
        .map(e => e._2.head)
        .toStream
    }
  }

  case class FeatureTree(filter: List[(Transformer, FeatureTree)] =
                           List[(Transformer, FeatureTree)](),
                         group: List[(LabeledGrouper, FeatureTree)] =
                           List[(LabeledGrouper, FeatureTree)](),
                         agg: List[EventAggregator] = List[EventAggregator]())
      extends Logging {

    /**
      * with filtered
      *
      * @param o transformer
      * @param t tree
      * @return
      */
    def f(o: Transformer, oo: EventFilterTransformer*)(
        t: FeatureTree): FeatureTree = {
      val st =
        oo.foldRight(t)((c, r) => FeatureTree.T.copy(filter = List((c, r))))
      this.copy(filter = filter :+ (o, st))
    }

    // grouped
    def g(o: LabeledGrouper, oo: LabeledGrouper*)(
        t: FeatureTree): FeatureTree = {
      val st =
        oo.foldRight(t)((c, r) => FeatureTree.T.copy(group = List((c, r))))
      this.copy(group = group :+ (o, st))
    }

    def s(o: FeatureOperator, oo: FeatureOperator*)(
        t: FeatureTree): FeatureTree = {
      val st = oo.foldRight(t)((c, r) =>
        c match {
          case mo: Transformer =>
            FeatureTree.T.copy(filter = List((mo, r)))
          case mo: LabeledGrouper =>
            FeatureTree.T.copy(group = List((mo, r)))
          case mo =>
            logWarning(s"unknown operation: ${mo.toString}")
            r
      })
      o match {
        case mo: Transformer =>
          this.copy(filter = filter :+ (mo, st))
        case mo: LabeledGrouper =>
          this.copy(group = group :+ (mo, st))
        case mo =>
          logWarning(s"unknown operation: ${mo.toString}")
          this.copy(filter = filter ::: st.filter, group = group ::: st.group)
      }
    }

    // with aggregator
    def a(o: EventAggregator, oo: EventAggregator*): FeatureTree = {
      oo.foldLeft(this.copy(agg = agg :+ o))((l, c) => l.a(c))
    }

    def ~(o: EventAggregator): FeatureTree = {
      this.a(o)
    }

    def ~(o: (FeatureOperator, FeatureTree)) = {
      this.s(o._1)(o._2)
    }

    /**
      * extract features from sample patient
      *
      * @param sample sample patient
      * @return
      */
    def extract(sample: SamplePatient)
      : (Stream[FeatureNameValue], List[(String, String)]) = {
      val indexTs = sample.indexDate.getMillis
      extract(
        Stream(
          (List[String](),
           sample.p.events.filter(e => e.end.isBefore(indexTs)).toStream,
           "",
           "")))
    }

    /**
      * @param eventss events stream , with parameters: LabelList, EventStream, LastLabel, LastValue
      * @return
      */
    def extract(eventss: Stream[(List[String], Events, String, String)])
      : (Stream[FeatureNameValue], List[(String, String)]) = {
      val filterBranch: ParSeq[(Stream[FeatureNameValue],
                                List[(String, String)])] = filter.par.flatMap {
        ft =>
          val transformer: Transformer = ft._1
          val currentStatus = transformer match {
            case _: EventFilterTransformer =>
              s"FILTER(${transformer.toString})"
            case _ => s"TRANSFORM(${transformer.toString})"
          }
          eventss.par.map { events =>
            val labels: List[String] = events._1 :+ currentStatus
            val resp: Events = transformer.apply(events._2)
            //(labels :+ s"TRANSFORM(${transformer.toString})", transformer.apply(events))
            ft._2.extract(Stream((labels, resp, currentStatus, "")))
          }
      }

      val groupBranch: ParSeq[(Stream[FeatureNameValue],
                               List[(String, String)])] = group.par.flatMap {
        gt =>
          val grouper: LabeledGrouper = gt._1
          val label: String = grouper.toString
          eventss.par.map { events =>
            gt._2.extract(
              grouper
                .group(events._2, events._1)
                .map(e => (e._1, e._2, label, e._3)))
          }
      }

      val aggBranch: ParSeq[(Stream[FeatureNameValue], List[(String, String)])] =
        agg.par.flatMap { at =>
          eventss.par.map { events =>
            val labels = events._1 :+ at.toString
            val groupKey =
              s"${at.toString}@[${(events._1.dropRight(1) :+ events._3).mkString("|")}]"
            val groupValue = events._4
            Exception.catching(classOf[Throwable]) opt at
              .apply(events._2)
              .asInstanceOf[Number]
              .doubleValue() match {
              case Some(value) =>
                //Some(FeatureNameValue(labels.mkString("|"), value))
                (Stream[FeatureNameValue](
                   FeatureNameValue(labels.mkString("|"), value)),
                 List[(String, String)]((groupKey, groupValue)))
              case None =>
                (Stream[FeatureNameValue](), List[(String, String)]())
            }
          }
        }

      // stats in middle layer
      //      val nodes: List[(String, String)] = eventss
      //        .flatMap(e =>
      //          if (e._1.nonEmpty) {
      //            Some((s"{${(e._1.dropRight(1) :+ e._3).mkString("|")}}", e._4))
      //          } else {
      //            None
      //        }).toList

      val featureNameValues =
        (filterBranch.map(_._1) ++ groupBranch.map(_._1) ++ aggBranch.map(
          _._1)).flatten.toStream
      val resultStats =
        (filterBranch.map(_._2) ++ groupBranch.map(_._2) ++ aggBranch.map(
          _._2)).flatten.distinct.toList
      (featureNameValues, resultStats)
    }

  }

  object FeatureTree {
    lazy val T: FeatureTree = new FeatureTree()
  }

}
