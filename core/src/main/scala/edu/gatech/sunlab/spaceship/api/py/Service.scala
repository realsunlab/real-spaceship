package edu.gatech.sunlab.spaceship.api.py

import edu.gatech.sunlab.spaceship.common.util.Logging
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.control.Exception.catching

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/30/15
  */
trait Service extends Logging {
  protected val errorHandler =
    catching(classOf[Exception]).withApply(e =>
      logError("fail service api with ", e))

  def fail(msg: String = "unknown") =
    s"""
      |{
      |     "msg": "$msg",
      |     "status": "fail"
      |}
    """.stripMargin

  def success(msg: String = "") =
    s"""
       |{
       |    "msg": "$msg",
       |    "status": "success"
       |}
    """.stripMargin
}

object SparkService extends Service {
  lazy val sc: SparkContext = createContext

  def createContext(appName: String, masterUrl: String): SparkContext = {
    logInfo(s"Creating SparkContext as $appName with master@$masterUrl")

    val conf = new SparkConf()
      .setAppName(appName)
      .setMaster(masterUrl)
      .set("spark.executor.memory", "30G")
      .set("spark.driver.memory", "10G")
    new SparkContext(conf)
  }

  def createContext(appName: String): SparkContext =
    createContext(appName, "local")

  def createContext: SparkContext = createContext("SpaceShip", "local[*]")

}
