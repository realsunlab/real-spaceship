package edu.gatech.sunlab.spaceship.common.events.expression

import edu.gatech.sunlab.spaceship.common.model.Event

import scala.util.matching.Regex

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 8/30/16
  */
abstract class BooleanExpr(left: Expression, right: Expression)
    extends BinaryExpr(left, right) {
  override def eval(e: Event): (Boolean, BooleanType)
}

trait TypedCompare extends BinaryOpTypeUtil {
  def compare(leftVal: Any, rightVal: Any): Boolean = {
    val leftType = DataTypes.get(leftVal)
    val rightType = DataTypes.get(rightVal)

    compare(leftType, leftVal, rightType, rightVal)
  }

  def compare(leftType: DataType,
              leftVal: Any,
              rightType: DataType,
              rightVal: Any): Boolean = {
    val (newLeftVal, newRightVal, commonType) =
      toSameType(leftType, leftVal, rightType, rightVal)
    val ord =
      commonType.asInstanceOf[AtomicType].ordering.asInstanceOf[Ordering[Any]]
    compare(ord, newLeftVal, newRightVal)
  }

  def compare(ord: Ordering[Any], leftVal: Any, rightVal: Any): Boolean
}

trait TypedCmpLt extends TypedCompare {
  def compare(ord: Ordering[Any], leftVal: Any, rightVal: Any): Boolean =
    ord.lt(leftVal, rightVal)
}

trait TypedCmpLte extends TypedCompare {
  def compare(ord: Ordering[Any], leftVal: Any, rightVal: Any): Boolean =
    ord.lteq(leftVal, rightVal)
}

trait TypedCmpGt extends TypedCompare {
  def compare(ord: Ordering[Any], leftVal: Any, rightVal: Any): Boolean =
    ord.gt(leftVal, rightVal)
}

trait TypedCmpGte extends TypedCompare {
  def compare(ord: Ordering[Any], leftVal: Any, rightVal: Any): Boolean =
    ord.gteq(leftVal, rightVal)
}

trait TypedCmpEq extends TypedCompare {
  def compare(ord: Ordering[Any], leftVal: Any, rightVal: Any): Boolean =
    ord.equiv(leftVal, rightVal)
}

abstract class CmpBooleanExpr(left: Expression, right: Expression)
    extends BooleanExpr(left, right)
    with TypedCompare {
  override def eval(e: Event): (Boolean, BooleanType) = {
    val (leftVal, leftTyp) = left.eval(e)
    val (rightVal, rightTyp) = right.eval(e)

    if (leftTyp == NullType || rightTyp == NullType) {
      (false, BooleanType)
    } else {
      val outputVal = compare(leftTyp, leftVal, rightTyp, rightVal)
      (outputVal, BooleanType)
    }
  }
}

class GtExpr(left: Expression, right: Expression)
    extends CmpBooleanExpr(left, right)
    with TypedCmpGt {
  override def toString = s"$left > $right"
}

class GteExpr(left: Expression, right: Expression)
    extends CmpBooleanExpr(left, right)
    with TypedCmpGte {
  override def toString = s"$left >= $right"
}

class LtExpr(left: Expression, right: Expression)
    extends CmpBooleanExpr(left, right)
    with TypedCmpLt {

  override def toString = s"$left < $right"
}

class LteExpr(left: Expression, right: Expression)
    extends CmpBooleanExpr(left, right)
    with TypedCmpLte {
  override def toString = s"$left <= $right"
}

class EqExpr(left: Expression, right: Expression)
    extends CmpBooleanExpr(left, right)
    with TypedCmpEq {
  override def toString = s"$left === $right"
}

class InExpr(left: Expression, right: Expression)
    extends BooleanExpr(left, right) {
  override def eval(e: Event): (Boolean, BooleanType) = {
    val (leftVal, leftTyp) = left.eval(e)
    val (rightVal, rightTyp) = right.eval(e)

    val res: Boolean = (leftTyp, rightTyp) match {
      case (a, b) if a == NullType || b == NullType => false
      case (a: AtomicType, ArrayType(elemType)) =>
        val common = commonType(a, elemType)
        val newLeftVal = TypeCast(leftVal, leftTyp, common)
        rightVal
          .asInstanceOf[Seq[Any]]
          .exists(rv => TypeCast(rv, elemType, common).equals(newLeftVal))
      case _ =>
        sys.error("InExpr left must be atomic and right must be an array")
    }
    (res, BooleanType)
  }

  override def toString = s"$left =?= $right"
}

class RegMatchExpr(left: Expression, right: Expression)
    extends BooleanExpr(left, right) {
  /// TODO cache compiled regex if right is a literal

  override def eval(e: Event): (Boolean, BooleanType) = {
    val (leftVal, _) = left.eval(e)
    val (rightVal, _) = right.eval(e)

    val res =
      new Regex(rightVal.toString).pattern.matcher(leftVal.toString).matches()
    (res, BooleanType)
  }

  override def toString = s"$left =~= $right"
}
