package edu.gatech.sunlab.spaceship.common.io

import edu.gatech.sunlab.spaceship.common.model.{
  CaseControlFeatureStat,
  FeatureStat
}
import org.json4s.{DefaultFormats, Formats}
import org.json4s.jackson.Serialization.write

import scalaz.{Zip, _}
import scalaz.std.list._

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/7/15
  */
object FeatureSummary {

  def save(overallSummary: List[FeatureStat],
           caseSummary: List[FeatureStat],
           controlSummary: List[FeatureStat],
           path: String = "output"): Unit = {
    implicit val formats: Formats = DefaultFormats

//    scala.tools.nsc.io.File("overallSummary.txt").writeAll(overallSummary.mkString("\n"))
//    scala.tools.nsc.io.File("caseSummary.txt").writeAll(caseSummary.mkString("\n"))
//    scala.tools.nsc.io.File("controlSummary.txt").writeAll(controlSummary.mkString("\n"))

    val summary =
      Zip[List].ap.tuple3(overallSummary, caseSummary, controlSummary).map {
        case (overall, cases, ctrls) =>
          new CaseControlFeatureStat(overall, cases, ctrls)
      }

    val pw =
      new java.io.PrintWriter(new java.io.File(path + "/featurestat.txt"))
    write(summary, pw)
    pw.close()
  }
}
