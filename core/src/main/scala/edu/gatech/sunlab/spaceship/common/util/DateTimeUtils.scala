package edu.gatech.sunlab.spaceship.common.util

import org.joda.time.{DateTime => DT}
import org.joda.time.format.{
  DateTimeFormat,
  DateTimeFormatter,
  ISODateTimeFormat
}

import scala.util.{Success, Try}

/**
  * Created by hang on 6/29/16.
  */
object DateTimeUtils {

  private val mdyFormat = DateTimeFormat.forPattern("MM/dd/yyyy")
  private val fmts = List(mdyFormat,
                          ISODateTimeFormat.dateTime(),
                          ISODateTimeFormat.basicDate(),
                          ISODateTimeFormat.date(),
                          ISODateTimeFormat.basicDateTime())

  def tryParse(value: String): Option[DT] = {
    fmts.map(fmt => Try(fmt.parseDateTime(value))).collectFirst {
      case Success(dt) => dt
    }
  }

  implicit def dateTimeOrdering: Ordering[DT] =
    Ordering.fromLessThan(_ isBefore _)
}
