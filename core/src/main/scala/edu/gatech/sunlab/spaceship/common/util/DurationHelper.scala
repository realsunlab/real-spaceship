package edu.gatech.sunlab.spaceship.common.util

import org.joda.time.{DateTime, Duration, PeriodType}
import org.joda.time.format.{ISOPeriodFormat, PeriodFormatterBuilder}

import scala.language.implicitConversions

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
object DurationHelper {
  def toInt(du: Duration, granularity: DateTimeGranularity.Value): Int =
    granularity match {
      case DateTimeGranularity.Day => du.getStandardDays.toInt
      case DateTimeGranularity.Month => (du.getStandardDays / 30).toInt
      case DateTimeGranularity.Year => (du.getStandardDays / 365).toInt
    }

  def prettyPrint(du: Duration): String = {
    val year = du.getStandardDays / 365
    val month = (du.getStandardDays % 365) / 30
    val day = (du.getStandardDays % 365) % 30
    val hour = du.getStandardHours % 24

    s"${year}Y${month}M${day}D${hour}h"
    // ISOPeriodFormat.standard().print(du.toPeriodTo(DateTime.now))
  }

  def prettyPrint(du: Option[Duration]): String = du match {
    case Some(d) => prettyPrint(d)
    case None => "None"
  }
}
