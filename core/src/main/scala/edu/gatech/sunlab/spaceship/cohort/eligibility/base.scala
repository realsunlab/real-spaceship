package edu.gatech.sunlab.spaceship.cohort.eligibility

import edu.gatech.sunlab.spaceship.common.events.HigherOrderEventExtractor
import edu.gatech.sunlab.spaceship.common.events.aggregate.EventAggregator
import edu.gatech.sunlab.spaceship.common.events.expression.{
  BooleanExpr,
  DataTypes,
  TypedCompare
}
import edu.gatech.sunlab.spaceship.common.model.Events
import edu.gatech.sunlab.spaceship.common.model.{Events, SamplePatient}
import edu.gatech.sunlab.spaceship.common.util.Events

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/15/16
  */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait PatientEligibility extends Serializable {
  def check(sp: SamplePatient): Boolean

  def and(other: PatientEligibility): PatientEligibility =
    new AndPatientEligibility(this, other)

  def or(other: PatientEligibility): PatientEligibility =
    new OrPatientEligibility(this, other)

  def unary_~ = new NotPatientEligibility(this)
}

class AndPatientEligibility(left: PatientEligibility,
                            right: PatientEligibility)
    extends PatientEligibility {
  def check(sp: SamplePatient): Boolean = left.check(sp) && right.check(sp)
}

class OrPatientEligibility(left: PatientEligibility, right: PatientEligibility)
    extends PatientEligibility {
  def check(sp: SamplePatient): Boolean = left.check(sp) || right.check(sp)
}

class NotPatientEligibility(eligibility: PatientEligibility)
    extends PatientEligibility {
  def check(sp: SamplePatient): Boolean = !eligibility.check(sp)
}

abstract class PatientEventsEligibility(
    higherOrderEventExtractor: HigherOrderEventExtractor)
    extends PatientEligibility {
  def check(sp: SamplePatient): Boolean = {
    val eventExtractor = higherOrderEventExtractor(List(sp.indexDate))

    val events = eventExtractor(sp.p)

    checkEvents(events)
  }

  def checkEvents(events: Events): Boolean
}

class EventExistsEligibility(
    higherOrderEventExtractor: HigherOrderEventExtractor)
    extends PatientEventsEligibility(higherOrderEventExtractor) {
  override def checkEvents(in: Events): Boolean = in.nonEmpty
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
abstract class PartiallyAppliedCompare(rightVal: Any)
    extends TypedCompare
    with Serializable {
  /* reuse type aware compare */
  val rightType = DataTypes.get(rightVal)
  def check(leftVal: Any): Boolean = {
    val leftType = DataTypes.get(leftVal)
    compare(leftType, leftVal, rightType, rightVal)
  }
}

class AggregationEventsEligibility(
    higherOrderEventExtractor: HigherOrderEventExtractor,
    eventAggregator: EventAggregator,
    predicate: PartiallyAppliedCompare)
    extends PatientEventsEligibility(higherOrderEventExtractor) {
  override def checkEvents(in: Events): Boolean = {
    val value = eventAggregator(in)
    if (value == null) false
    else predicate.check(value)
  }

  override def toString: String =
    s"$eventAggregator on [$higherOrderEventExtractor] should $predicate"
}

class SingleEventBasedEligibility(
    higherOrderEventExtractor: HigherOrderEventExtractor,
    booleanExpr: BooleanExpr)
    extends PatientEventsEligibility(higherOrderEventExtractor) {
  override def checkEvents(in: Events): Boolean = in match {
    case Stream.Empty => false
    case Stream(x, xs @ _ *) => booleanExpr.eval(x)._1
  }

  override def toString: String =
    s"[$higherOrderEventExtractor] should $booleanExpr"
}
