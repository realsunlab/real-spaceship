package edu.gatech.sunlab.spaceship.common.events.aggregate

import java.io.Serializable

import edu.gatech.sunlab.spaceship.common.events.FeatureOperator
import edu.gatech.sunlab.spaceship.common.model.Event
import edu.gatech.sunlab.spaceship.common.util.Logging
import org.joda.time.Duration

import scala.collection.mutable.ListBuffer

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/6/16
  */
trait EventAggregator extends Serializable with Logging with FeatureOperator {
  type EventSeq = Seq[Event]

  def apply(input: EventSeq): Any
}

object LengthEventAggregator extends EventAggregator {
  def apply(input: EventSeq): Int = input.length

  override def toString: String = "LENGTH"
}

abstract class BaseFieldAggregator(field: String = "value")
    extends EventAggregator

class SumEventAggregator(field: String = "value")
    extends BaseFieldAggregator(field) {
  def apply(input: EventSeq): Any = {
    input
      .flatMap(e => e.properties.get(field))
      .map(_.asInstanceOf[Number].doubleValue)
      .sum
  }

  override def toString: String = s"SUM($field)"
}

/**
  * Simply get property value directly, mainly used for attributes (those don't have time stamp)
  *
  * @param field property to retrieve
  */
class PropertyValueEventAggregator(field: String = "value")
    extends BaseFieldAggregator(field) {
  def apply(input: EventSeq): Any = {
    if (input.isEmpty) {
      logWarning("Empty input")
      null
    } else input.head.properties(field)
  }

  override def toString: String = field
}

object DurationEventAggregator extends EventAggregator {
  override val toString = "DURATION"

  def apply(input: EventSeq): Int = {
    input.flatMap(_.interval).map(_.toDuration.getStandardDays.toInt).sum
  }
}

class MaxContinuousDurationAggregator(val maxGap: Duration = Duration.ZERO)
    extends EventAggregator {
  override def apply(in: EventSeq): Duration = {
    in.tail
      .foldLeft(ListBuffer((in.head.interval.get.toDuration, in.head.end))) {
        case (acc, e) =>
          val previous = acc.last
          val gap = new Duration(previous._2, e.begin)
          if (gap.compareTo(maxGap) <= 0) {
            /* replace last unless elem to add end time is later than duration end up to now */
            if (e.end.compareTo(previous._2) > 0) {
              val last =
                (previous._1.plus(new Duration(previous._2, e.end)), e.end)
              acc.update(acc.length - 1, last)
            }
          } else {
            /* create a new end and new duration */
            acc.append((e.interval.get.toDuration, e.end))
          }
          acc
      }
      .map(_._1)
      .maxBy(_.getMillis)
  }
}
