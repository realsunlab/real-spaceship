package edu.gatech.sunlab.spaceship.common.io

import edu.gatech.sunlab.spaceship.common.model.SamplePatient
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object CohortAdapter {
  def save(patients: RDD[SamplePatient], path: String): Unit = {
    patients.saveAsObjectFile(path)
  }

  def load(sc: SparkContext, path: String): RDD[SamplePatient] = {
    sc.objectFile[SamplePatient](path)
  }
}
