package edu.gatech.sunlab.spaceship

import java.security.InvalidParameterException

/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
package object common {

  def isCase(label: Double) = label == 1.0d

  def isControl(label: Double) = label == 0.0d

  object BooleanMergeType {

    sealed trait enum

    case object All extends enum

    case object Any extends enum

    def parse(str: String): enum = str.toLowerCase match {
      case "all" => All
      case "any" => Any
      case _ => throw new InvalidParameterException(str)
    }
  }

  final val GlobalSerialVersionUID = 0L
}
