/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
package edu.gatech.sunlab.spaceship.common.model

import org.joda.time.{DateTime, DateTimeComparator}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
class Patient(val id: PatientId, unorderedEvents: List[Event])
    extends Serializable {
  val events = unorderedEvents
    .filter(_.begin != null)
    .sortBy(_.begin)(new Ordering[DateTime] {
      override def compare(x: DateTime, y: DateTime): Int =
        DateTimeComparator.getInstance().compare(x, y)
    })

  private val eventGrpByConcept = events.groupBy(_.concept)
  private val attributes =
    unorderedEvents.filter(_.begin == null).map(e => (e.concept, e)).toMap

  lazy val latestDate = events.last.end
  def eventsWithConcept(concept: String): List[Event] = {
    attributes
      .get(concept)
      .map(List(_))
      .getOrElse(eventGrpByConcept.getOrElse(concept, List.empty))
  }

  override def toString: String = {
    s"Patient-$id"
  }
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
class SamplePatient(val p: Patient,
                    val indexDate: DateTime,
                    val targetDate: DateTime,
                    val targetValue: Double = 0.0d)
    extends Serializable {
  def withIndexDate(date: DateTime): SamplePatient =
    new SamplePatient(p, date, targetDate, targetValue)

  def id = p.id
}

object SamplePatient {
  def withTarget(p: Patient, targetDate: DateTime, targetValue: Double) =
    new SamplePatient(p, null, targetDate, targetValue)
}
