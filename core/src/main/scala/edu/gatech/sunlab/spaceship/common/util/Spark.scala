package edu.gatech.sunlab.spaceship.common.util

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by hang on 7/15/16.
  */
object Spark {
  def createContext(appName: String, masterUrl: String): SparkContext = {
    val conf = new SparkConf()
      .setAppName(appName)
      .setMaster(masterUrl)
      .set("spark.executor.memory", "30G")
      .set("spark.driver.memory", "10G")
    //      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    //      .set("spark.kryoserializer.buffer","24")
    //    conf.registerKryoClasses(Array(classOf[Event]))
    new SparkContext(conf)
  }

  def createContext(appName: String): SparkContext =
    createContext(appName, "local[*]")

  def createContext: SparkContext = createContext("SpaceShip", "local[*]")
}
