package edu.gatech.sunlab.spaceship.common.io

import edu.gatech.sunlab.spaceship.common.model.{Event, Patient}
import edu.gatech.sunlab.spaceship.common.util.Logging
import org.apache.spark.HashPartitioner
import org.apache.spark.rdd.RDD

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/7/16
  */
object PatientLoader extends Logging {
  def load(events: RDD[Event]): RDD[Patient] = {
    val sc = events.sparkContext
    val patients = events
      .map(e => (e.patientId, e))
      .groupByKey(new HashPartitioner(sc.defaultParallelism))
      .mapValues(_.toList)
      .map(identity)
      .map { case (pid, evs) => new Patient(pid, evs) }

    patients.cache()
    logInfo(s"Total number of patients loaded: ${patients.count()}")
    patients
  }
}

class PatientWriter {}
