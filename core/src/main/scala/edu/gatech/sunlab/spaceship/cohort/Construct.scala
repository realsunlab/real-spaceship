package edu.gatech.sunlab.spaceship.cohort

import edu.gatech.sunlab.spaceship.cohort.eligibility.PatientEligibility
import edu.gatech.sunlab.spaceship.cohort.index.IndexAssigner
import edu.gatech.sunlab.spaceship.cohort.target.TargetAssigner
import edu.gatech.sunlab.spaceship.common.model.{Patient, SamplePatient}
import org.apache.spark.rdd.RDD

/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
object MultiIndexDateStrategy {

  sealed trait enum

  case object All extends enum

  case object First extends enum

  case object Last extends enum

  case object Random extends enum

}

class CohortBuilder(targetAssigner: TargetAssigner,
                    indexAssigner: IndexAssigner,
                    patientEligibility: PatientEligibility)
    extends Serializable {
  def build(patients: RDD[Patient]): RDD[SamplePatient] = {
    val patientsWithTarget = patients.map(targetAssigner.apply)
    val patientsWithIndex =
      patientsWithTarget
        .filter(_.nonEmpty)
        .map(_.head)
        .flatMap(indexAssigner.apply)
    patientsWithIndex.filter(patientEligibility.check)
  }
}

//abstract class CohortConstructor extends Serializable {
//  def apply(patient: Patient): List[SamplePatient]
//}
//
//class IndexFirstCohortConstructor(val assigner: WithoutTargetIndexDateAssigner,
//                                  val patientIndexfilter: PatientIndexDatesFilter,
//                                  val target: TargetAssigner,
//                                  multiIndexDateStrategy: MultiIndexDateStrategy.enum = MultiIndexDateStrategy.First)
//  extends CohortConstructor {
//
//  override def apply(patient: Patient): List[SamplePatient] = {
//    val patientWithCandiateIndexDate = assigner(patient)
//
//    if (patientWithCandiateIndexDate == null) return List.empty
//
//    val patientWithFilteredIndex = patientIndexfilter(patientWithCandiateIndexDate)
//
//    if (patientWithFilteredIndex.isEmpty) return List.empty
//
//    val indexDates = patientWithFilteredIndex.get.indexDates
//
//    val samplePatients = multiIndexDateStrategy match {
//      case MultiIndexDateStrategy.All => indexDates.map(SamplePatient.onlyIndexDate(patient, _))
//      case MultiIndexDateStrategy.First => List(SamplePatient.onlyIndexDate(patient, indexDates.head))
//      case MultiIndexDateStrategy.Last => List(SamplePatient.onlyIndexDate(patient, indexDates.last))
//      case MultiIndexDateStrategy.Random => List(SamplePatient.onlyIndexDate(patient, indexDates(Random.nextInt(indexDates.length))))
//    }
//
//    samplePatients.flatMap(target.apply)
//  }
//}
