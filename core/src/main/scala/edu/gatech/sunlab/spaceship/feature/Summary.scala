package edu.gatech.sunlab.spaceship.feature

import edu.gatech.sunlab.spaceship.common.model.{
  FeatureConstructionResult,
  FeatureStat
}
import edu.gatech.sunlab.spaceship.common.{isCase, isControl}
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.stat.MultivariateOnlineSummarizer
import org.apache.spark.rdd.RDD

import scalaz.{Zip, _}
import scalaz.std.list._

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/7/15
  */
object Summary {
  def apply(featureConstructionResult: FeatureConstructionResult)
    : (List[FeatureStat],
       List[FeatureStat],
       List[FeatureStat],
       Long,
       Long,
       Long) = {
    val data = featureConstructionResult.features.cache()

    val allFeatures = data.map(_.features)
    val casePatientFeatures = data.filter(x => isCase(x.label)).map(_.features)
    val controlPatientFeatures =
      data.filter(x => isControl(x.label)).map(_.features)

//    println("------------------------------------------")
//    println(f"   numPatients: ${allFeatures.count()}")
//    println(f"   numCase    : ${casePatientFeatures.count()}")
//    println(f"   numControl : ${controlPatientFeatures.count()}")
//    println("------------------------------------------")
    val overallSummary =
      summarize(allFeatures, featureConstructionResult.mapping)
    val caseSummary =
      summarize(casePatientFeatures, featureConstructionResult.mapping)
    val controlSummary =
      summarize(controlPatientFeatures, featureConstructionResult.mapping)

    (overallSummary,
     caseSummary,
     controlSummary,
     allFeatures.count(),
     casePatientFeatures.count(),
     controlPatientFeatures.count())
  }

  def summarize(data: RDD[Vector],
                mapping: Map[String, Int]): List[FeatureStat] = {
    val summary = data.treeAggregate(new MultivariateOnlineSummarizer)(
      (aggregator, data) => aggregator.add(data),
      (aggregator1, aggregator2) => aggregator1.merge(aggregator2))

    val nameIndices =
      mapping.map(pair => (pair._2, pair._1)).toList.sortBy(_._1)

    Zip[List].ap.apply6(nameIndices,
                        summary.numNonzeros.toArray.toList,
                        summary.min.toArray.toList,
                        summary.mean.toArray.toList,
                        summary.max.toArray.toList,
                        summary.variance.toArray.toList) {
      case (nameIndex, nnz, min, mean, max, variance) =>
        FeatureStat(nameIndex._1,
                    nameIndex._2,
                    nnz.toLong,
                    min,
                    mean,
                    max,
                    variance)
    }
  }
}
