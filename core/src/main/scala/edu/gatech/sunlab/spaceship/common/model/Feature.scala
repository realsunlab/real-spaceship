package edu.gatech.sunlab.spaceship.common.model

import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/2/15
  */
case class FeatureNameValue(name: String, value: Double) extends Serializable
case class NamedFeatureTrainingSample(id: String,
                                      label: Double,
                                      features: List[FeatureNameValue])
    extends Serializable

case class LabeledPointWithId(id: String, label: Double, features: Vector)
    extends Serializable {
  def toLabeledPoint = LabeledPoint(label, features)

  override def toString: String = {
    "(%s,%s)".format(label, features)
  }
}

class FeatureConstructionResult(val features: RDD[LabeledPointWithId],
                                val mapping: Map[String, Int])
    extends Serializable

case class FeatureStat(index: Int,
                       name: String,
                       count: Long,
                       min: Double,
                       mean: Double,
                       max: Double,
                       variance: Double)

case class CaseControlFeatureStat(index: Int,
                                  name: String,
                                  count: Long,
                                  min: Double,
                                  mean: Double,
                                  max: Double,
                                  variance: Double,
                                  caseCount: Long,
                                  caseMin: Double,
                                  caseMean: Double,
                                  caseMax: Double,
                                  caseVariance: Double,
                                  controlCount: Long,
                                  controlMin: Double,
                                  controlMean: Double,
                                  controlMax: Double,
                                  controlVariance: Double) {
  def this(overall: FeatureStat, cases: FeatureStat, ctrls: FeatureStat) =
    this(
      overall.index,
      overall.name,
      overall.count,
      overall.min,
      overall.mean,
      overall.max,
      overall.variance,
      cases.count,
      cases.min,
      cases.mean,
      cases.max,
      cases.variance,
      ctrls.count,
      ctrls.min,
      ctrls.mean,
      ctrls.max,
      ctrls.variance
    )
}
