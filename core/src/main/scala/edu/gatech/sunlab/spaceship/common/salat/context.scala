package edu.gatech.sunlab.spaceship.common.salat

import com.novus.salat.json.{
  TimestampDateStrategy,
  StringObjectIdStrategy,
  JSONConfig
}
import com.novus.salat.{TypeHintFrequency, StringTypeHintStrategy, Context}
import org.joda.time.DateTimeZone

/**
  * @author Hang Su <hangsu@gatech.edu>.
  */
object context {
  implicit val ctx = new Context {
    val name = "json-test-context"
    override val typeHintStrategy =
      StringTypeHintStrategy(when = TypeHintFrequency.WhenNecessary,
                             typeHint = "_t")
    override val jsonConfig = JSONConfig(
      dateStrategy = TimestampDateStrategy(DateTimeZone.forID("US/Eastern")),
      //StringDateStrategy(dateFormatter = ISODateTimeFormat.dateTime.withZone("US/Eastern")),
      objectIdStrategy = StringObjectIdStrategy)
  }
}
