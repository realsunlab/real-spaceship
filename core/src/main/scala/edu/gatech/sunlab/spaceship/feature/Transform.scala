package edu.gatech.sunlab.spaceship.feature

import edu.gatech.sunlab.spaceship.common.model.{
  LabeledPointWithId,
  FeatureConstructionResult
}
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.stat.MultivariateOnlineSummarizer

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/7/15
  */
abstract class Transform {
  def apply(input: FeatureConstructionResult): FeatureConstructionResult
}

class NormalTransform extends Transform {
  override def apply(
      input: FeatureConstructionResult): FeatureConstructionResult = {
    input.features.cache()
    val scalar = new StandardScaler().fit(input.features.map(_.features))
    val scaledFeatures =
      input.features.map(f =>
        LabeledPointWithId(f.id, f.label, scalar.transform(f.features)))
    new FeatureConstructionResult(scaledFeatures, input.mapping)
  }
}
