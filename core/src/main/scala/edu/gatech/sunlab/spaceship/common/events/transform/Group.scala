package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.events.aggregate.EventAggregator
import edu.gatech.sunlab.spaceship.common.model.{
  Event,
  Events,
  Temporal,
  Temporals
}
import edu.gatech.sunlab.spaceship.common.util.Events
import org.joda.time.Duration

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/6/16
  */
trait EventGroupTransformer extends Transformer

/**
  * Concatenate events into one if gap is smaller than {{maxGap}} and share
  * common property value
  *
  * @param property property to check, by default concept
  * @param maxGap maximum allowed gaps to be allowed
  */
class IntervalConcatEventGroupTransformer(property: String = "concept",
                                          maxGap: Duration = Duration.ZERO)
    extends EventGroupTransformer {
  override def apply(in: Events): Events = {
    if (in.isEmpty) return in

    val current = in.head
    var remaining = in.tail
    val bt = current.begin
    var et = current.end

    while (remaining.nonEmpty && et
             .plus(maxGap)
             .compareTo(remaining.head.begin) >= 0 &&
           (property == "" || current.properties(property) == remaining.head
             .properties(property))) {
      et = remaining.head.end
      remaining = remaining.tail
    }

    val properties =
      if (property == "" || property == "concept") Seq.empty[(String, Any)]
      else Seq(property -> current.properties(property))
    val concept = if (property == "concept") current.concept else "concact"
    val e =
      new Event(current.patientId, concept, bt, et, _properties = properties)

    e #:: apply(remaining)
  }
}

/**
  * Create events for overlap between neighbors in original input events
  * if they differ in {{property}} value
  *
  * @param property property to compare, by default concept
  * @param maxGap maximum allowed gap
  */
class NeighborOverlapEventGroupTransformer(property: String = "concept",
                                           maxGap: Duration = Duration.ZERO)
    extends EventGroupTransformer {

  /**
    * Temporal workaround to use group and small look ahead window
    * of size 5 to make the transformation from {{in}} stateless
    *
    * TODO better look ahead with buffer. Copy properties of two with prefix 1_, 2_
    */
  override def apply(in: Events): Events = Events.sliding(in, 5).flatMap {
    case Seq(first, other @ _ *) =>
      other.flatMap {
        case second =>
          if (first.end.plus(maxGap).compareTo(second.begin) >= 0 &&
              (property == "" || first.properties(property) != second
                .properties(property))) {
            Some(
              new Event(first.patientId, "overlap", second.begin, first.end))
          } else {
            None
          }
      }
    case _ => None
  }
}

/* define common operators to group events */
@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait TemporalGrouper[T <: Temporal] extends Serializable {
  def group(in: Stream[T]): Stream[Seq[T]]

  //TODO grouper may need return new begin, end time
}

class SlidingGrouper[T <: Temporal](window: Int, step: Int = 1)
    extends TemporalGrouper[T] {
  override def group(in: Stream[T]): Stream[Seq[T]] =
    Events.sliding(in, window)
}

class SimpleGrouper[T <: Temporal](window: Int, step: Int = 1)
    extends TemporalGrouper[T] {
  override def group(in: Stream[T]): Stream[Seq[T]] = Events.group(in, window)
}

class WholeGrouper[T <: Temporal] extends TemporalGrouper[T] {
  override def group(in: Stream[T]): Stream[Seq[T]] = Stream(in)
}

class CalenderYearTemporalGrouper[T <: Temporal] extends TemporalGrouper[T] {
  override def group(in: Stream[T]): Stream[Seq[T]] = {
    if (in.isEmpty) return Stream.empty
    val res = ArrayBuffer[T](in.head)
    var remaining = in.tail

    while (remaining.nonEmpty && remaining.head.begin.getYear == res.head.begin.getYear) {
      res.append(remaining.head)
      remaining = remaining.tail
    }
    res #:: group(remaining)
  }
}

class CalenderQuarterTemporalGrouper[T <: Temporal]
    extends TemporalGrouper[T] {
  override def group(in: Stream[T]): Stream[Seq[T]] = {
    if (in.isEmpty) return Stream.empty
    val res = ArrayBuffer[T](in.head)
    var remaining = in.tail

    while (remaining.nonEmpty &&
           remaining.head.begin.getYear == res.head.begin.getYear && // same year
           ((remaining.head.begin.getMonthOfYear - 1) / 3) == ((res.head.begin.getMonthOfYear - 1) / 3)) // quarter
    {
      res.append(remaining.head)
      remaining = remaining.tail
    }
    res #:: group(remaining)
  }
}

class AggregateGroupedEventsGroupTransformer(
    val grouper: TemporalGrouper[Event],
    val properties: Seq[(String, EventAggregator)])
    extends EventGroupTransformer {
  override def apply(in: Events): Events = {
    grouper.group(in).flatMap {
      case a if a.isEmpty => None
      case events =>
        val props = properties.map(p => (p._1, p._2(events)))
        Some(
          new Event(events.head.patientId,
                    "Group",
                    events.head.begin,
                    events.last.end,
                    props))
    }
  }
}
