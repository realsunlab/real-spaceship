package edu.gatech.sunlab.spaceship.common.events.transform

import edu.gatech.sunlab.spaceship.common.events.FeatureOperator
import edu.gatech.sunlab.spaceship.common.events.expression.BooleanExpr
import edu.gatech.sunlab.spaceship.common.model.{Event, Events, Patient}
import edu.gatech.sunlab.spaceship.common.util._

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/1/16
  */
abstract class EventFilterTransformer
    extends Transformer
    with Logging
    with FeatureOperator {
  override def apply(in: Events): Events = in.filter(check)
  def check(e: Event): Boolean

  def and(that: EventFilterTransformer): EventFilterTransformer =
    new AndEventFilterTransformer(this, that)
  def or(that: EventFilterTransformer): EventFilterTransformer =
    new OrEventFilterTransformer(this, that)

  def &&(that: EventFilterTransformer): EventFilterTransformer =
    new AndEventFilterTransformer(this, that)
  def ||(that: EventFilterTransformer): EventFilterTransformer =
    new OrEventFilterTransformer(this, that)
  def unary_~ : EventFilterTransformer = new NotEventFilterTransformer(this)
}

class AndEventFilterTransformer(val left: EventFilterTransformer,
                                val right: EventFilterTransformer)
    extends EventFilterTransformer {

  def check(e: Event): Boolean = left.check(e) && right.check(e)

  override def toString: String =
    "(" + left + ") AND (" + right + ")"
}

class OrEventFilterTransformer(val left: EventFilterTransformer,
                               val right: EventFilterTransformer)
    extends EventFilterTransformer {

  def check(e: Event): Boolean = left.check(e) || right.check(e)

  override def toString: String =
    "(" + left + ") OR (" + right + ")"
}

class NotEventFilterTransformer(val filter: EventFilterTransformer)
    extends EventFilterTransformer {
  def check(e: Event): Boolean = !filter.check(e)

  override def toString: String = "~" + filter.toString
}

class PropertyFilterTransformer(bp: BooleanExpr)
    extends EventFilterTransformer {
  def check(e: Event): Boolean = {
    bp.eval(e)._1
  }

  override def toString: String = bp.toString
}
