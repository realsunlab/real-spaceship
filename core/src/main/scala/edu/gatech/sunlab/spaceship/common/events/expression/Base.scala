package edu.gatech.sunlab.spaceship.common.events.expression

import edu.gatech.sunlab.spaceship.common.model.Event
import edu.gatech.sunlab.spaceship.common.util.Logging

import scala.reflect.runtime.universe.TypeTag

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait Expression extends Serializable with Logging {
  def eval(e: Event): (Any, DataType)

  def +(other: Expression): ArithmeticExpr = new AddExpr(this, other)
  def -(other: Expression): ArithmeticExpr = new MinusExpr(this, other)

  def >(other: Expression): BooleanExpr = new GtExpr(this, other)
  def <(other: Expression): BooleanExpr = new LtExpr(this, other)
  def >=(other: Expression): BooleanExpr = new GteExpr(this, other)
  def <=(other: Expression): BooleanExpr = new LteExpr(this, other)
  def ===(other: Expression): BooleanExpr = new EqExpr(this, other)
  def =?=(other: Expression): BooleanExpr = new InExpr(this, other)
  def =~=(other: Expression): BooleanExpr = new RegMatchExpr(this, other)
}

class LiteralExpr[T: TypeTag](v: T) extends Expression {
  lazy val res = lazy_eval

  private def lazy_eval = v match {
    case seq: Seq[Any] =>
      val elemType = DataTypes.get(seq.head)
      val typ = new ArrayType(elemType)
      (v, typ)
    case _ => (v, AtomicType.dataType(v))
  }

  override def eval(e: Event): (Any, DataType) = res

  override def toString = v.toString
}

class FieldExpr(name: String) extends Expression {
  override def eval(e: Event): (Any, DataType) = {
    val value = e.properties.get(name)
    if (value.isDefined) {
      val dataType = DataTypes.get(value.get)
      (value.get, dataType)
    } else {
//      logDebug(s"failed to find $name field in $e")
      (null, NullType)
    }
  }

  //override def toString = s"$$$name"
  override def toString = name
}

trait BinaryOpTypeUtil {
  protected def toSameType(leftType: DataType,
                           leftVal: Any,
                           rightType: DataType,
                           rightVal: Any): (Any, Any, DataType) = {
    val common = commonType(leftType, rightType)
    val newLeftVal = TypeCast(leftVal, leftType, common)
    val newRightVal = TypeCast(rightVal, rightType, common)

    (newLeftVal, newRightVal, common)
  }

  protected def commonType(leftTyp: DataType, rightTyp: DataType): DataType =
    (leftTyp, rightTyp) match {
      case (l: AtomicType, r: AtomicType) if l.tag.tpe =:= r.tag.tpe => leftTyp
      case (l: NumericType, r: NumericType) if l.tag.tpe weak_<:< r.tag.tpe =>
        rightTyp
      case (l: NumericType, r: NumericType) if r.tag.tpe weak_<:< l.tag.tpe =>
        leftTyp
      case _ => sys.error("failed to find higher order type")
    }
}

abstract class BinaryExpr(left: Expression, right: Expression)
    extends Expression
    with BinaryOpTypeUtil

object TypeCast {
  def apply(inVal: Any, inTyp: DataType, outType: DataType): Any =
    (inTyp, outType) match {
      case (a, b) if a == b => inVal
      case (in: NumericType, out: DoubleType) =>
        in.numeric.asInstanceOf[Numeric[Any]].toDouble(inVal)
      case _ => sys.error(s"cast from $inTyp to $outType not supported")
    }
}
