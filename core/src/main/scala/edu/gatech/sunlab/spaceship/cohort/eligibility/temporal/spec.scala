package edu.gatech.sunlab.spaceship.cohort.eligibility.temporal

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/20/16
  */
import java.util.UUID

import edu.gatech.sunlab.spaceship.cohort.eligibility.PatientEligibility
import edu.gatech.sunlab.spaceship.common.events.HigherOrderEventExtractor
import edu.gatech.sunlab.spaceship.common.model.{SamplePatient, Events}
import edu.gatech.sunlab.spaceship.common.util.Logging
import org.joda.time.Duration

import scala.collection.mutable

class EventsContext(id2events: Map[UUID, Events]) extends Logging {
  private val eventsMap: mutable.Map[UUID, Events] =
    mutable.Map(id2events.toSeq: _*)

  def get(id: UUID): Events = {
    val res = eventsMap.get(id)
    if (res.isEmpty) {
      logWarning(s"Got empty events for $id")
      Stream.empty
    } else res.get
  }
}

abstract class TemporalEligibility extends PatientEligibility with Logging {
  def check(sp: SamplePatient): Boolean = {
    /* evaluate all higher order extractors first */
    val id2events = eventExtractors
      .map(higherExtractor => (higherExtractor.uuid, higherExtractor))
      .toMap
      .mapValues {
        case higherOrderEventExtractor =>
          val eventExtractor = higherOrderEventExtractor(List(sp.indexDate))
          eventExtractor(sp.p)
      }

    val eventsContext = new EventsContext(id2events)

    logDebug(s"extractor hash id evaluated as ${id2events.keys}")

    val temporalPredicate = predicate()

    temporalPredicate.eval(eventsContext)._1
  }

  def predicate(): TemporalPredicate

  def eventExtractors: List[HigherOrderEventExtractor]

  def and(other: TemporalEligibility) = new AndTemporalEligibility(this, other)

  def or(other: TemporalEligibility) = new OrTemporalEligibility(this, other)

  override def toString: String = {
    val extractorStr = eventExtractors
      .map(higherExtractor =>
        s"${System.identityHashCode(higherExtractor)} := $higherExtractor")
      .mkString("\n")
    val predicateStr = predicate().toString

    s"$predicateStr \n where:\n$extractorStr"
  }
}

class AndTemporalEligibility(left: TemporalEligibility,
                             right: TemporalEligibility)
    extends TemporalEligibility {
  def predicate(): TemporalPredicate = {
    left.predicate() and right.predicate()
  }

  override def eventExtractors: List[HigherOrderEventExtractor] =
    left.eventExtractors ++ right.eventExtractors
}

class OrTemporalEligibility(left: TemporalEligibility,
                            right: TemporalEligibility)
    extends TemporalEligibility {
  def predicate(): TemporalPredicate = {
    left.predicate() or right.predicate()
  }

  override def eventExtractors: List[HigherOrderEventExtractor] =
    left.eventExtractors ++ right.eventExtractors
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
trait AtomicTemporalEligibilitySpec extends Serializable {
  val left: HigherOrderEventExtractor
  val right: HigherOrderEventExtractor
  val noLeft: Boolean
  val noRight: Boolean

  def build: TemporalEligibility = {
    val leftEventsId = left.uuid
    val rightEventsId = right.uuid

    new TemporalEligibility {
      override def predicate(): TemporalPredicate =
        instantiatePredicate(leftEventsId, rightEventsId)

      override def eventExtractors: List[HigherOrderEventExtractor] =
        List(left, right)
    }
  }

  def instantiatePredicate(leftEventsId: UUID,
                           rightEventsId: UUID): TemporalPredicate
}

case class BeforeTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                         right: HigherOrderEventExtractor,
                                         noLeft: Boolean,
                                         noRight: Boolean,
                                         most: Option[Duration] = None,
                                         least: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenBefore {
      override val maxDuration: Option[Duration] = most
      override val minDuration: Duration = least
    }

  def atMost(d: Duration) = this.copy(most = Some(d))

  def atLeast(d: Duration) = this.copy(least = d)
}

case class AfterTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                        right: HigherOrderEventExtractor,
                                        noLeft: Boolean,
                                        noRight: Boolean,
                                        most: Option[Duration] = None,
                                        least: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenAfter {
      override val maxDuration: Option[Duration] = most
      override val minDuration: Duration = least
    }

  def atMost(d: Duration) = this.copy(most = Some(d))

  def atLeast(d: Duration) = this.copy(least = d)
}

case class MeetTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                       right: HigherOrderEventExtractor,
                                       noLeft: Boolean,
                                       noRight: Boolean,
                                       tol: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenMeet {
      override val tolerance: Duration = tol
    }

  def tolerate(d: Duration) = this.copy(tol = d)
}

case class OverlapTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                          right: HigherOrderEventExtractor,
                                          noLeft: Boolean,
                                          noRight: Boolean,
                                          least: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenOverlap {
      override val minDuration: Duration = least
    }

  def atLeast(d: Duration) = this.copy(least = d)
}

case class StartTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                        right: HigherOrderEventExtractor,
                                        noLeft: Boolean,
                                        noRight: Boolean,
                                        tol: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenStart {
      override val tolerance: Duration = tol
    }

  def tolerate(d: Duration) = this.copy(tol = d)
}

case class FinishTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                         right: HigherOrderEventExtractor,
                                         noLeft: Boolean,
                                         noRight: Boolean,
                                         tol: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenFinish {
      override val tolerance: Duration = tol
    }

  def tolerate(d: Duration) = this.copy(tol = d)
}

case class DuringTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                         right: HigherOrderEventExtractor,
                                         noLeft: Boolean,
                                         noRight: Boolean,
                                         afterBegin: Duration = Duration.ZERO,
                                         beforeEnd: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenDuring {
      override val beginMargin: Duration = afterBegin
      override val endMargin: Duration = beforeEnd
    }

  def after(d: Duration) = this.copy(afterBegin = d)

  def before(d: Duration) = this.copy(beforeEnd = d)
}

case class EqualTemporalEligibilitySpec(left: HigherOrderEventExtractor,
                                        right: HigherOrderEventExtractor,
                                        noLeft: Boolean,
                                        noRight: Boolean,
                                        tol: Duration = Duration.ZERO)
    extends AtomicTemporalEligibilitySpec {
  override def instantiatePredicate(leftEventsId: UUID,
                                    rightEventsId: UUID): TemporalPredicate =
    new AtomicTemporalPredicate(leftEventsId, rightEventsId, noLeft, noRight)
    with EventAllenEquals {
      override val tolerance: Duration = tol
    }

  def tolerate(d: Duration) = this.copy(tol = d)
}
