package edu.gatech.sunlab.spaceship.feature

import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.common.events.aggregate.EventAggregator
import edu.gatech.sunlab.spaceship.common.events.expression.BooleanExpr
import edu.gatech.sunlab.spaceship.common.events.transform._
import edu.gatech.sunlab.spaceship.feature.FeatureBuilder.FeatureTree

//import org.json4s._
//import org.json4s.jackson.JsonMethods._

/**
  *
  * @author Yu Jing <yu@argcv.com> on 10/14/16
  */
object dsl {

  def CONCEPT = AttrLabeledGrouper("concept")
  def AG(property: String) = AttrLabeledGrouper(property)
  def BG(bexpr: BooleanExpr) = BinaryLabeledGrouper(bexpr)
  def CG(mapper: EventGroupMapper) = CustomLabeledGrouper(mapper)
  def MG(mapper: MultiValueEventGroupMapper) =
    MultiValueCustomLabeledGrouper(mapper)

  //  implicit def ops2Tree(o: (FeatureOperator, FeatureTree)): FeatureTree = T.s(o._1)(o._2)

  implicit def ea2Tree(o: EventAggregator): FeatureTree = T.a(o)

  def T: FeatureTree = FeatureTree.T

  // filter
  implicit class BoolExprConv(o: BooleanExpr) {
    def ->(t: FeatureTree): FeatureTree = {
      T.s(o)(t)
    }
  }

  implicit class TransformerConv(o: Transformer) {
    def ->(t: FeatureTree): FeatureTree = {
      T.s(o)(t)
    }
  }

  implicit class LabeledGrouperConv(o: LabeledGrouper) {
    def ->(t: FeatureTree): FeatureTree = {
      T.s(o)(t)
    }
  }

  implicit class FeatureTreeConv(o: FeatureTree) {
    def ~(t: FeatureTree): FeatureTree = {
      val oWithFilter = t.filter.foldLeft(o)((l, c) => l.f(c._1)(c._2))
      val oWithGroup = t.group.foldLeft(oWithFilter)((l, c) => l.g(c._1)(c._2))
      t.agg.foldLeft(oWithGroup)((l, c) => l.a(c))
    }
  }

}
