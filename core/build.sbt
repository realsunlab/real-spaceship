name := "spaceship-core"

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

libraryDependencies ++= Seq(
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.5",
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  "org.json4s" %% "json4s-ext" % "3.2.11",
  "com.novus" %% "salat" % "1.9.9",
  "joda-time" % "joda-time" % "2.8.2",
  "org.scalaz" %% "scalaz-core" % "7.1.3",
  "net.sf.py4j" % "py4j" % "0.9",
  // "com.typesafe.scala-logging" %% "scala-logging"        % "3.4.0",
  "it.unimi.dsi" % "fastutil" % "7.0.12"
)

fork in run := true
run in Compile <<= Defaults.runTask(fullClasspath in Compile,
                                    mainClass in (Compile, run),
                                    runner in (Compile, run))
runMain in Compile <<= Defaults.runMainTask(fullClasspath in Compile,
                                            runner in (Compile, run))

assemblyMergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
  case m if m.startsWith("META-INF") => MergeStrategy.discard
  case PathList("javax", "servlet", xs @ _ *) => MergeStrategy.first
  case PathList("org", "apache", xs @ _ *) => MergeStrategy.first
  case PathList("org", "jboss", xs @ _ *) => MergeStrategy.first
  case "about.html" => MergeStrategy.rename
  case "reference.conf" => MergeStrategy.concat
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

assemblyOption in assembly := (assemblyOption in assembly).value
  .copy(includeScala = false)

assemblyExcludedJars in assembly := {
  val cp = (fullClasspath in assembly).value
  cp filter { _.data.getName.contains("spark-") }
}
