package edu.gatech.sunlab.spaceship.choa

import java.io.File

import edu.gatech.sunlab.spaceship.cohort.CohortBuilder
import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.cohort.index.FixedOffsetIndexAssigner
import edu.gatech.sunlab.spaceship.cohort.target.OccurrenceTargetAssigner
import edu.gatech.sunlab.spaceship.common.events.transform._
import edu.gatech.sunlab.spaceship.common.io.{
  FeatureAdapter,
  JsonFileEventLoader,
  PatientLoader
}
import edu.gatech.sunlab.spaceship.common.model.{Event, SamplePatient}
import edu.gatech.sunlab.spaceship.common.util.{Logging, Spark}
import edu.gatech.sunlab.spaceship.feature.FeatureBuilder.FeatureTree
import edu.gatech.sunlab.spaceship.feature.RDDFeatureConstruct
import edu.gatech.sunlab.spaceship.feature.dsl._
import org.apache.spark.rdd.RDD

import scala.language.postfixOps

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class ConceptEventGroupMapper() extends MultiValueEventGroupMapper {
  override def property: String = "concept"

  override def map(in: Event): List[String] = {
    //in.concept.split("/").toList
    val s = in.concept.trim
    if (s.length > 0) {
      List[String](s)
    } else {
      List[String]()
    }
  }
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class ConceptPrefixGroupMapper() extends EventGroupMapper {
  override def map(in: Event): String =
    s"${in.concept}-${in.properties.get("prefix").getOrElse("").toString}"

  override def property: String = "concept-prefix"
}

@SerialVersionUID(edu.gatech.sunlab.spaceship.common.GlobalSerialVersionUID)
case class ConceptVocabGroupMapper() extends EventGroupMapper {
  override def map(in: Event): String =
    s"${in.concept}-${in.properties.get("vocab").getOrElse("").toString}"

  override def property: String = "concept-vocab"
}

/**
  *
  * usage:<br/>
  * {{{
  * MY_JAVA_OPTS=$JAVA_OPTS" -Xms32g -Xmx32g  -XX:ReservedCodeCacheSize=2048m -XX:+UseConcMarkSweepGC -XX:ParallelCMSThreads=16 -XX:ParallelGCThreads=16 "
  * env JAVA_OPTS="$MY_JAVA_OPTS"  SBT_OPTS="" sbt "choa/runMain edu.gatech.sunlab.spaceship.choa.Main"
  * }}}
  *
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 9/27/16
  */
object Main extends App with Logging {
  val sc = Spark.createContext

  val cts = System.currentTimeMillis()

  val inputPathMore = "/tmp/choa/choa.events.small.json"
  val inputPathLess = "/tmp/choa/choa.events.small.filtered.json"
  val inputPath = inputPathMore
  val outputPath = s"/tmp/choa/result_$cts"
  val featurePath = s"$outputPath/features"
  val labelPath = s"$outputPath/labels"
  val statsPath = s"$outputPath/stats"
  val events = new JsonFileEventLoader(inputPath, sc).load()
  val patients = PatientLoader.load(events)

  val asthmaInpatient = $"concept" === "inpatient" && $"reason" === "asthma"
  val target = new OccurrenceTargetAssigner(asthmaInpatient | First, Last)
  val index = new FixedOffsetIndexAssigner(30 days)

  val asthma: EventFilterTransformer = $"type" === "diag" && $"concept" =~= "493.*"
  val asthmaBeforeIndex = Within(-1 years, -1 days) | asthma
  val eligibility = (LENGTH > 0).on(asthmaBeforeIndex)

  val cohort: RDD[SamplePatient] =
    new CohortBuilder(target, index, eligibility).build(patients)

  //  patients.map(p => new SamplePatient(p, p.events.last.begin, p.events.last.begin)).
  //   filter(eligibility.check)

  //less()

  //  def less(): Unit = {
  //    println("save start")
  //    cohort.saveAsObjectFile(inputPathLess)
  //    println("save done")
  //  }

  featureExtractor()

  def featureExtractor(): Unit = {
    // 1) filter ‘type’ === ‘med’ | group by “concept” and “prefix”,
    // 2） filter ‘type’ === ‘proc’ | group by “concept”, “vocab”,
    // 3) filter by type == diag | group by vocab and concept
    // 4) filter by type == encounter | group by concept
    // 5) filter ‘type’ = ‘geo’ | group by concept | LAST | SUM
    //    val exp: FeatureTree = T
    //      .s($"type" === "med", CustomLabeledGrouper(ConceptPrefixGroupMapper()))(
    //        T.a(LENGTH, DURATION))
    //      .s($"type" === "proc", CustomLabeledGrouper(ConceptVocabGroupMapper()))(
    //        T.a(LENGTH, DURATION))
    //      .s($"type" === "diag", AttrLabeledGrouper("concept"))(T.a(LENGTH,
    //        DURATION))
    //      .s($"type" === "encounter", AttrLabeledGrouper("concept"))(T.a(LENGTH,
    //        DURATION))
    //      .s($"type" === "geo", AttrLabeledGrouper("concept"), Last)(T.a(
    //        SUM("value")))
//    val exp3: FeatureTree = T
//      .s($"type" === "med", CG(ConceptPrefixGroupMapper()))(T.a(LENGTH, DURATION))
//      .s($"type" === "proc", CG(ConceptVocabGroupMapper()))(T.a(LENGTH, DURATION))
//      .s($"type" === "diag", CONCEPT)(T.a(LENGTH, DURATION))
//      .s($"type" === "encounter", CONCEPT)(T.a(LENGTH, DURATION))
//      .s($"type" === "geo", CONCEPT, Last)(T.a(SUM("value")))

    val exp = (($"type" === "med") -> (CG(ConceptPrefixGroupMapper()) -> LENGTH ~ DURATION)) ~
        (($"type" === "proc") -> (CG(ConceptVocabGroupMapper()) -> LENGTH ~ DURATION)) ~
        (($"type" === "diag") -> (CONCEPT -> LENGTH ~ DURATION)) ~
        (($"type" === "encounter") -> (CONCEPT -> LENGTH ~ DURATION)) ~
        (($"type" === "geo") -> (CONCEPT -> (Last -> SUM("value"))))

    val samples = cohort

    val result =
      RDDFeatureConstruct(samples)(exp.extract)

    val featureResult = result._1

    val featureStats = result._2

    if (!new File(outputPath).mkdirs()) {
      System.err.println("")
      logError(s"## mkdir output failed ...$outputPath")
    }

    featureStats
      .map(e => s"${e._1}\t${e._2.length}\t${e._2.mkString("\t")}")
      .saveAsTextFile(statsPath)

    FeatureAdapter.save(featureResult, labelPath, featurePath)
  }

  sc.stop()
}
