package edu.gatech.sunlab.spaceship.ucb

import edu.gatech.sunlab.spaceship.cohort.CohortBuilder
import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.cohort.index.EventIndexAssigner
import edu.gatech.sunlab.spaceship.cohort.target.OccurrenceTargetAssigner
import edu.gatech.sunlab.spaceship.common.events.Concept
import edu.gatech.sunlab.spaceship.common.events.transform.Offset
import edu.gatech.sunlab.spaceship.common.io.{
  EventJsonParser,
  JsonUtils,
  PatientLoader,
  JsonFileEventLoader
}
import edu.gatech.sunlab.spaceship.common.util.Spark
import edu.gatech.sunlab.spaceship.common.util.DateTimeGranularity._
import scala.language.postfixOps
import edu.gatech.sunlab.spaceship.common.events.transform.Within

/**
  * @author Hang Su <hangsu@gatech.edu>
  *         Created on 6/24/16
  */
object Main extends App {
  val sc = Spark.createContext

  val aedNames = """CARBAMAZEPINE
                   |DIVALPROEX SODIUM
                   |ETHOSUXIMIDE
                   |ETHOTOIN
                   |EZOGABINE
                   |FELBAMATE
                   |FOSPHENYTOIN SODIUM
                   |GABAPENTIN
                   |LACOSAMIDE
                   |LAMOTRIGINE
                   |LEVETIRACETAM
                   |METHSUXIMIDE
                   |OXCARBAZEPINE
                   |PHENOBARBITAL
                   |PHENYTOIN
                   |PREGABALIN
                   |PRIMIDONE
                   |RUFINAMIDE
                   |TIAGABINE HCL
                   |TOPIRAMATE
                   |VALPROATE SODIUM
                   |VIGABATRIN
                   |ZONISAMIDE""".stripMargin
    .split("\n")
    .filterNot(_.isEmpty)
    .toList

  val aed = $"concept" =?= (")", "2") && $"type" === "medication"
  val diagnosis = (($"concept" === "78039") | First(2) | Merge | $"size" > 1) union $"concept" =~= "345.*"
  val failure = aed | Concat("concept", 3 months) | $"duration" > (6 months) | Overlap(
      "concept",
      15 days)

  val cases = failure | Merge | $"size" >= 4
  val ctrls = failure | Merge | $"size" === 1
  val index = failure | First

  val targetAssigner = new OccurrenceTargetAssigner(cases, ctrls)
  val indexAssigner = new EventIndexAssigner(index)

  val age = Concept("dob") | Offset(granularity = Year)
  val diagcode = ($"concept" === "78039" || $"concept" =~= "345.*") | Within(
      -2 years,
      0 days)
  val eligibility = Exists(diagnosis) and (diagcode before aed atMost (10 days) atLeast (-2 days)) and ($"value" > 16)
      .on(age)

  val inputPath = "file:/home/hang/servers/hang-safe/pcp"
  val pevents = new JsonFileEventLoader(inputPath, sc)
    .load()
    .filter(e =>
      aed.check(e) || e.concept == "78039" || e.concept
        .startsWith("345") || e.concept == "dob")
  //val events = sc.parallelize(Seq("test")).map(EventJsonParser.apply).filter(e => e.concept == "dob" || aed.check(e) || e.concept == "78039" || e.concept.startsWith("345"))
  val patients = PatientLoader.load(pevents)

  val cohort = new CohortBuilder(targetAssigner, indexAssigner, eligibility)
    .build(patients)

  sc.stop()
}
