package org.sutterhealth.rdd.pcp

import edu.gatech.sunlab.spaceship.cohort.{CohortBuilder, dsl}
import dsl._
import edu.gatech.sunlab.spaceship.cohort.index.FixedOffsetIndexAssigner
import edu.gatech.sunlab.spaceship.cohort.target.OccurrenceTargetAssigner
import edu.gatech.sunlab.spaceship.common.events.transform.Within
import edu.gatech.sunlab.spaceship.common.io.{
  JsonFileEventLoader,
  PatientLoader
}
import edu.gatech.sunlab.spaceship.common.model.SamplePatient
import edu.gatech.sunlab.spaceship.common.util.Spark
import org.joda.time.Duration

import scala.language.postfixOps

/**
  * Created by hang on 6/28/16.
  */
object Main {
  def main(args: Array[String]) {
    val sc = Spark.createContext

    val inputPath = "file:/home/hang/servers/hang-safe/pcp"
    val events = new JsonFileEventLoader(inputPath, sc).load()
    val patients = PatientLoader.load(events).cache()

    println(patients.count())

    /* prediction target */
    val pcp = ($"concept" === "Office Visit" || $"concept" === "Urgent Care Office Visit") && $"type" === "encounter" && $"dept_specialty" =?= ("Family Medicine", "Internal Medicine", "Obstetrics-Gynecology", "Gynecology") && $"closed" === true && $"status" =?= ("Completed", "Arrived", "")

    val window = "07/01/2011" to "06/30/2012"
    val filter = pcp | window
    val atLeastOnePcp = LENGTH > 0 on filter

    val caseTarget = pcp | Merge | $"size" > 1 | Begin
    val ctrlTarget = pcp | Merge | $"size" === 1 | Begin

    val targetAssigner = new OccurrenceTargetAssigner(caseTarget, ctrlTarget)
    val indexAssigner = new FixedOffsetIndexAssigner(Duration.ZERO)

    val previousEncounter = Within(-18 months, -1 days) | $"type" === "encounter"
    val noPreviousEncounter = (LENGTH === 0).on(previousEncounter)

    val eligibility = noPreviousEncounter and atLeastOnePcp

    val cohort = new CohortBuilder(targetAssigner, indexAssigner, eligibility)
      .build(patients)

    println(s"cohort size Property(${cohort.count()}")

    patients.filter(filter.apply(_).nonEmpty).count

    patients
      .map(new SamplePatient(_, null, null, 0.0))
      .filter(atLeastOnePcp.check)
      .count

    sc.stop()
  }
}
