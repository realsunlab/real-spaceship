package org.sutterhealth.rdd.cvd

import edu.gatech.sunlab.spaceship.cohort.CohortBuilder
import edu.gatech.sunlab.spaceship.cohort.dsl._
import edu.gatech.sunlab.spaceship.cohort.index.EventIndexAssigner
import edu.gatech.sunlab.spaceship.cohort.target.OccurrenceTargetAssigner
import edu.gatech.sunlab.spaceship.common.events.transform.Within
import edu.gatech.sunlab.spaceship.common.io.{
  JsonFileEventLoader,
  PatientLoader
}
import edu.gatech.sunlab.spaceship.common.util.Spark

import scala.language.postfixOps

/**
  * Created by hang on 7/15/16.
  */
object Main extends App {
  val sc = Spark.createContext

  val inputPath = "file:/home/hang/servers/hang-safe/pcp"
  val events = new JsonFileEventLoader(inputPath, sc).load()
  val patients = PatientLoader.load(events)

  val cardiovascular = (($"concept" =~= "41[0|1|3|4].*") || ($"concept" =~= "43[0-2|4-7].*")) && ($"type" === "diagnosis")
  val diabetes =
    /* 250.xx, primary diabetes (except for 250.x1 and 250.x3, which specify type I) */
    (($"concept" =~= "250.*") && ~($"concept" =~= "250.\\d1" || $"concept" =~= "250.\\d3")) ||
      /* 357.2, polyneuropathy in diabetes */
      ($"concept" === "357.2") ||
      /* 362.0x, diabetic retinopathy */
      ($"concept" === "362.0") || ($"concept" === "362.01") ||
      /* 366.41, diabetic cataract */
      ($"concept" === "366.41")

  val insulin = $"concept" =?= ("Insulin", "Insulin Sensitizing Agents", "Insulin-Like Growth Factors (Somatomedins)") && $"type" === "medclass"
  val antihyperglycemic = $"concept" =?= ("Sulfonylureas", "Biguanides") && $"type" === "medclass"

  val hemoglobin = $"concept" === "HEMOGLOBIN A1C %" && $"type" === "lab" && $"value" >= 6.5
  val glucose = ($"concept" === "GLUCOSE FASTING" || $"concept" === "GLUCOSE") && $"type" === "lab" && $"value" >= 200
  val lab = hemoglobin union glucose

  val targetAssigner =
    new OccurrenceTargetAssigner(cardiovascular | First, Last)

  val beforeTarget = Within(-5 years, -1 years)
  val diagnosis = (diabetes | beforeTarget | Grouped(2, "size" -> LENGTH) | $"size" >= 2) union
      (diabetes | beforeTarget intersect insulin | beforeTarget) union
      (diabetes | beforeTarget intersect antihyperglycemic | beforeTarget) union
      (lab | beforeTarget | Grouped(2, "size" -> LENGTH) | $"size" >= 2) union
      (lab | beforeTarget intersect insulin | beforeTarget) union
      (lab | beforeTarget intersect antihyperglycemic | beforeTarget)

  val indexAssigner = new EventIndexAssigner(
    diagnosis | First | Project("bt" -> ($"bt" + (1 years))))

  val switch = $"type" === "medclass" | Within(-5 years, 1 days) | Concat(
      "concept",
      10 days) | Overlap("concept", 5 days)
  val eligibility = (LENGTH > 0).on(switch)

  val cohort = new CohortBuilder(targetAssigner, indexAssigner, eligibility)
    .build(patients)

  println(s"cohort size == ${cohort.count()}")

  sc.stop()
}
