import json
import datetime
from datetime import timedelta
import random
from random import randrange, randint
import string
import numpy as np

json.JSONEncoder.default = lambda self,obj: (obj.isoformat() if isinstance(obj, datetime.datetime) else None)

def gen_random_interval():
    start = datetime.datetime(2008, 1, 1)
    stop = datetime.datetime(2010, 1, 1)
    total_seconds = int((stop - start).total_seconds())
    begin = randrange(total_seconds)
    end = randrange(begin, total_seconds)

    return start + timedelta(seconds=begin), start + timedelta(seconds=end)

def gen_patient_event(patient_id, concept_name="unknown"):
    begin, end = gen_random_interval()

    return {
        "pid": patient_id,
        "concept": concept_name,
        "begin": begin,
        "end": end,
        "attr1": randint(0, 10),
        "attr2": random.random(),
        "attr3": string.printable[randint(0, 20)]
    }

def gen_patient_events(patient_id):
    concept_ids = np.random.choice(4, 8)
    for concept_id in concept_ids:
        concept_name = "c" + str(concept_id)
        yield gen_patient_event(patient_id, concept_name)

def gen_patient_attributes(patient_id):
    start = datetime.datetime(1970, 1, 1)
    stop = datetime.datetime(2000, 1, 1)
    total_seconds = int((stop - start).total_seconds())
    offset = randrange(total_seconds)
    dob = start + timedelta(seconds=offset)
    return[{
        "pid": patient_id,
        "concept": "DOB",
        "value": dob
    },{
        "pid": patient_id,
        "concept": "gender",
        "value": ['m', 'f'][randint(0, 1)]
    }]

def main():
    for pid in range(20):
        patient_id = pid
        # with open('%d.txt' % pid, 'wb'):
        for event in gen_patient_events(patient_id):
            print json.dumps(event)
        for attribute in gen_patient_attributes(patient_id):
            print json.dumps(attribute)

if __name__ == "__main__":
    main()
