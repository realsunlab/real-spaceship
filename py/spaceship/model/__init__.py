# model here relies on database, connect to db

from mongoengine import connect
from spaceship.config import CONF

connect('spaceship', host=CONF['db']['host'], port=CONF['db']['port'], connect=False)
