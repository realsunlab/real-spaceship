import logging

from mongoengine import *
import datetime
from types import FunctionType

LOG = logging.getLogger(__name__)


class Dataset(Document):
    name = StringField(max_length=100, unique=True)
    des = StringField()
    create_time = DateTimeField(default=datetime.datetime.utcnow)
    train_path = StringField()
    test_path = StringField()
    feature_map_path = StringField()
    status = StringField()
