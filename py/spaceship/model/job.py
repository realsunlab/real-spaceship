import sys
import logging
try:
    import cPickle as pickle
except:
    import pickle

from mongoengine import *
import datetime
from types import FunctionType

LOG = logging.getLogger(__name__)

JOB_RUN_SUBMITTED = "submitted"
JOB_RUN_RUNNING = "running"
JOB_RUN_SUCCEED = "succeed"
JOB_RUN_FAIL = "fail"


class Job(Document):
    name = StringField(max_length=100)
    model_type = StringField(max_length=100)
    create_time = DateTimeField(default=datetime.datetime.utcnow)
    train_path = StringField()
    test_path = StringField()
    raw_space = FileField()  # defined using python code

    @staticmethod
    def with_name_path(name, train_path, test_path):
        return Job(name=name, train_path=train_path, test_path=test_path)

    @property
    def lasted_run(self):
        if self.id is not None:
            return JobRun.objects(job_id=self.id).order_by('-submit_time').first()
        return None

    @property
    def space(self):
        """
        :return: parsed space definition by evaluating the `raw_space` python code
        """
        _space = None
        if self.raw_space is not None:
            space_str = self.raw_space.read()
            self.raw_space.get().seek(0)

            """
            evaluate the load space definition in str type, this
            evaluation actually is dangerous. As a TODO we need to
            check the content before evaluation
            """
            try:
                eval_locals = {}
                exec(space_str, {}, eval_locals)
                _space = eval_locals['space']
            except Exception as e:
                LOG.exception("failed to evaluate the space content")

        return _space


class JobRun(Document):
    job = ReferenceField(Job, required=True)

    mode = StringField(default="local")

    backend = StringField(default="scikit")
    backend_params = DictField(default={})

    scheduling = StringField(default="random")
    scheduling_params = DictField(default={})

    trial_time_limit = IntField(default=900)

    submit_time = DateTimeField(default=datetime.datetime.utcnow)
    start_time = DateTimeField(default=datetime.datetime.utcnow)
    finish_time = DateTimeField(default=datetime.datetime.utcnow)
    status = StringField(required=True, default=JOB_RUN_SUBMITTED)


class Metric(EmbeddedDocument):
    auc = FloatField(min_value=0, max_value=1)
    average_precision = FloatField(min_value=0, max_value=1)
    precision = FloatField(min_value=0, max_value=1)
    recall = FloatField(min_value=0, max_value=1)
    f1 = FloatField(min_value=0, max_value=1)
    mcc = FloatField(min_value=-1, max_value=1)
    accuracy = FloatField(min_value=0, max_value=1)
    mean_squared_error = FloatField(min_value=-sys.maxint - 1, max_value=sys.maxint)
    explained_variance_score = FloatField(min_value=-sys.maxint - 1, max_value=sys.maxint)
    mean_absolute_error = FloatField(min_value=-sys.maxint - 1, max_value=sys.maxint)
    median_absolute_error = FloatField(min_value=-sys.maxint - 1, max_value=sys.maxint)
    r2_score = FloatField(min_value=-sys.maxint - 1, max_value=sys.maxint)


class Result(EmbeddedDocument):
    train = EmbeddedDocumentField(Metric)
    test = EmbeddedDocumentField(Metric)


class StepSpec(EmbeddedDocument):
    name = StringField()
    spec = DictField()
    selection = IntField(default=0) # For flash to catalog what options were chosen

    def __str__(self):
        return "%s:%r:%d" % (self.name, self.spec, self.selection)

    @property
    def python_spec(self):
        def t(v):
            # decode unicode first as we all use ascii
            if isinstance(v, unicode):
                v = str(v)

            if isinstance(v, str) and v.startswith("pickle_"):
                LOG.debug("converting %s from pickle to spec", v)
                return pickle.loads(v[7:])
            else:
                return v

        return {k: t(v) for k, v in self.spec.items()}

    def clean(self):
        """
        make sure spec is savable
        :return: self
        """
        def t(v):
            if isinstance(v, FunctionType):
                LOG.debug("convert function %s in StepSpec to pickle", v.func_name)
                return "pickle_" + pickle.dumps(v)
            else:
                return v

        self.spec = {k: t(v) for k, v in self.spec.items()}


TRIAL_STATUS_RUNNING = "running"
TRIAL_STATUS_WAITING = "waiting"
TRIAL_STATUS_FAIL = "fail"
TRIAL_STATUS_SUCCEED = "succeed"
TRIAL_STATUS_UNKNOWN = "unknown"


class Trial(Document):
    steps_spec = ListField(EmbeddedDocumentField(StepSpec))
    result = EmbeddedDocumentField(Result)
    job_run = ReferenceField(JobRun, required=True)
    status = StringField(required=True, default=TRIAL_STATUS_UNKNOWN)
    submit_time = DateTimeField(default=datetime.datetime.utcnow)
    start_time = DateTimeField(default=datetime.datetime.utcnow)
    finish_time = DateTimeField(default=datetime.datetime.utcnow)
    msg = StringField()
    other = DictField()



