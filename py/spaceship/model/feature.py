import logging

from mongoengine import *
import datetime
from types import FunctionType

LOG = logging.getLogger(__name__)


class FeatureAnalysisJob(Document):
    name = StringField(max_length=100)
    create_time = DateTimeField(default=datetime.datetime.utcnow)
    train_path = StringField()
    feature_map_path = StringField()


class FeatureAnalysisResult(Document):
    job = ReferenceField(FeatureAnalysisJob, required=True)
    method = StringField(max_length=100)
    create_time = DateTimeField(default=datetime.datetime.utcnow)


class FeatureItem(Document):
    job_result = ReferenceField(FeatureAnalysisResult, required=True)
    name = StringField(max_length=1000)
    sign = StringField(max_length=5)
    prob = FloatField()
    rank = IntField()
