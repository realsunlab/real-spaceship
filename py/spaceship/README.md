# Known Issue
On Mac and Windows, potentially the PyMongo module we used to connect database may cause deadlock. Details see
1. [initializing multiple connections from multiprocessing threads causes database connections to fail to be created](https://jira.mongodb.org/browse/PYTHON-961)
2. [PyMongo FAQ: Using PyMongo with Multiprocessing](http://api.mongodb.org/python/current/faq.html#using-pymongo-with-multiprocessing)
