from spaceship.run import job_controller
from spaceship.model.job import Job, JobRun

from .utils import MongoMockTestCase


class TestController(MongoMockTestCase):
    def test_add_job_run(self):
        job = Job.with_name_path(name="test_run",
                                 train_path="data/ml/small_train.svmlight",
                                 test_path="data/ml/small_test.svmlight")

        with open('../default_space.py', 'r') as f:
            job.raw_space = f.read()

        job.save()
