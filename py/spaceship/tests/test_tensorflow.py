import os
import traceback

from unittest import TestCase
from spaceship.backend.scikit import ScikitPipePathFactory
from spaceship.model.job import StepSpec
from spaceship.default_tensorflow_space import space
from spaceship.scheduler.base import RandomScheduler
import math


class TestTensorflow(TestCase):
    def test_parse(self):
        steps_spec = [
            ("feat_norm", {
                    "feat_norm": "MaxAbsScaler"
            }),
            ("feat_pre", {
                    "gamma": 1.0,
                    "feat_pre": "RBFSampler",
                    "n_components": math.log(100)
            }),
            ("classifier", {
                    "steps": 100,
                    "classifier": "tf_LinearClassifier"
            })
        ]

        path_factory = ScikitPipePathFactory(False)
        steps_spec = [StepSpec(name=step_name, spec=spec) for step_name, spec in steps_spec]
        pipe_path = path_factory.parse(steps_spec)
        assert(pipe_path._feature_constructor is None)

    def test_run_pipe_path(self):
        scheduler = RandomScheduler(space, max_iter=1)
        path_factory = ScikitPipePathFactory(True)

        for steps_spec in scheduler:
            pipe_path = path_factory.parse(steps_spec)

            train_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/ml/small_train.svmlight")
            test_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/ml/small_test.svmlight")

            try:
                pipe_path.run(train_path, test_path)
            except (TypeError, ValueError, NotImplementedError), e:
                print steps_spec
                traceback.print_exc(e)
