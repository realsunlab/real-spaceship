from spaceship.model.job import Job, JobRun, Trial
from spaceship.run.local import LocalSeqRunner
from .utils import MongoMockTestCase


class TestLocalSeqRunner(MongoMockTestCase):
    def test_run(self):
        # scheduler = RandomScheduler(space=space, max_iter=20)

        job = Job.with_name_path(name="test_run",
                                 train_path="data/ml/small_train.svmlight",
                                 test_path="data/ml/small_test.svmlight")

        with open('../default_space.py', 'r') as f:
            job.raw_space = f.read()

        job.save()  # force save then job.id will be available

        job_run = JobRun(job=job,
                         scheduling="random",
                         scheduling_params=dict(max_iter=20)
                         )
        job_run.save()

        runner = LocalSeqRunner(job_run)
        runner.run()

        trials = Trial.objects(job_run=job_run)

        self.assertEqual(len(trials), 20)
