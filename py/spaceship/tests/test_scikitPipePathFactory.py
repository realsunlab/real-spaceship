import os
import traceback

from unittest import TestCase
from spaceship.backend.scikit import ScikitPipePathFactory
from spaceship.model.job import StepSpec
from spaceship.default_space import space
from spaceship.scheduler.base import RandomScheduler


class TestScikitPipePathFactory(TestCase):
    def test_parse(self):
        steps_spec = [
            ("feat_norm", {
                    "feat_norm": "MaxAbsScaler"
            }),
            ("feat_pre", {
                    "C": 6567.47422206214,
                    "feat_pre": "LinearSVC",
                    "tol": 0.00975074252851604
            }),
            ("classifier", {
                    "loss": "log",
                    "eta0": 0.0308263365895203,
                    "n_iter": 95,
                    "l1_ratio": 0.7537056248702753,
                    "learning_rate": "optimal",
                    "classifier": "SGDClassifier",
                    "penalty": "l2",
                    "power_t": 0.8088125582416906,
                    "epsilon": 0.000013770507872346287,
                    "alpha": 0.0026882986982910907,
                    "average": True
            })
        ]

        path_factory = ScikitPipePathFactory(False)
        steps_spec = [StepSpec(name=step_name, spec=spec) for step_name, spec in steps_spec]
        pipe_path = path_factory.parse(steps_spec)
        assert(pipe_path._feature_constructor is None)

    def test_run_pipe_path(self):
        scheduler = RandomScheduler(space, max_iter=50)
        path_factory = ScikitPipePathFactory(True)

        for steps_spec in scheduler:
            pipe_path = path_factory.parse(steps_spec)

            train_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/ml/small_train.svmlight")
            test_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/ml/small_test.svmlight")

            try:
                pipe_path.run(train_path, test_path)
            except (TypeError, ValueError, NotImplementedError), e:
                print steps_spec
                traceback.print_exc(e)

    def test_run_sparse_pipe_path(self):
        from spaceship.default_sparse_space import space as sspace
        scheduler = RandomScheduler(sspace, max_iter=50)
        path_factory = ScikitPipePathFactory(False)

        for steps_spec in scheduler:
            pipe_path = path_factory.parse(steps_spec)

            train_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/ml/small_train.svmlight")
            test_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/ml/small_test.svmlight")

            try:
                pipe_path.run(train_path, test_path)
            except (ValueError, NotImplementedError), e:
                print steps_spec
                traceback.print_exc(e)
