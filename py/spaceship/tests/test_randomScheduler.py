from unittest import TestCase

from spaceship.default_space import space
from spaceship.scheduler.base import RandomScheduler


class TestRandomScheduler(TestCase):
    def test_next(self):
        scheduler = RandomScheduler(space, max_iter=5)
        steps_specs = [steps_spec for steps_spec in scheduler]

        self.assertEqual(len(steps_specs), 5, "generated spec size problem")


