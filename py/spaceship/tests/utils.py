from unittest import TestCase
from mongoengine import register_connection


class MongoMockTestCase(TestCase):
    def setUp(self):
        register_connection('default', 'mongoenginetest', host='mongomock://localhost', is_mock=True)
