from random import random
from unittest import TestCase, skip

from hyperopt.pyll.stochastic import sample as hpsample

from spaceship.default_space import space
from spaceship.model.job import Trial, Result, Metric, StepSpec
from spaceship.scheduler.flash import FlashScheduler

@skip("Falsh scheduler related tests disabled")
class TestFlashScheduler(TestCase):
    @staticmethod
    def random_trial(steps_spec=None):
        steps_spec = steps_spec or [(step_name, hpsample(choices)) for step_name, choices in space]
        db_steps_spec = [StepSpec(name=step_name, spec=spec) for step_name, spec in steps_spec]

        trial = Trial(
            steps_spec=db_steps_spec,
            result=Result(
                train=Metric(
                    auc=random()
                ),
                test=Metric(
                    auc=random()
                )
            )
        )

        return trial

    def test_next(self):
        scheduler = FlashScheduler(space, init_budget=10)

        for i in range(5):
            scheduler.add_trial(self.random_trial())

        self.assertEqual(len(scheduler._history), 5)

        idx = 0
        for steps_spec in scheduler:
            # assuming we runed the spec
            scheduler.add_trial(self.random_trial(steps_spec))
            self.assertEqual(len(scheduler._history), 6 + idx)
            idx += 1

