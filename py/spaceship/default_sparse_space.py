from math import log
from hyperopt import hp
from hyperopt.pyll import scope
from sklearn.feature_selection import f_classif, chi2
import numpy as np

feat_norm_list = [
    {
        'feat_norm': 'MaxAbsScaler'
    },
    {
        'feat_norm': 'StandardScaler',
        'with_mean': False,
        'with_std': hp.choice('with_std', [True, False])
    }
]

feat_norm = hp.choice('feat_norm', feat_norm_list)

feat_pre_list = [
    {'feat_pre': 'ExtraTreesClassifier',
        'criterion': hp.choice('criterion', ['gini', 'entropy']),  # default: 'gini'
        'max_features': scope.int(hp.uniform('max_features', 1, 5)),  # default: 1
        'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),  # default: 2
        'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20)),  # default: 1
        'bootstrap': hp.choice('bootstrap', [True, False])},  # default: False

    {'feat_pre': 'RBFSampler',
        'gamma': hp.uniform('gamma', 0.3, 2),  # default: 1.0
        'n_components': scope.int(hp.loguniform('n_components', log(50), log(10000)))},  # default: log(100)

    {'feat_pre': 'LinearSVC',
        'tol': hp.loguniform('tol', log(1e-5), log(1e-1)),  # default: log(1e-4)
        'C': hp.loguniform('C', log(0.03125), log(32768))},  # default: log(1)

    {'feat_pre': 'RandomTreesEmbedding',
        'n_estimators': scope.int(hp.uniform('n_estimators', 10, 100)),  # default: 10
        'max_depth': scope.int(hp.uniform('max_depth', 2, 10)),  # default: 5
        'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),  # default: 2
        'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20))},  # default: 1

    {'feat_pre': 'SelectPercentile',
        'percentile': scope.int(hp.uniform('percentile', 1, 99))  # default: 50
    },

    {'feat_pre': 'GenericUnivariateSelect',
        'param': hp.uniform('param', 0.01, 0.5),  # default: 0.1
        'mode': hp.choice('mode', ['fpr', 'fdr', 'fwe'])   # default: 'fpr'
    }
]

feat_pre = hp.choice('feat_pre', feat_pre_list)


classifier_list = [
    {'classifier': 'AdaBoostClassifier',
        'n_estimators': scope.int(hp.uniform('n_estimators', 50, 500)),  # default: 50
        'learning_rate': hp.loguniform('learning_rate', log(0.0001), log(2)),  # default: log(0.1)
        'algorithm': hp.choice('algorithm', ['SAMME', 'SAMME.R'])},  # default: 'SAMME.R'

    {'classifier': 'DecisionTreeClassifier',
        'criterion': hp.choice('criterion', ['gini', 'entropy']),  # default: 'gini'
        'max_depth': hp.uniform('max_depth', 0, 2),  # default: 0.5
        'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),  # default: 2
        'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20))},  # default: 1

    {'classifier': 'ExtraTreesClassifier',
        'criterion': hp.choice('criterion', ['gini', 'entropy']),  # default: 'gini'
        'max_features': scope.int(hp.uniform('max_features', 1, 5)),  # default: 1
        'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),  # default: 2
        'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20)),  # default: 1
        'bootstrap': hp.choice('bootstrap', [True, False])},  # default: False

    {'classifier': 'LinearSVC',
        'tol': hp.loguniform('tol', log(1e-5), log(1e-1)),  # default: log(1e-4)
        'C': hp.loguniform('C', log(0.03125), log(32768))},  # default: log(1)

    {'classifier': 'SVC',
        'C': hp.loguniform('C', log(0.03125), log(32768)),  # default: log(1)
        'kernel': hp.choice('kernel', ['rbf', 'sigmoid', 'poly']),  # default: 'rbf'
        'degree': scope.int(hp.uniform('degree', 1, 5)),  # default: 3
        'gamma': hp.loguniform('gamma', log(3.0517578125e-05), log(8)),  # default: log(0.1)
        'coef0': hp.uniform('coef0', -1, 1),  # default: 0
        'shrinking': hp.choice('shrinking', [True, False]),  # default: True
        'tol': hp.loguniform('tol', log(1e-5), log(1e-1))},  # default: log(1e-4)

    {'classifier': 'MultinomialNB',
        'alpha': hp.loguniform('alpha', log(1e-2), log(100)),  # default: log(1)
        'fit_prior': hp.choice('fit_prior', [True, False])},  # default: True

    {'classifier': 'PassiveAggressiveClassifier',
        'loss': hp.choice('loss', ['hinge', 'squared_hinge']),  # default: 'hinge'}
        'n_iter': hp.uniform('n_iter', 5, 1000),  # default: 20
        'C': hp.loguniform('C', log(1e-5), log(10))},  # default: log(1)

    {'classifier': 'RandomForestClassifier',
        'criterion': hp.choice('criterion', ['gini', 'entropy']),  # default: 'gini'
        'max_features': scope.int(hp.uniform('max_features', 1, 5)),  # default: 1
        'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),  # default: 2
        'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20)),  # default: 1
        'bootstrap': hp.choice('bootstrap', [True, False])},  # default: True

    {'classifier': 'SGDClassifier',
        'loss': hp.choice('loss', ['hinge', 'log', 'squared_hinge', 'perceptron', 'modified_huber']),  # default: 'hinge'
        'epsilon': hp.loguniform('epsilon', log(1e-5), log(1e-1)),  # default: log(1e-4)
        'penalty': hp.choice('penalty', ['l1', 'l2']),  # default: 'l2'
        'l1_ratio': hp.uniform('l1_ratio', 0, 1),  # default: 0.15
        'alpha': hp.loguniform('alpha', log(1e-6), log(1e-1)),  # default: log(0.0001)
        'n_iter': scope.int(hp.uniform('n_iter', 5, 1000)),  # default: 20
        'learning_rate': hp.choice('learning_rate', ['optimal', 'constant', 'invscaling']), # default: 'optimal'
        'power_t': hp.uniform('power_t', 1e-5, 1),  # default: 0.25
        'eta0': hp.uniform('eta0', 1e-6, 1e-1),  # default: 0.01
        'average': hp.choice('average', [True, False])}]  # default: False

classifier = hp.choice('classifier', classifier_list)

space = [('feat_norm', feat_norm), ('feat_pre', feat_pre), ('classifier', classifier)]