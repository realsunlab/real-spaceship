from bson import ObjectId


def as_object_id(obj):
    if not isinstance(obj, ObjectId):
        obj = ObjectId(obj)
    return obj
