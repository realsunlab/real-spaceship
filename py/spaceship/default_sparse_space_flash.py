from math import log
from hyperopt import hp
from hyperopt.pyll import scope
from sklearn.feature_selection import f_classif, chi2
import numpy as np

feat_norm_list = [
    {
        'feat_norm': 'MaxAbsScaler'
    },
    {
        'feat_norm': 'StandardScaler',
        'fn0:with_mean': False,
        'fn0:with_std': hp.choice('fn0:with_std', [True, False])
    }
]

feat_norm = hp.choice('feat_norm', feat_norm_list)

feat_pre_list = [
    {'feat_pre': 'ExtraTreesClassifier',
        'fp1:criterion': hp.choice('fp1:criterion', ['gini', 'entropy']),  # default: 'gini'
        'fp1:max_features': scope.int(hp.uniform('fp1:max_features', 1, 5)),  # default: 1
        'fp1:min_samples_split': scope.int(hp.uniform('fp1:min_samples_split', 2, 20)),  # default: 2
        'fp1:min_samples_leaf': scope.int(hp.uniform('fp1:min_samples_leaf', 1, 20)),  # default: 1
        'fp1:bootstrap': hp.choice('fp1:bootstrap', [True, False])},  # default: False

    {'feat_pre': 'RBFSampler',
        'fp2:gamma': hp.uniform('fp2:gamma', 0.3, 2),  # default: 1.0
        'fp2:n_components': scope.int(hp.loguniform('fp2:n_components', log(50), log(10000)))},  # default: log(100)

    {'feat_pre': 'LinearSVC',
        'fp3:tol': hp.loguniform('fp3:tol', log(1e-5), log(1e-1)),  # default: log(1e-4)
        'fp3:C': hp.loguniform('fp3:C', log(0.03125), log(32768))},  # default: log(1)

    {'feat_pre': 'RandomTreesEmbedding',
        'fp4:n_estimators': scope.int(hp.uniform('fp4:n_estimators', 10, 100)),  # default: 10
        'fp4:max_depth': scope.int(hp.uniform('fp4:max_depth', 2, 10)),  # default: 5
        'fp4:min_samples_split': scope.int(hp.uniform('fp4:min_samples_split', 2, 20)),  # default: 2
        'fp4:min_samples_leaf': scope.int(hp.uniform('fp4:min_samples_leaf', 1, 20))},  # default: 1

    {'feat_pre': 'SelectPercentile',
        'fp5:percentile': scope.int(hp.uniform('fp5:percentile', 1, 99))  # default: 50
    },

    {'feat_pre': 'GenericUnivariateSelect',
        'fp6:param': hp.uniform('fp6:param', 0.01, 0.5),  # default: 0.1
        'fp6:mode': hp.choice('fp6:mode', ['fpr', 'fdr', 'fwe'])   # default: 'fpr'
    }
]

feat_pre = hp.choice('feat_pre', feat_pre_list)


classifier_list = [
    {'classifier': 'AdaBoostClassifier',
        'c0:n_estimators': scope.int(hp.uniform('c0:n_estimators', 50, 500)),  # default: 50
        'c0:learning_rate': hp.loguniform('c0:learning_rate', log(0.0001), log(2)),  # default: log(0.1)
        'c0:algorithm': hp.choice('c0:algorithm', ['SAMME', 'SAMME.R'])},  # default: 'SAMME.R'

    {'classifier': 'DecisionTreeClassifier',
        'c1:criterion': hp.choice('c1:criterion', ['gini', 'entropy']),  # default: 'gini'
        'c1:max_depth': hp.uniform('c1:max_depth', 0, 2),  # default: 0.5
        'c1:min_samples_split': scope.int(hp.uniform('c1:min_samples_split', 2, 20)),  # default: 2
        'c1:min_samples_leaf': scope.int(hp.uniform('c1:min_samples_leaf', 1, 20))},  # default: 1

    {'classifier': 'ExtraTreesClassifier',
        'c2:criterion': hp.choice('c2:criterion', ['gini', 'entropy']),  # default: 'gini'
        'c2:max_features': scope.int(hp.uniform('c2:max_features', 1, 5)),  # default: 1
        'c2:min_samples_split': scope.int(hp.uniform('c2:min_samples_split', 2, 20)),  # default: 2
        'c2:min_samples_leaf': scope.int(hp.uniform('c2:min_samples_leaf', 1, 20)),  # default: 1
        'c2:bootstrap': hp.choice('c2:bootstrap', [True, False])},  # default: False

    {'classifier': 'LinearSVC',
        'c3:tol': hp.loguniform('c3:tol', log(1e-5), log(1e-1)),  # default: log(1e-4)
        'c3:C': hp.loguniform('c3:C', log(0.03125), log(32768))},  # default: log(1)

    {'classifier': 'SVC',
        'c4:C': hp.loguniform('c4:C', log(0.03125), log(32768)),  # default: log(1)
        'c4:kernel': hp.choice('c4:kernel', ['rbf', 'sigmoid', 'poly']),  # default: 'rbf'
        'c4:degree': scope.int(hp.uniform('c4:degree', 1, 5)),  # default: 3
        'c4:gamma': hp.loguniform('c4:gamma', log(3.0517578125e-05), log(8)),  # default: log(0.1)
        'c4:coef0': hp.uniform('c4:coef0', -1, 1),  # default: 0
        'c4:shrinking': hp.choice('c4:shrinking', [True, False]),  # default: True
        'c4:tol': hp.loguniform('c4:tol', log(1e-5), log(1e-1))},  # default: log(1e-4)

    {'classifier': 'MultinomialNB',
        'c5:alpha': hp.loguniform('c5:alpha', log(1e-2), log(100)),  # default: log(1)
        'c5:fit_prior': hp.choice('c5:fit_prior', [True, False])},  # default: True

    {'classifier': 'PassiveAggressiveClassifier',
        'c6:loss': hp.choice('c6:loss', ['hinge', 'squared_hinge']),  # default: 'hinge'}
        'c6:n_iter': hp.uniform('c6:n_iter', 5, 1000),  # default: 20
        'c6:C': hp.loguniform('c6:C', log(1e-5), log(10))},  # default: log(1)

    {'classifier': 'RandomForestClassifier',
        'c7:criterion': hp.choice('c7:criterion', ['gini', 'entropy']),  # default: 'gini'
        'c7:max_features': scope.int(hp.uniform('c7:max_features', 1, 5)),  # default: 1
        'c7:min_samples_split': scope.int(hp.uniform('c7:min_samples_split', 2, 20)),  # default: 2
        'c7:min_samples_leaf': scope.int(hp.uniform('c7:min_samples_leaf', 1, 20)),  # default: 1
        'c7:bootstrap': hp.choice('c7:bootstrap', [True, False])},  # default: True

    {'classifier': 'SGDClassifier',
        'c8:loss': hp.choice('c8:loss', ['hinge', 'log', 'squared_hinge', 'perceptron', 'modified_huber']),  # default: 'hinge'
        'c8:epsilon': hp.loguniform('c8:epsilon', log(1e-5), log(1e-1)),  # default: log(1e-4)
        'c8:penalty': hp.choice('c8:penalty', ['l1', 'l2']),  # default: 'l2'
        'c8:l1_ratio': hp.uniform('c8:l1_ratio', 0, 1),  # default: 0.15
        'c8:alpha': hp.loguniform('c8:alpha', log(1e-6), log(1e-1)),  # default: log(0.0001)
        'c8:n_iter': scope.int(hp.uniform('c8:n_iter', 5, 1000)),  # default: 20
        'c8:learning_rate': hp.choice('c8:learning_rate', ['optimal', 'constant', 'invscaling']), # default: 'optimal'
        'c8:power_t': hp.uniform('c8:power_t', 1e-5, 1),  # default: 0.25
        'c8:eta0': hp.uniform('c8:eta0', 1e-6, 1e-1),  # default: 0.01
        'c8:average': hp.choice('c8:average', [True, False])}]  # default: False

classifier = hp.choice('classifier', classifier_list)

space = [('feat_norm', feat_norm), ('feat_pre', feat_pre), ('classifier', classifier)]
