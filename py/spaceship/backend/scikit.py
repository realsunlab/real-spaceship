import logging
from copy import deepcopy

import sklearn.metrics

from sklearn.pipeline import Pipeline
from sklearn.datasets import load_svmlight_file
from sklearn.cross_validation import StratifiedKFold

# Intentionally import for reflection purpose
from sklearn.preprocessing import MinMaxScaler, StandardScaler, Normalizer
from sklearn.decomposition import FastICA, KernelPCA, PCA
from sklearn.cluster import FeatureAgglomeration
from sklearn.kernel_approximation import RBFSampler, Nystroem
from sklearn.preprocessing import PolynomialFeatures, MaxAbsScaler, MinMaxScaler, Normalizer, RobustScaler, StandardScaler
from sklearn.feature_selection import SelectPercentile, GenericUnivariateSelect, SelectFromModel
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier, \
    RandomForestClassifier, ExtraTreesClassifier, RandomTreesEmbedding
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import LinearSVC, SVC
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.linear_model import PassiveAggressiveClassifier, SGDClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LinearRegression   #importing regression models here
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
# from sklearn.neural_network import MLPRegressor

import math

from .base import PipePath, PipePathFactory
from spaceship.fs.file_wrapper import fopen

from tensorflow_wrapper import tf_LinearClassifier
from tensorflow_wrapper import tf_DNNClassifier
LOG = logging.getLogger(__name__)


class ScikitPipePath(PipePath):
    def __init__(self, feature_constructor, scikit_pipe, retry_with_dense):
        self._retry_with_dense = retry_with_dense
        self._scikit_pipe = scikit_pipe
        self._feature_constructor = feature_constructor
        PipePath.__init__(self)

    def run_classification(self, train_path, test_path):
        LOG.info("Running scikit based pipeline %r", self._scikit_pipe)

        if self._feature_constructor is not None:
            X_train, y_train = self._feature_constructor.fit_transform(train_path)
            X_test, y_test = self._feature_constructor.transform(test_path)
        else:
            # load training data test data directly from file system
            #X_train, y_train = load_svmlight_file(train_path, zero_based=True)
            #X_test, y_test = load_svmlight_file(test_path, n_features=X_train.shape[1], zero_based=True)
            with fopen(train_path) as f_train_path:
                X_train, y_train = load_svmlight_file(f_train_path, zero_based=True)
            with fopen(test_path) as f_test_path:
                X_test, y_test = load_svmlight_file(f_test_path, n_features=X_train.shape[1], zero_based=True)            

        try:
            train_score, test_score = self._get_train_test_error_classification(X_train, y_train, X_test, y_test)

        except TypeError as e:
            if self._error_about_spase(str(e)) and self._retry_with_dense:
                LOG.info("retry due to sparse")
                # re-try with dense features
                X_train = X_train.toarray()
                X_test = X_test.toarray()

                train_score, test_score = self._get_train_test_error_classification(X_train, y_train, X_test, y_test)
            else:
                LOG.exception("unexpected error in fitting/testing scikit based pipe")
                raise e

        res = {'train': train_score, 'test': test_score}
        LOG.info("Finished running scikit based pipeline with %r", res)
        return res

    def run_regression(self, train_path, test_path):
        LOG.info("Running scikit based pipeline %r", self._scikit_pipe)

        if self._feature_constructor is not None:
            X_train, y_train = self._feature_constructor.fit_transform(train_path)
            X_test, y_test = self._feature_constructor.transform(test_path)
        else:
            # load training data test data directly from file system
            X_train, y_train = load_svmlight_file(train_path, zero_based=True)
            X_test, y_test = load_svmlight_file(test_path, n_features=X_train.shape[1], zero_based=True)


        try:
            train_score, test_score = self._get_train_test_error_regression(X_train, y_train, X_test, y_test)

        except TypeError as e:
            if self._error_about_spase(str(e)) and self._retry_with_dense:
                LOG.info("retry due to sparse")
                # re-try with dense features
                X_train = X_train.toarray()
                X_test = X_test.toarray()

                train_score, test_score = self._get_train_test_error_regression(X_train, y_train, X_test, y_test)
            else:
                LOG.exception("unexpected error in fitting/testing scikit based pipe")
                raise e

        res = {'train': train_score, 'test': test_score}
        LOG.info("Finished running scikit based pipeline with %r", res)
        return res

    @staticmethod
    def _error_about_spase(error):
        key_words = ['toarray', 'sparse']
        return any(word in error for word in key_words)

    def _get_train_test_error_classification(self, X_train, y_train, X_test, y_test, k=3):
            train_score = self._run_k_fold(X_train, y_train, k)

            # fit on entire data to get testing score
            scikit_pipe = deepcopy(self._scikit_pipe)
            scikit_pipe.fit(X_train, y_train)
            test_score = self._score_classification(X_test, y_test, scikit_pipe)
            return train_score, test_score

    def _get_train_test_error_regression(self, X_train, y_train, X_test, y_test, k=3):
            #regression is not cross-validated
            scikit_pipe = deepcopy(self._scikit_pipe)
            scikit_pipe.fit(X_train, y_train)
            train_score = self._score_regression(X_train, y_train, scikit_pipe)
            test_score = self._score_regression(X_test, y_test, scikit_pipe)
            return train_score, test_score

    def _run_k_fold(self, X, y, n_folds=3):
        skf = StratifiedKFold(y, n_folds=n_folds)
        scores = {}
        for train_index, test_index in skf:
            X_train, y_train = X[train_index], y[train_index]
            X_test, y_test = X[test_index], y[test_index]

            scikit_pipe = deepcopy(self._scikit_pipe)
            scikit_pipe.fit(X_train, y_train)
            fold_scores = self._score_classification(X_test, y_test, scikit_pipe)
            for k, v in fold_scores.items():
                scores[k] = scores.get(k, 0.0) + v

        return {k: v/n_folds for k, v in scores.items()}

    @staticmethod
    def _score_classification(X, y, scikit_pipe):
        """ get score for fitted mode
        various metrics
        :return: auc, averge_precision, precision, recall, f1, mcc, accuracy
        """
        y_predict = scikit_pipe.predict(X)
        y_predict_prob = y_predict
        if hasattr(scikit_pipe.steps[-1][-1], 'decision_function'):
            y_predict_prob = scikit_pipe.decision_function(X)
        elif hasattr(scikit_pipe.steps[-1][-1], 'predict_proba'):
            y_predict_prob = scikit_pipe.predict_proba(X)[:, 1]
        accuracy = sklearn.metrics.accuracy_score(y, y_predict)  # Accuracy classification score.
        f1 = sklearn.metrics.f1_score(y, y_predict, average='binary')  # F1 score, aka balanced F-score or F-measure
        mcc = sklearn.metrics.matthews_corrcoef(y, y_predict)  # Matthews correlation coefficient (MCC)
        precision = sklearn.metrics.precision_score(y, y_predict, average='binary')
        recall = sklearn.metrics.recall_score(y, y_predict)

        # Metrics based on Prediction Score (or Confidence Value)
        avg_precision = sklearn.metrics.average_precision_score(y, y_predict_prob)  # average precision (AP)
        auc = sklearn.metrics.roc_auc_score(y, y_predict_prob)  # Area Under the Curve (AUC)

        return {
            'auc': auc,
            'average_precision': avg_precision,
            'precision': precision,
            'recall': recall,
            'f1': f1,
            'mcc': mcc,
            'accuracy': accuracy
            }

    @staticmethod
    def _score_regression(X, y, scikit_pipe):
        """ get score for fitted mode
        various metrics
        :return: auc, averge_precision, precision, recall, f1, mcc, accuracy
        """
        # regression metrics are calculated here

        y_predict = scikit_pipe.predict(X)
        y_predict_prob = y_predict
        if hasattr(scikit_pipe.steps[-1][-1], 'decision_function'):
            y_predict_prob = scikit_pipe.decision_function(X)
        elif hasattr(scikit_pipe.steps[-1][-1], 'predict_proba'):
            y_predict_prob = scikit_pipe.predict_proba(X)[:, 1]
        expvar = sklearn.metrics.explained_variance_score(y, y_predict) # explained variance score
        mabserr = sklearn.metrics.mean_absolute_error(y,y_predict) # mean absolute error
        msqerr = sklearn.metrics.mean_squared_error(y,y_predict) # mean squared error
        medabserr = sklearn.metrics.median_absolute_error(y,y_predict) # median squared error
        r2score = sklearn.metrics.r2_score(y,y_predict) # r2 score error
        mcc=0
        accuracy=0

        return {
            'explained_variance_score': expvar,
            'mean_absolute_error': mabserr,
            'mean_squared_error': msqerr,
            'median_absolute_error': medabserr,
            'r2_score': r2score,
            }


class ScikitPipePathFactory(PipePathFactory):
    def __init__(self, retry_with_dense=True):
        PipePathFactory.__init__(self)
        self._retry_with_dense = bool(retry_with_dense)

    def parse(self, steps_spec):
        def get_class(name): return globals().get(name)

        """
        {'classifier': 'LDA',
         'n_components': 73.9057685211818,
         'shrinkage': None,
         'tol': 6.274028407317751e-05}
        """
        scikit_pipe_steps = []
        for step_spec in steps_spec:
            step_name, spec = step_spec.name, step_spec.python_spec

            LOG.debug("Instantiating step [%s] with %r", step_name, spec)

            class_name = spec[step_name]
            kwargsB = deepcopy(spec)
            del kwargsB[step_name]  # rm {classifier: 'LDA'} i.e.
            kwargs = {}
            for step in kwargsB:
                i = step.find(':')
                #print step, i
                if i >= 0:
                    kwargs[step[(i+1):]] = kwargsB[step]
                else:
                    kwargs[step] = kwargsB[step]


            class_ = get_class(class_name)
            if class_ is not None:
                step_instance = class_(**kwargs)
                scikit_pipe_steps.append((step_name, step_instance))
            else:
                LOG.error("Failed to find class of name %s", class_name)

        ppl = Pipeline(scikit_pipe_steps)

        return ScikitPipePath(None, ppl, self._retry_with_dense)
