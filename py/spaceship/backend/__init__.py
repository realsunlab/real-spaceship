import scikit

_backend = {
    'scikit': scikit.ScikitPipePathFactory
}

_LOG = __import__("logging").getLogger(__name__)


def get_backend_by_name(path_factory_name, **kwargs):
    _LOG.debug("getting pipe path factory of name %s and param %r", path_factory_name, kwargs)

    class_ = _backend.get(path_factory_name, None)

    if class_ is None:
        return None

    _instance = class_(**kwargs)

    return _instance
