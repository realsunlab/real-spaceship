from tensorflow.contrib.learn.python.learn import LinearClassifier
from tensorflow.contrib.learn.python.learn import DNNClassifier

import tensorflow.contrib.learn.python.learn as learn


class tf_DNNClassifier(object):

    def __init__(self,steps,hidden_units_1,hidden_units_2,hidden_units_3, input_fn=None,  batch_size=None,monitors=None, max_steps=None):
        self.steps = steps
        self.hidden_units_1 = hidden_units_1
        self.hidden_units_2 = hidden_units_2
        self.hidden_units_3 = hidden_units_3
        self.monitors = monitors
        self.input_fn = input_fn
        self.batch_size = batch_size
        self.max_steps = max_steps
        pass


    def predict(self, x=None):
        """Runs inference to determine the predicted class."""
        preds = self.model._estimator.predict(x)


        return preds['classes']

    def predict_proba(self,x=None):

        preds = self.model._estimator.predict(x)

        return preds['probabilities']

    def fit(self, x=None, y=None):
        x = x.astype(int)
        y = y.astype(int)
        feature_columns = learn.infer_real_valued_columns_from_input(x)

        self.model = DNNClassifier(n_classes=3,hidden_units=[self.hidden_units_1, self.hidden_units_2, self.hidden_units_3],feature_columns=feature_columns)

        if self.monitors is not None:
          deprecated_monitors = [
              m for m in self.monitors
              if not isinstance(m, session_run_hook.SessionRunHook)
          ]
          for monitor in deprecated_monitors:
            monitor.set_estimator(self)
            monitor._lock_estimator()  # pylint: disable=protected-access

        result = self.model._estimator.fit(x=x, y=y, input_fn=self.input_fn, steps=self.steps,
                                     batch_size=self.batch_size, monitors=self.monitors,
                                     max_steps=self.max_steps)

        return result

class tf_LinearClassifier(object):

    def __init__(self,steps,input_fn=None,  batch_size=None,monitors=None, max_steps=None):
        self.steps = steps
        self.monitors = monitors
        self.input_fn = input_fn
        self.batch_size = batch_size
        self.max_steps = max_steps
        pass


    def predict(self, x=None):
        """Runs inference to determine the predicted class."""
        preds = self.model._estimator.predict(x)


        return preds['classes']

    def predict_proba(self,x=None):

        preds = self.model._estimator.predict(x)

        return preds['probabilities']

    def fit(self, x=None, y=None):
        x = x.astype(int)
        y = y.astype(int)
        feature_columns = learn.infer_real_valued_columns_from_input(x)

        self.model = LinearClassifier(n_classes=2,feature_columns=feature_columns)

        if self.monitors is not None:
          deprecated_monitors = [
              m for m in self.monitors
              if not isinstance(m, session_run_hook.SessionRunHook)
          ]
          for monitor in deprecated_monitors:
            monitor.set_estimator(self)
            monitor._lock_estimator()  # pylint: disable=protected-access

        result = self.model._estimator.fit(x=x, y=y, input_fn=self.input_fn, steps=self.steps,
                                     batch_size=self.batch_size, monitors=self.monitors,
                                     max_steps=self.max_steps)

        return result