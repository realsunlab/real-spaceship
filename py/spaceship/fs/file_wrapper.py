import logging
from contextlib import contextmanager

LOG = logging.getLogger(__name__)


@contextmanager
def fopen(path):
    try:
        if(path[0:4]=="hdfs"):
            LOG.info("loading from hdfs files")
            import pydoop.hdfs as hdfs
            f=hdfs.open(path)
        else:
            LOG.info("loading from local files")
            f=open(path)
        yield f
    finally:
        f.close()
