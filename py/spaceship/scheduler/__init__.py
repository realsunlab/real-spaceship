import base
import flash

_schedulers = {
    "random": base.RandomScheduler,
    "flash": flash.FlashScheduler
}

_LOG = __import__("logging").getLogger(__name__)


def get_scheduler_by_name(scheduler_name, **kwargs):
    _LOG.debug("getting scheduler of name %s and param %r", scheduler_name, kwargs)

    class_ = _schedulers.get(scheduler_name, None)

    if class_ is None:
        return None

    _instance = class_(**kwargs)
    return _instance

