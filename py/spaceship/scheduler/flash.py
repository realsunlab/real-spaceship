from .base import Scheduler

import HPOlib.cv as cv
from hyperopt.pyll.stochastic import sample as hpsample
from hyperopt import hp
from spaceship.model.job import \
    TRIAL_STATUS_RUNNING, TRIAL_STATUS_SUCCEED, JOB_RUN_RUNNING, JOB_RUN_SUCCEED
from spaceship.model.job import StepSpec
from scipy.stats import norm
from functools import partial
import numpy as np
import time
import hyperopt
from sklearn import linear_model

import logging

LOG = logging.getLogger(__name__)


class FlashScheduler(Scheduler):
    def __init__(self, space, init_budget = 30, ei_budget = 30, tpe_budget = 1000,
            use_optimal_design = 1, ei_xi = 100, top_k_pipelines = 10, seed = 10):
        self._ei_xi = ei_xi
        self._ei_remain = ei_budget
        self._init_budget = init_budget
        self._final_subspace = False
        self._final_subspace = space
        self._top_k_pipelines = top_k_pipelines
        #self._tpe_budget = tpe_budget
        self._seed = seed
        self._trials = hyperopt.Trials()
        max_iter = init_budget + ei_budget + tpe_budget
        max_iter_stage = [init_budget, init_budget+ei_budget, max_iter]
        Scheduler.__init__(self, space, max_iter, max_iter_stage)

        # Need to offset one index in (use it as 1 indexed)
        ni = [len(d[1].pos_args)-1 for d in self._space]
        self._ni = ni
        self._cum_ni = np.cumsum(ni)

        if use_optimal_design == 1:
            picks = self.get_random_picks_by_optimal_design(ni, init_budget)
        else:
            picks = self.get_pure_random_picks(ni, init_budget)
        self._picks = self.convert_picks_to_spaceship(picks)
        self._X = []
        self._y = []
        self._y_time = []
        self._alpha = 1.0
        # used for tpe
        self.max_queue_len = 100

    def timestamp(self, date):
        return time.mktime(date.timetuple())

    def add_trial(self, trial):
        #print trial.steps_spec
        #print '0', trial.steps_spec[0]
        # make sure the logged result is a number (accept evaluations return 100.0)

        if self._ei_remain >= 0 and trial.status == TRIAL_STATUS_SUCCEED:
            result = trial.result.test.auc * -1.0
            time = self.timestamp(trial.finish_time) - self.timestamp(trial.start_time)
            if time < 1:
                time = 1
            x = [[0]*n for n in self._ni]
            x[0][trial.steps_spec[0].selection] = 1
            x[1][trial.steps_spec[1].selection] = 1
            x[2][trial.steps_spec[2].selection] = 1
            x_flat = np.array(x[0]+x[1]+x[2])
            self._X.append(x_flat)
            self._y.append(result)
            self._y_time.append(np.log(time))
        elif self._ei_remain < 0:
            tid = trial.steps_spec[0].selection
            #print 'trial', tid
            for tri in self._trials._dynamic_trials:
                #print 'match?', tri['tid']
                if tri['tid'] == tid:
                    #print 'got it'
                    if trial.status != TRIAL_STATUS_SUCCEED:
                        tri['state'] = hyperopt.base.JOB_STATE_ERROR
                        #print 'error'
                    else:
                        tri['state'] = hyperopt.base.JOB_STATE_DONE
                        rval = trial.result.test.auc * -1.0
                        #print 'success', rval
                        # TODO I'm not sure TPE will work right unless actual loss is calculated
                        tri['result'] = {'loss': float(rval), 'status': hyperopt.base.STATUS_OK}
                    break
            self._trials.refresh()

    def _ei_next(self, count = 1):
        if len(self._X) == 0:
            LOG.error('No random trials succeeded')
            return False
        lr = linear_model.Ridge(alpha=self._alpha)
        lr.fit(self._X, self._y)
        lr_time = linear_model.Ridge(alpha=self._alpha)
        lr_time.fit(self._X, self._y_time)
        ebeta = lr.coef_[:self._cum_ni[0]], \
                lr.coef_[self._cum_ni[0]:self._cum_ni[1]], \
                lr.coef_[self._cum_ni[1]:]
        LOG.info('LR model estimated unit ranking: %s %s %s' % (str(ebeta[0].argsort()),
                                                                      str(ebeta[1].argsort()),
                                                                      str(ebeta[2].argsort())))
        ebeta_time = lr_time.coef_[:self._cum_ni[0]], \
                     lr_time.coef_[self._cum_ni[0]:self._cum_ni[1]], \
                     lr_time.coef_[self._cum_ni[1]:]
        LOG.info('LR Time model estimated unit ranking: %s %s %s' % (str(ebeta_time[0].argsort()),
                                                                           str(ebeta_time[1].argsort()),
                                                                           str(ebeta_time[2].argsort())))
        # pick the best pipeline by EI
        x_next = self._get_next_by_EI(self._ni, self._alpha, lr, lr_time, np.array(self._X), self._y, float(self._ei_xi), count)
        #print x_next
        picks = []
        for item in x_next:
            pick = [[np.argmax(x_next_i)] for x_next_i in item]
            if count == 1:
                return self.convert_pick_to_spaceship(pick)
            picks.append(pick)
        #print pick
        return self.convert_picks_to_spaceship(picks)

    def _compute_final(self):
        lr = linear_model.Ridge(alpha=self._alpha)
        lr.fit(self._X, self._y)
        lr_time = linear_model.Ridge(alpha=self._alpha)
        lr_time.fit(self._X, self._y_time)
        ebeta = lr.coef_[:self._cum_ni[0]], \
                lr.coef_[self._cum_ni[0]:self._cum_ni[1]], \
                lr.coef_[self._cum_ni[1]:]
        LOG.info('LR model estimated unit ranking: %s %s %s' % (str(ebeta[0].argsort()),
                                                                      str(ebeta[1].argsort()),
                                                                      str(ebeta[2].argsort())))
        ebeta_time = lr_time.coef_[:self._cum_ni[0]], \
                     lr_time.coef_[self._cum_ni[0]:self._cum_ni[1]], \
                     lr_time.coef_[self._cum_ni[1]:]
        LOG.info('LR Time model estimated unit ranking: %s %s %s' % (str(ebeta_time[0].argsort()),
                                                                           str(ebeta_time[1].argsort()),
                                                                           str(ebeta_time[2].argsort())))
        pick = self._get_covered_units_by_ei(self._ni, self._alpha, lr, lr_time, np.array(self._X), self._y, 0, self._top_k_pipelines)
        self._final_subspace = self._construct_subspace(pick)
        #print self._final_subspace
        return

    def _tpe_next(self, count):
        if self._final_subspace == False:
            self._compute_final()
        def domain_fn():
            return 1
        domain = hyperopt.Domain(domain_fn, self._final_subspace, rseed=int(self._seed))
        tpe_with_seed = partial(hyperopt.tpe.suggest, seed=int(self._seed))

        # Mostly from https://github.com/hyperopt/hyperopt/blob/master/hyperopt/base.py (hyperopt.FMinIter)
        n_queued = 0
        algo = tpe_with_seed
        trials = self._trials
        N = count

        def get_queue_len():
            return self._trials.count_by_state_unsynced(hyperopt.base.JOB_STATE_NEW)

        stopped = False
        while n_queued < N:
            qlen = get_queue_len()
            while qlen < self.max_queue_len and n_queued < N:
                n_to_enqueue = N - n_queued
                new_ids = trials.new_trial_ids(n_to_enqueue)
                self._trials.refresh()
                #if 0:
                #    for d in self._trials.trials:
                #        print 'trial %i %s %s' % (d['tid'], d['state'],
                #            d['result'].get('status'))

                new_trials = algo(new_ids, domain, trials) #,
                                  #self.rstate.randint(2 ** 31 - 1))
                assert len(new_ids) >= len(new_trials)
                if len(new_trials):
                    self._trials.insert_trial_docs(new_trials)
                    self._trials.refresh()
                    n_queued += len(new_trials)
                    qlen = get_queue_len()
                else:
                    stopped = True
                    break

            #self.serial_evaluate()

            if stopped:
                break

        return_trials = []
        for trial in new_trials:
            spec = self._convert_tpe_pick_to_spaceship(trial, domain)
            if count == 1:
                return spec
            return_trials.append(spec)

        return return_trials


    def _next_n(self, count):
        return self._next(count)

    def _next(self, count = 1):
        if len(self._picks) > 0:
            if count > 1:
                raise ValueError('These should only be used one at a time.')
            pick = self._picks.pop()
        elif self._ei_remain > 0:
            self._ei_remain -= count
            pick = self._ei_next(count)
        else:
            self._ei_remain = -1
            pick = self._tpe_next(count)
            #print 'tpe pick', pick
        return pick

    def _get_next_by_EI(self, ni, alpha, lr, lr_time, X, y, ei_xi, count):
        '''
        Args:
            ni: number of units in the each layer
            alpha: lambda for Ridge regression
            lr: fitted performance model in burning period
            lr_time: fitted time model in burning period
            X: all previous inputs x
            y: all previous observations corresponding to X
            ei_xi: parameter for EI exploitation-exploration trade-off
        Returns:
            x_next: a nested list [[0,1,0], [1,0,0,0], ...] as the next input x to run a specified pipeline
        '''
        var = np.var(lr.predict(X) - y)
        m = np.dot(X.T, X)
        inv = np.linalg.inv(m + alpha * np.eye(sum(ni)))
        maxEI = float('-inf')
        x_next = None
        nexts = []
        for i in range(np.prod(ni)):
            x = [[0]*n for n in ni]
            x_flat = []
            pipeline = self.get_pipeline_by_flatten_index(ni, i)
            for layer in range(len(ni)):
                x[layer][pipeline[layer]] = 1
                x_flat += x[layer]
            x_flat = np.array(x_flat)
            mu_x = lr.predict([x_flat])
            var_x = var * (1 + np.dot(np.dot(x_flat, inv), x_flat.T))
            sigma_x = np.sqrt(var_x)
            u = (np.min(y) - ei_xi - mu_x) / sigma_x
            EI = sigma_x * (u*norm.cdf(u) + norm.pdf(u))
            estimated_time = lr_time.predict([x_flat])[0]
            if estimated_time <= 0:
                LOG.error('Invalid estimated time %s using 1' % (estimated_time))
                estimated_time = 1
            EIPS = EI / estimated_time
            if EIPS > maxEI and count == 1:
                maxEI = EIPS
                x_next = x
            else:
                nexts.append((EIPS, x))
        if count == 1:
            return [x_next]
        else:
            nexts = sorted(nexts, reverse=True, key=lambda x_next: x_next[0]);
            if count > len(nexts):
                # An alternative here might be to duplicate the list and run some items twice, but I'm not certain if that's good or not...
                raise ValueException('Requested more items than possible, use less than n threads: ', len(nexts))
            select = nexts[:count]
            return [s[1] for s in select]

    def _compute_EI(self, ni, alpha, lr, lr_time, X, y, ei_xi, x):
        var = np.var(lr.predict(X) - y)
        m = np.dot(X.T, X)
        inv = np.linalg.inv(m + alpha * np.eye(sum(ni)))
        x_flat = np.array(x)
        mu_x = lr.predict([x_flat])
        var_x = var * (1 + np.dot(np.dot(x_flat, inv), x_flat.T))
        sigma_x = np.sqrt(var_x)
        u = (np.min(y) - ei_xi - mu_x) / sigma_x
        EI = sigma_x * (u*norm.cdf(u) + norm.pdf(u))
        estimated_time = lr_time.predict([x_flat])[0]
        EIPS = EI / estimated_time

        return EIPS

    def _get_covered_units_by_ei(self, ni, alpha, lr, lr_time, X, y, ei_xi, top_k_pipelines):
        ppl_pred = []
        for i in range(np.prod(ni)):
            x = [[0]*n for n in ni]
            pipeline = self.get_pipeline_by_flatten_index(ni, i)
            x_flat = []
            for layer in range(len(ni)):
                x[layer][pipeline[layer]] = 1
                x_flat += x[layer]
            predict = self._compute_EI(ni, alpha, lr, lr_time, X, y, ei_xi, x_flat)
            ppl_pred.append([predict, pipeline])

        sorted_ppl_pred = sorted(ppl_pred, key=lambda x: x[0], reverse=True)
        cover = [[] for layer in range(len(ni))]
        for k in range(top_k_pipelines):
            pipeline = sorted_ppl_pred[k][1]
            for layer in range(len(ni)):
                unit = pipeline[layer]
                if unit not in cover[layer]:
                    cover[layer].append(unit)

        return cover

    def n_stateless_init(self):
        """
        Number of trials to run without care about history
        """
        return self._init_budget

    def _construct_subspace(self, pick):
        subspace = {
            'feat_norm': [],
            'feat_pre': [],
            'classifier': []
        }

        j = 0
        #print pick
        #print 'pi'

        for step_name, choices in self._space:
            for rawpos in pick[j]:
                #print j, i
                #rawpos = pick[j][i]
                pos = rawpos + 1 # The choices have an extra first element
                subspace[step_name].append(choices.pos_args[pos])
            j += 1

        return {
            'feat_norm': hp.choice('feat_norm', subspace['feat_norm']),
            'feat_pre': hp.choice('feat_pre', subspace['feat_pre']),
            'classifier': hp.choice('classifier', subspace['classifier']),
        }

    def _convert_tpe_pick_to_spaceship(self, trial, domain):
        pick = hyperopt.base.spec_from_misc(trial['misc'])
        memo =  domain.memo_from_config(pick)
        ctrl = hyperopt.base.Ctrl(self._trials, current_trial=trial)
        memo2 = hyperopt.utils.use_obj_for_literal_in_memo(domain.expr, ctrl, hyperopt.base.Ctrl, memo)
        rval = hyperopt.pyll.rec_eval(
                    domain.expr,
                    memo=memo,
                    print_node_on_error=domain.rec_eval_print_node_on_error
        )
        #print 'pyll', rval

        step = []
        for step_name, spec in rval:
            new_step = StepSpec(name=step_name, spec=spec, selection = trial['tid'])
            #print new_step
            step.append(new_step)
        return step

    def convert_pick_to_spaceship(self, pick):
        j = 0
        step = []
        for step_name, choices in self._space:
            rawpos = pick[j][0]
            pos = rawpos + 1 # The choices have an extra first element
            step.append(StepSpec(name=step_name, spec=hpsample(choices.pos_args[pos]), selection = rawpos))
            j += 1
        return step


    def convert_picks_to_spaceship(self, picks):
        newPicks = []
        for i in range(len(picks)):
            newPicks.append(self.convert_pick_to_spaceship(picks[i]))
        return newPicks


    def get_pure_random_picks(self, ni, init_budget):
        picks = []
        for k in range(init_budget):
            idx = np.random.randint(np.prod(ni))
            pipeline = self.get_pipeline_by_flatten_index(ni, idx)
            pick = []
            for layer in range(len(ni)):
                pick.append([pipeline[layer]])
            picks.append(pick)

        return picks

    def get_det(self, k, P):
        assert P.shape[0] == P.shape[1]  # verify P is a square matrix
        p = min(k, P.shape[0])
        eigvals, eigvecs = np.linalg.eig(P)
        top_eigvals = sorted(eigvals, reverse=True)[:p]

        return np.prod(top_eigvals)

    def get_pipeline_by_flatten_index(self, ni, index):
        layer1 = index / np.prod(ni[1:]) % ni[0]
        layer2 = index / np.prod(ni[2:]) % ni[1]
        #layer3 = index / np.prod(ni[3:]) % ni[2]
        layer3 = index % ni[2]
        pipeline = [layer1, layer2, layer3] #, layer4]

        return pipeline


    def get_x_by_flat_index(self, ni, index):
        x = [[0]*n for n in ni]
        pipeline = self.get_pipeline_by_flatten_index(ni, index)
        x_flat = []
        for layer in range(len(ni)):
            x[layer][pipeline[layer]] = 1
            x_flat += x[layer]
        x_flat = np.array([x_flat])

        return x_flat

    def get_random_picks_by_optimal_design(self, ni, init_budget):
        #Initial candidates, here we use all possible pipelines
        candidates = []
        for i in range(np.prod(ni)):
                x = self.get_x_by_flat_index(ni, i)
                candidates.append(x)

        x1 = self.get_x_by_flat_index(ni, np.random.randint(np.prod(ni)))
        P = np.dot(x1.T, x1)
        S = [x1]
        for k in range(2, init_budget+1):
                D_scores = []
                for j, xj in enumerate(candidates):
                        score = self.get_det(k, P+np.dot(xj.T, xj))
                        D_scores.append(score)
                jstars = np.argwhere(D_scores == np.amax(D_scores))
                jstar = np.random.choice(jstars.flatten())
                x_jstar = candidates[jstar]
                P += np.dot(x_jstar.T, x_jstar)
                S.append(x_jstar)

        picks = []
        cum_ni = np.cumsum(ni)
        for x in S:
                pick = [[] for layer in range(len(ni))]
                pick[0] = [np.argmax(x[0][:cum_ni[0]])]
                pick[1] = [np.argmax(x[0][cum_ni[0]:cum_ni[1]])]
                pick[2] = [np.argmax(x[0][cum_ni[1]:])]
                #pick[3] = [np.argmax(x[0][cum_ni[2]:])]
                picks.append(pick)

        return picks

    def extra_rounds(self):
        return 2
