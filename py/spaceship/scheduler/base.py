import logging

from hyperopt.pyll.stochastic import sample as hpsample

from spaceship.model.job import StepSpec

LOG = logging.getLogger(__name__)


class Scheduler:
    def __init__(self, space, max_iter=100, max_iter_stage=[100]):
        self._space = space
        self._max_iter = max_iter
        self._max_iter_stage = max_iter_stage
        self._cur_iter = 0

    def next(self):
        if self._cur_iter < self._max_iter:
            self._cur_iter += 1
            return self._next()
        raise StopIteration

    def next_n(self, count):
        if self._cur_iter+count < self._max_iter:
            self._cur_iter += count
            return self._next_n(count)
        raise StopIteration

    def __iter__(self):
        # Iterators are iterables too.
        # Adding this functions to make them so.
        return self

    ###########################
    # implement in sub class
    ###########################
    def has_next(self, stage=0):
        return self._cur_iter < self._max_iter_stage[stage]

    def _next(self):
        pass

    def _next_n(self):
        pass

    def add_trial(self, trial):
        pass

    def extra_rounds(self):
        return 0

    def n_stateless_init(self):
        """
        Number of trials to run without care about history
        :return:
        """
        return self._max_iter


class RandomScheduler(Scheduler):

    def _next(self):
        steps_spec = [StepSpec(name=step_name, spec=hpsample(choices)) for step_name, choices in self._space]

        return steps_spec

    def _next_n(self):
        raise Exception('Random scheduler provides only one step at a time.')
