import logging
import datetime
import Queue

from threading import RLock

from spaceship.model.job import Trial, StepSpec, \
    JOB_RUN_RUNNING, JOB_RUN_SUCCEED, \
    TRIAL_STATUS_FAIL, TRIAL_STATUS_SUCCEED, TRIAL_STATUS_WAITING
from spaceship.utils.db import as_object_id
from spaceship.scheduler import get_scheduler_by_name

LOG = logging.getLogger(__name__)

# TODO consider loading from  database for incomplete jobs
_pending_job_run = Queue.Queue()
_current_job_run_and_schedule = None
_current_job_lock = RLock()


def add_job_run(job_run):
    # create the first run
    # job_run = JobRun(job=job)
    # job_run.save(cascade=True, scheduling)

    LOG.info("adding new job run")
    global _current_job_run_and_schedule
    if _current_job_run_and_schedule is None:
        with _current_job_lock:
            if _current_job_run_and_schedule is None:
                scheduler = get_scheduler_by_name(job_run.scheduling, **job_run.scheduling_params)
                if scheduler is not None:
                    _current_job_run_and_schedule = (job_run, scheduler)
                else:
                    LOG.error("failed to find scheduler of name [%s]", job_run.scheduling)
            else:
                _pending_job_run.put(job_run)
    else:
        _pending_job_run.put(job_run)


def next_trial():
    global _current_job_run_and_schedule
    if _current_job_run_and_schedule is None:
        LOG.info("_current_job_run is None")
        return None

    next_steps_spec = None
    job_run, scheduler = _current_job_run_and_schedule
    with _current_job_lock:
        try:
            next_steps_spec = scheduler.next()
        except StopIteration:
            LOG.info("current job_run with id %r exhaust", job_run.id)
            try:
                next_steps_spec = None
                job_run = _pending_job_run.get_nowait()
                job_run.status = JOB_RUN_RUNNING
                job_run.start_time = datetime.datetime.utcnow()
                job_run.save()

                scheduler = get_scheduler_by_name(job_run.scheduling, **job_run.scheduling_params)
                if scheduler is not None:
                    _current_job_run_and_schedule = (job_run, scheduler)
                    next_steps_spec = scheduler.next()
                else:
                    LOG.error("failed to find scheduler of name [%s]", job_run.scheduling)
            except Queue.Empty:
                job_run = None
                _current_job_run_and_schedule = None

    if next_steps_spec is None:
        return None

    db_steps_spec = [StepSpec(name=step_name, spec=spec) for step_name, spec in next_steps_spec]

    trial = Trial(job_run=job_run,
                  status=TRIAL_STATUS_WAITING,
                  steps_spec=db_steps_spec
                  )
    trial.save()
    LOG.info("Scheduling trial [%r] for job_run [%r] with name [%s]", trial.id, job_run.id, job_run.job.name)
    return trial.id


def trial_update(trial_id):
    trial_id = as_object_id(trial_id)
    trial = Trial(id=trial_id).get()

    is_last_trial = False

    if _current_job_run_and_schedule is None:
        other_trials = Trial(job_run=trial.job_run)
        is_last_trial = all([_trial_is_finish(trial) for trial in other_trials])
    else:
        job_run, scheduler = _current_job_run_and_schedule
        if job_run.id != trial.job_run.id:
            other_trials = Trial(job_run=trial.job_run)
            is_last_trial = all([_trial_is_finish(trial) for trial in other_trials])

    if is_last_trial:
        trial_job_run = trial.job_run
        trial_job_run.status = JOB_RUN_SUCCEED
        trial_job_run.finish_time = datetime.datetime.utcnow()


def _trial_is_finish(trial):
    return trial.status == TRIAL_STATUS_FAIL or trial.status == TRIAL_STATUS_SUCCEED


"""
workers are untrusted, need mechanism to expire trial in server side
"""