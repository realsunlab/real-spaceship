
import logging
from datetime import datetime
from multiprocessing import Process

from .base import Runner, real_exec_trial
from spaceship.model.job import TRIAL_STATUS_RUNNING, TRIAL_STATUS_FAIL, \
    JOB_RUN_SUCCEED, JOB_RUN_RUNNING

LOG = logging.getLogger(__name__)


class LocalSeqRunner(Runner):
    def run(self):
        self._job_run.status = JOB_RUN_RUNNING
        self._job_run.start_time = datetime.utcnow()
        self._job_run.save()

        for steps_spec in self._scheduler:
            LOG.debug("Got spec as %r", steps_spec)

            trial = self._create_trial(steps_spec, TRIAL_STATUS_RUNNING)
            self.exec_trial(trial)
        self._job_run.finish_time = datetime.utcnow()
        self._job_run.status = JOB_RUN_SUCCEED
        self._job_run.save()

    def exec_trial(self, trial):
        msg = None

        trial_process = None
        exit_code = 0
        try:
            LOG.debug("preparing sub process to run trial")
            process_name = "trial_" + str(trial.id)[-5:]
            trial_process = Process(target=real_exec_trial, name=process_name, args=(trial.id,))
            trial_process.daemon = True
            trial_process.start()
            LOG.info("trial subprocess has id as %r", trial_process.pid)
            trial_process.join(self._job_run.trial_time_limit)
            if trial_process.is_alive():
                LOG.info("trial time out, kill %r", trial.id)
                # time limit reach, kill it
                trial_process.terminate()
                # join after terminate to update status
                trial_process.join()
                msg = "timeout, killed"
            exit_code = trial_process.exitcode
        except KeyboardInterrupt:
            LOG.info("CTRL+C by user")
            if trial_process is not None:
                trial_process.terminate()
                trial_process.join()
            self.handle_ctrl_c(trial)
        except Exception as e:
            exit_code = 1
            msg = repr(e)
            LOG.exception("failed sub process running trial")

        trial.reload()
        if exit_code != 0 and trial.status == TRIAL_STATUS_RUNNING:
            trial.status = TRIAL_STATUS_FAIL
            trial.finish_time = datetime.utcnow()
            trial.msg = msg

        trial.save()

    def handle_ctrl_c(self, trial):
        from mongoengine.connection import disconnect
        disconnect()

        if trial is not None:
            LOG.debug("clean up trial")
            trial.status = TRIAL_STATUS_FAIL
            trial.finish_time = datetime.utcnow()
            trial.msg = "killed by user"
            trial.save(cascade=False)

        if self._job_run is not None:
            LOG.debug("clean up run")
            self._job_run.finish_time = datetime.utcnow()
            self._job_run.status = JOB_RUN_SUCCEED
            self._job_run.save(cascade=False)

        LOG.info("Bye")
        exit(0)
