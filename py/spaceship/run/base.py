import logging

from traceback import format_exc
from datetime import datetime
from copy import deepcopy

from spaceship.model.job import Metric, Result, \
    Trial, TRIAL_STATUS_WAITING, TRIAL_STATUS_FAIL, TRIAL_STATUS_SUCCEED
from spaceship.utils.db import as_object_id
from spaceship.backend import get_backend_by_name
from spaceship.scheduler import get_scheduler_by_name

LOG = logging.getLogger(__name__)


class Runner:
    def __init__(self, job_run):
        self._job = job_run.job
        self._job_run = job_run

        """
        pitfall, should use deepcopy
        """
        scheduling_params = deepcopy(job_run.scheduling_params) or {}
        scheduling_params['space'] = self._job.space

        self._scheduler = get_scheduler_by_name(job_run.scheduling, **scheduling_params)

    def run(self):
        pass

    def _create_trial(self, steps_spec, status=TRIAL_STATUS_WAITING):
        trial = Trial(job_run=self._job_run,
                      status=status,
                      steps_spec=steps_spec
                      )
        trial.save()
        return trial


def real_exec_trial(trial_id):
    trial = None
    try:
        trial_id = as_object_id(trial_id)

        trial = Trial.objects(id=trial_id).get()
        job = trial.job_run.job


        backend_params = deepcopy(trial.job_run.backend_params) or {}
        pipe_path_factory = get_backend_by_name(trial.job_run.backend, **backend_params)
        pipe_path = pipe_path_factory.parse(steps_spec=trial.steps_spec)

        if job.model_type=="regression":
            result = pipe_path.run_regression(job.train_path, job.test_path)
        elif job.model_type=="classification":
            result = pipe_path.run_classification(job.train_path, job.test_path)

        train_result = Metric(**result['train'])
        test_result = Metric(**result['test'])

        result = Result(train=train_result, test=test_result)
        trial.result = result

        trial.status = TRIAL_STATUS_SUCCEED
        trial.finish_time = datetime.utcnow()
        trial.save()

        LOG.info("Succeed: trial [%r] for job [%r] with name [%s]",
                 trial.id, job.id, job.name)
    except Exception as e:
        LOG.error("Failed trial [%r] with exception: %s", trial.id, repr(e))
        if trial is not None:
            trial.status = TRIAL_STATUS_FAIL
            trial.finish_time = datetime.utcnow()
            trial.msg = format_exc(e)
            trial.save()