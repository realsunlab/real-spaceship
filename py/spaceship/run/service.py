"""
A Flask based web service such that one can submit new job runs and rerun existing jobs
"""

import os
import sys

from flask import Flask
from flask_jsonrpc import JSONRPC

PROJECT_DIR, PROJECT_MODULE_NAME = os.path.split(
    os.path.dirname(os.path.realpath(__file__))
)

FLASK_JSONRPC_PROJECT_DIR = os.path.join(PROJECT_DIR, os.pardir)
if os.path.exists(FLASK_JSONRPC_PROJECT_DIR) \
        and FLASK_JSONRPC_PROJECT_DIR not in sys.path:
    sys.path.append(FLASK_JSONRPC_PROJECT_DIR)


app = Flask(__name__)
master_rpc = JSONRPC(app, '/master', enable_web_browsable_api=True)


@master_rpc.method('App.argsValidatePythonMode(a1=int, a2=str, a3=bool, a4=list, a5=dict) -> object')
def args_validate_python_mode(a1, a2, a3, a4, a5):
    return u'int: {0}, str: {1}, bool: {2}, list: {3}, dict: {4}'.format(a1, a2, a3, a4, a5)


@master_rpc.method('App.divide(Number, Number) -> Number', validate=True)
def divide(a, b):
    return a / float(b)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)

# app.run(threaded=True)
