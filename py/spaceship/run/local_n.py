import logging

from datetime import datetime
from multiprocessing import JoinableQueue, Queue, Process
from Queue import Empty

from mongoengine.errors import DoesNotExist, MultipleObjectsReturned

from spaceship.model.job import \
    Trial, TRIAL_STATUS_RUNNING, \
    TRIAL_STATUS_FAIL, JOB_RUN_RUNNING, JOB_RUN_SUCCEED
from spaceship.utils.db import as_object_id
from .base import real_exec_trial, Runner

LOG = logging.getLogger(__name__)


class Consumer(Process):
    def __init__(self, task_queue, res_queue):
        super(Consumer, self).__init__()
        self._task_queue = task_queue
        self._res_queue = res_queue

    def run(self):
        proc_name = self.name
        LOG.info('%s:%d running', proc_name, self.pid)
        while True:
            next_trial_id = self._task_queue.get(block=True)
            if next_trial_id is None:
                # shutdown
                LOG.info('%s:%d exiting', proc_name, self.pid)
                self._task_queue.task_done()
                break
            # handle task
            self.exec_trial(next_trial_id)
            self._res_queue.put(next_trial_id)  # let consumer know it's done
            self._task_queue.task_done()

        return

    def exec_trial(self, trial_id):
        trial = None
        msg = None
        trial_process = None
        exit_code = 0

        try:
            trial_id = as_object_id(trial_id)

            try:
                trial = Trial.objects(id=trial_id).get()
            except (DoesNotExist, MultipleObjectsReturned):
                LOG.exception("failed to get trial")
                return

            LOG.debug("preparing sub process to run trial")
            process_name = "trial_" + str(trial.id)[-5:]

            trial.start_time = datetime.utcnow()
            trial.status = TRIAL_STATUS_RUNNING
            trial.save()

            trial_process = Process(target=real_exec_trial, name=process_name, args=(trial.id,))
            trial_process.daemon = True
            trial_process.start()
            LOG.info("trial subprocess has id as %r", trial_process.pid)
            trial_process.join(trial.job_run.trial_time_limit)
            if trial_process.is_alive():
                LOG.info("trial time out, kill %r", trial_id)
                # time limit reach, kill it
                trial_process.terminate()
                # join after terminate to update status
                trial_process.join()
                msg = "timeout, killed"
            exit_code = trial_process.exitcode
        except KeyboardInterrupt:
            self.handle_ctrl_c(trial_process, trial)
        except Exception as e:
            exit_code = 1
            msg = repr(e)
            LOG.exception("failed sub process running trial")

        trial.reload()
        if exit_code != 0 and trial.status == TRIAL_STATUS_RUNNING:
            trial.status = TRIAL_STATUS_FAIL
            trial.finish_time = datetime.utcnow()
            trial.msg = msg

        trial.save()

    def handle_ctrl_c(self, trial_process, trial):
        LOG.info("%s ctrl+c by user", self.name)
        if trial_process is not None:
            trial_process.terminate()
            trial_process.join()

        if trial is not None:
            trial.status = TRIAL_STATUS_FAIL
            trial.finish_time = datetime.utcnow()
            trial.msg = "killed by user"
            trial.save(cascade=False)

        LOG.info("%s exit", self.name)
        exit(0)


class LocalPoolRunner(Runner):
    def __init__(self, job_run, n=10):
        Runner.__init__(self, job_run)
        self._n_workers = n

        # pre fork
        self._trials_queue = JoinableQueue()
        self._res_queue = Queue()

        self._workers = []
        for i in range(self._n_workers):
            worker = Consumer(self._trials_queue, self._res_queue)
            worker.name = "Worker-%d" % i
            worker.start()

            self._workers.append(worker)

    def run(self):
        try:
            self._job_run.status = JOB_RUN_RUNNING
            self._job_run.start_time = datetime.utcnow()
            self._job_run.save()

            self._run()

            self._job_run.finish_time = datetime.utcnow()
            self._job_run.status = JOB_RUN_SUCCEED
            self._job_run.save(cascade=False)
        except KeyboardInterrupt:
            LOG.info("exiting main")
            for worker in self._workers:
                worker.terminate()
                worker.join()

            self._job_run.finish_time = datetime.utcnow()
            self._job_run.status = JOB_RUN_SUCCEED
            self._job_run.save(cascade=False)

    def _run(self):
        # stage one: run all independent randoms
        for index in range(self._scheduler.n_stateless_init()):
            steps_spec = self._scheduler.next()
            trial = self._create_trial(steps_spec)
            self._trials_queue.put(trial.id)

        # wait until all finish
        self._trials_queue.join()

        LOG.info("Stage one of random trial finish")

        # pass history to scheduler
        for _ in range(self._scheduler.n_stateless_init()):
            try:
                finished_trial_id = self._res_queue.get_nowait()
                finished_trial_id = as_object_id(finished_trial_id)
                trial = Trial.objects(id=finished_trial_id).get()
                self._scheduler.add_trial(trial)
            except Empty:
                LOG.error("Unexpected res_queue empty")

        LOG.info("Passed all history to scheduler")

        for n in range(self._scheduler.extra_rounds()):
            # one trial per worker
            # This has to be done in bulk so different items are created
            step_specs = self._scheduler.next_n(self._n_workers)
            # Bad things happen later if we didn't get enough items to queue...
            assert len(step_specs) == self._n_workers
            for steps_spec in step_specs:
                trial = self._create_trial(steps_spec, TRIAL_STATUS_RUNNING)
                self._trials_queue.put(trial.id)

            LOG.info("Stage two round %d: %d trials added to try", n+1, len(step_specs))

            while self._scheduler.has_next(n+1):
                finished_trial_id = self._res_queue.get(block=True)
                LOG.debug("Got one old trial %s", finished_trial_id)
                # update history
                finished_trial_id = as_object_id(finished_trial_id)
                trial = Trial.objects(id=finished_trial_id).get()
                self._scheduler.add_trial(trial)
                # add new trial
                steps_spec = self._scheduler.next()
                trial = self._create_trial(steps_spec, TRIAL_STATUS_RUNNING)
                self._trials_queue.put(trial.id)

            # Grab trailing history
            for _ in range(self._n_workers):
                finished_trial_id = self._res_queue.get(block=True)
                LOG.debug("Got one old trial %s", finished_trial_id)
                # update history
                finished_trial_id = as_object_id(finished_trial_id)
                trial = Trial.objects(id=finished_trial_id).get()
                self._scheduler.add_trial(trial)

            self._trials_queue.join()
            LOG.info("Stage two round %d complete", n+1)

        LOG.info("There's no more trial from scheduler")
        # add poison pill to stop consumers
        for i in range(self._n_workers):
            self._trials_queue.put(None)

        # wait all to finish
        self._trials_queue.join()
        LOG.info("All worker should finish now")
