from __future__ import absolute_import

from datetime import datetime

from mongoengine.errors import DoesNotExist, MultipleObjectsReturned

from celery import Celery
from celery.utils.log import get_task_logger
from celery.exceptions import SoftTimeLimitExceeded

from spaceship.config import CONF
from spaceship.model.job import \
    Trial, TRIAL_STATUS_RUNNING, \
    TRIAL_STATUS_FAIL
from spaceship.utils.db import as_object_id
from .base import real_exec_trial


celery = Celery('spaceship.run')

celery.conf.update(**CONF['celery'])

LOG = get_task_logger(__name__)


@celery.task(bind=True, retry=False)
def exec_trial(self, trial_id):
    trial = None
    try:
        trial_id = as_object_id(trial_id)

        try:
            trial = Trial.objects(id=trial_id).get()
        except (DoesNotExist, MultipleObjectsReturned):
            LOG.exception("failed to get trial")
            return

        trial.other = trial.other or {}
        trial.other['celery-task-id'] = self.request.id

        trial.start_time = datetime.utcnow()
        trial.status = TRIAL_STATUS_RUNNING
        trial.save()
        real_exec_trial(trial_id)
    except SoftTimeLimitExceeded:
        if trial is not None:
            LOG.info("trial [%r] timeout", trial.id)

            trial.reload()
            trial.finish_time = datetime.utcnow()
            trial.status = TRIAL_STATUS_FAIL
            trial.msg = "reach time limit %s(s)" % str(self.soft_time_limit)
