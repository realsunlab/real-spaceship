import logging

from datetime import datetime
from threading import Thread

from spaceship.model.job import Job, JobRun, \
    JOB_RUN_SUCCEED, JOB_RUN_RUNNING
from spaceship.run.base import Runner
from spaceship.run.trial_queue import exec_trial, celery

LOG = logging.getLogger(__name__)


class RemoteJobRunner(Runner):
    def __init__(self, job_run):
        Runner.__init__(self, job_run)
        self._n_pending = 0
        self._trial_state = celery.events.State()
        self._recv = None

    def run(self):
        try:
            self._job_run.status = JOB_RUN_RUNNING
            self._job_run.start_time = datetime.utcnow()
            self._job_run.save()

            self._run()

            self._job_run.finish_time = datetime.utcnow()
            self._job_run.status = JOB_RUN_SUCCEED
            self._job_run.save(cascade=False)
        except KeyboardInterrupt:
            LOG.info("exiting main")
            self._job_run.finish_time = datetime.utcnow()
            self._job_run.status = JOB_RUN_SUCCEED
            self._job_run.save(cascade=False)

    def _run(self):
        self._job_run.status = JOB_RUN_RUNNING
        self._job_run.start_time = datetime.utcnow()
        self._job_run.save()

        monitor_thread = Thread(target=self.trial_monitor)
        monitor_thread.daemon = True
        monitor_thread.start()

        self._n_pending = self._scheduler.n_stateless_init()

        # Stage one: run all independent randoms
        for index in range(self._scheduler.n_stateless_init()):
            steps_spec = self._scheduler.next()
            trial = self._create_trial(steps_spec)
            exec_trial.apply_async(args=[str(trial.id)],
                                   time_limit=self._job_run.trial_time_limit,
                                   soft_time_limit=(self._job_run.trial_time_limit - 50))
        # TODO other stage in trial_event handler: when a old one finish, add a new trial

        # use time out
        while True:
            monitor_thread.join(120)
            if not monitor_thread.isAlive():
                LOG.info("Event handler finished")
                break

    def trial_event_handler(self, event):
        self._trial_state.event(event)
        LOG.info("received event %r", event)

        self._n_pending -= 1
        if event['type'] == 'task-failed' or event['type'] == 'task-succeeded':
            pass

        if self._n_pending == 0:
            self._recv.should_stop = True
        # TODO handle hard time limit reach event: u'exception': u'TimeLimitExceeded(180,)',
        # state.workers

        LOG.debug("N remaining tasks = %d", self._n_pending)

    def trial_monitor(self):
        try:
            with celery.connection() as connection:
                self._recv = celery.events.Receiver(connection, handlers={
                    'task-failed': self.trial_event_handler,
                    'task-succeeded': self.trial_event_handler,
                    'task-revoked': self.trial_event_handler,
                    '*': self._trial_state.event
                })
                self._recv.capture(limit=None, timeout=None, wakeup=True)

        except (KeyboardInterrupt, SystemExit):
            raise

if __name__ == "__main__":
    job = Job.with_name_path(name="test_run",
                             train_path="data/ml/small_train.svmlight",
                             test_path="data/ml/small_test.svmlight")

    with open('../default_space.py', 'r') as f:
        job.raw_space = f.read()

    job.save()  # force save then job.id will be available

    job_run = JobRun(job=job,
                     scheduling="random",
                     scheduling_params=dict(max_iter=20)
                     )
    job_run.save()

    job_runner = RemoteJobRunner(job_run)
    job_runner.run()

