import os, inspect, yaml

_config_path = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))), 'config.yml')

CONF = {}

with open(_config_path, "r") as ymlfile:
    CONF = yaml.load(ymlfile)

# post processing to get database setting from environment variable if any
CONF['db']['host'] = os.getenv('MONGO_PORT_27017_TCP_ADDR', CONF['db']['host'])
CONF['db']['port'] = int(os.getenv('MONGO_PORT_27017_TCP_PORT', CONF['db']['port']))

