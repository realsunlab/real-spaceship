from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
import numpy as np
from spaceship.model.feature import FeatureAnalysisJob, FeatureAnalysisResult, FeatureItem
from datetime import datetime
import argparse


def load_feature_map(f_name):
    print("Loading feature map...")
    feature_to_idx = {}
    idx_to_feature = {}
    with open(f_name) as f_in:
        for cnt, line in enumerate(f_in):
            x = line.strip().split(",")
            # feature_to_idx[x[0]] = int(x[1])
            # idx_to_feature[int(x[1])] = x[0]
            feature_to_idx[x[0]] = cnt
            idx_to_feature[cnt] = x[0]
    return feature_to_idx, idx_to_feature


def load_feature_vectors(f_name, n_features):
    print("Loading feature vectors...")
    X = []
    y = []
    with open(f_name) as f_in:
        for line in f_in:
            tmp = line.strip().split()
            x = np.zeros(n_features)
            y.append(float(tmp[0]))
            for feature in tmp[1:]:
                tmp2 = feature.split(":")
                x[int(tmp2[0])] = float(tmp2[1])
            X.append(x)
    return np.array(X), np.array(y)


def logistic_regression(X, y, feature_map, job_result):
    print("Regression...")
    clf = LogisticRegression()
    fit = clf.fit(X, y)
    indices = sorted(range(len(clf.coef_[0])), key=lambda x: np.abs(clf.coef_[0][x]), reverse=True)
    with open("lr_rank.txt", "w") as f_out:
        cnt = 0
        for i in indices:
            label = "+"
            if clf.coef_[0][i] < 0:
                label = "-"
            f_out.write("%s,%s,%s\n" % (feature_map[i], label, clf.coef_[0][i]))
            if cnt < 1000:
                FeatureItem(job_result=job_result, sign=label, name=feature_map[i], prob=clf.coef_[0][i], rank=cnt).save()
            cnt += 1
    print("Regression done.")
    return clf.coef_[0]


def univariate_pearson(X, y, feature_map, job_result):
    print("Pearson's r...")
    from scipy.stats import pearsonr
    pearsons = {}
    for i in range(X.shape[1]):
        if i % 100 == 0:
            print("Calculating feature %s: %s" % (i, feature_map[i]))
        x = X[:, i]
        result = pearsonr(y, x)
        pearsons[i] = result[0]
    sorted_pearsons = sorted(pearsons.items(), key=lambda x: np.abs(x[1]), reverse=True)
    results = []
    with open("pearson_rank.txt", "w") as f_out:
        cnt = 0
        for item in sorted_pearsons:
            label = "+"
            if item[1] < 0:
                label = "-"
            f_out.write("%s,%s,%s\n" % (feature_map[item[0]], label, item[1]))
            if cnt < 1000:
                FeatureItem(job_result=job_result, sign=label, name=feature_map[item[0]], rank=cnt, prob=item[1]).save()
            cnt += 1
    print("Pearson done.")
    return results


def recursive_feature_elimination(X, y, lr_scores, feature_map):
    # feature extraction
    print("Recursive feature elimination...")
    model = LogisticRegression()
    rfe = RFE(model, 1)
    fit = rfe.fit(X, y)
    print("Num Features: %d" % fit.n_features_)
    print("Selected Features: %s" % fit.support_)
    print("Feature Ranking: %s" % fit.ranking_)
    indices = sorted(range(len(fit.ranking_)), key=lambda x: fit.ranking_[x])
    with open("rfe_rank.txt", "w") as f_out:
        for i in indices:
            label = "+"
            if lr_scores[i] < 0:
                label = "-"
            f_out.write("%s,%s\n" % (feature_map[i], label))
    print("RFE done.")


def extra_tree(X, y, lr_scores, feature_map, job_result):
    print("Extra tree...")
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.datasets import load_iris
    clf = ExtraTreesClassifier()
    clf = clf.fit(X, y)
    print(clf.feature_importances_)
    indices = sorted(range(len(clf.feature_importances_)), key=lambda x: clf.feature_importances_[x], reverse=True)
    results = []
    with open("tree_rank.txt", "w") as f_out:
        cnt = 0
        for i in indices:
            label = "+"
            if lr_scores[i] < 0:
                label = "-"
            f_out.write("%s,%s,%s\n" % (feature_map[i], label, clf.feature_importances_[i]))
            if cnt < 1000:
                FeatureItem(job_result=job_result, name=feature_map[i], sign=label, rank=cnt, prob=clf.feature_importances_[i]).save()
            cnt += 1
    print("Extra tree done.")
    return results


def run(name, feature_map_path, feature_vector_path):
    job = FeatureAnalysisJob(name=name,
                             train_path=feature_vector_path, feature_map_path=feature_map_path)
    job.save()
    feature_to_idx, idx_to_feature = load_feature_map(feature_map_path)
    X, y = load_feature_vectors(feature_vector_path, max(idx_to_feature.keys())+1)
    lr_results = FeatureAnalysisResult(job=job, method="lr").save()
    pearson_results = FeatureAnalysisResult(job=job, method="pearson").save()
    extra_tree_results = FeatureAnalysisResult(job=job, method="extra tree").save()
    lr_scores = logistic_regression(X, y, idx_to_feature, lr_results)
    univariate_pearson(X, y, idx_to_feature, pearson_results)
    extra_tree(X, y, lr_scores, idx_to_feature, extra_tree_results)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", required=True)
    parser.add_argument("--feature_map", required=True)
    parser.add_argument("--train_file", required=True)

    args = parser.parse_args()
    run(args.name, args.feature_map, args.train_file)
