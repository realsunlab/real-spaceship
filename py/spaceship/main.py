import argparse
import os
import inspect
import logging
import logging.config
import json
import re

from spaceship.model.job import Job, JobRun
from spaceship.run.local import LocalSeqRunner
from spaceship.run.remote import RemoteJobRunner
from spaceship.run.local_n import LocalPoolRunner
from spaceship.utils.db import as_object_id

_log_config_path = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
                                'logging.conf')
logging.config.fileConfig(_log_config_path, disable_existing_loggers=False)

LOG = logging.getLogger(__name__)
LOG.debug("started")


def parse_job_config(config_path):
    with open(config_path, 'rb') as f:
        json_job = json.load(f)

    space_path = json_job.pop('space_path')
    job = Job(**json_job)

    # parse space
    with open(space_path, 'r') as f:
        job.raw_space = f.read()

    job.save()
    return job


def parse_job_run_config(config_path, mode, job=None):
    with open(config_path, 'rb') as f:
        json_job_run = json.load(f)

    if job is None:
        job = json_job_run.pop('job')
        job = Job.objects(id=as_object_id(job)).get()

    job_run = JobRun(**json_job_run)
    job_run.job = job
    job_run.mode = mode
    job_run.save()

    return job_run


def parse_runner(mode, job_run):
    pattern = re.compile("local\[(\d+)\]")
    if mode == "local":
        return LocalSeqRunner(job_run)
    elif mode == "remote":
        return RemoteJobRunner(job_run)
    elif pattern.match(mode) is not None:
        n_workers = int(pattern.match(mode).group(1))
        return LocalPoolRunner(job_run, n_workers)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", default="local")
    parser.add_argument("--job_config")
    parser.add_argument("job_run_config")

    args = parser.parse_args()

    # create job
    job = None
    if args.job_config:
        job = parse_job_config(args.job_config)

    # create job run
    job_run = parse_job_run_config(args.job_run_config, args.mode, job)

    # create runner
    runner = parse_runner(args.mode, job_run)
    runner.run()
