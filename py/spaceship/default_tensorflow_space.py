from math import log
from hyperopt import hp
from hyperopt.pyll import scope
from sklearn.feature_selection import f_classif, chi2

feat_norm_list = [
    {
        'feat_norm': 'MaxAbsScaler'
    },
    {
        'feat_norm': 'StandardScaler',
        'with_mean': False,
        'with_std': hp.choice('with_std', [True, False])
    }
]

feat_norm = hp.choice('feat_norm', feat_norm_list)

feat_pre_list = [

    {'feat_pre': 'RBFSampler',
        'gamma': hp.uniform('gamma', 0.3, 2),  # default: 1.0
        'n_components': scope.int(hp.loguniform('n_components', log(50), log(10000)))},  # default: log(100)

]

feat_pre = hp.choice('feat_pre', feat_pre_list)


classifier_list = [

    { 'classifier': 'tf_LinearClassifier',
         'steps': scope.int(hp.uniform('steps',100,300)),
    },

    {'classifier': 'tf_DNNClassifier',
    'steps': scope.int(hp.uniform('steps', 100, 300)),
    'hidden_units_1': scope.int(hp.uniform('hidden_units_1', 10, 20)),
    'hidden_units_2': scope.int(hp.uniform('hidden_units_2', 30, 40)),
    'hidden_units_3': scope.int(hp.uniform('hidden_units_3', 10, 20)),
    },


]

classifier = hp.choice('classifier', classifier_list)

space = [('feat_norm', feat_norm), ('feat_pre', feat_pre), ('classifier', classifier)]
