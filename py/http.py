# -*- coding: utf-8 -*-
import logging

from flask import Flask, request, jsonify

app = Flask("spaceship")
app.config['JSON_AS_ASCII'] = False

controllers = {}
logging.basicConfig(level=logging.DEBUG)

from spaceship.model.dataset import Dataset
from spaceship.feature import feature_ranking
import threading


@app.route('/datasets', methods=['GET'])
def parse():
    datasets = []
    for dataset in Dataset.objects:
        datasets.append({
            "name": dataset.name,
            "des": dataset.des,
            "create_time": dataset.create_time,
            "train_path": dataset.train_path,
            "test_path": dataset.test_path,
            "feature_map_path": dataset.feature_map_path
        })

    return jsonify(**{"data": datasets})


@app.route('/feature/run/<name>', methods=['POST'])
def feature(name):
    print("feature analysis...")
    data = Dataset.objects(name=name).first()

    thread = threading.Thread(target=feature_ranking.run, args=(name, data.feature_map_path, data.train_path))
    thread.start()

    print("feature analysis done")
    return data.to_json()



if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5533, debug=True)
